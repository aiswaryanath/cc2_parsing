package MT900UI;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import MT199.MT199;
import ReadDBConfiguration.ReadDB;
import Utility.TakeScreenShot_evidence;

public class TestMT900UI {
	
	public static void PUSHMT900() throws Exception{
		System.setProperty("webdriver.chrome.driver", "D:/Core_ComponentTool/IE_Driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
//		driver.get("http://10.10.8.62:10062/BPS/testMT900.jsp");
		driver.get(ReadDB.getMT199_MT900_Stub());
		driver.manage().window().maximize();
		
		driver.findElement(By.name("txtName")).sendKeys(MT900.append_output); 
		TakeScreenShot_evidence.Evidences(driver, "PUSHMT900");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
		Thread.sleep(3000);
		TakeScreenShot_evidence.Evidences(driver, "PUSHMT900_SubmitLOG");
		driver.quit();
	}
	
}
