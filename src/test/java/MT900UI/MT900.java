package MT900UI;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import MT103UI.GetValueMT103;
import MT199.MT199_Modules;
import Utility.SaltString;

public class MT900 {
	
	static PrintStream json_console_add = null;
	public static StringBuffer append_output=null;
	public static String CreateFile_MT900(String FIle_WORKITEMID) throws Exception {
		 String out_put=null;
		     append_output=new StringBuffer();
		 	 MT199_Modules.ifDataExistThenAppend("IWSL    PSH4L900              TO"+new SimpleDateFormat("yyMMdd").format(new Date()).toString()+"000511"+SaltString.getRandNo(3)+"I1545 09CIBCCAT0ACMO00002", append_output);
		 	 MT199_Modules.ifDataExistThenAppend("1545 09PNBPUS3NAXXX04760", append_output);
		 	 MT199_Modules.ifDataExistThenAppend("900 02", append_output);
			 MT199_Modules.ifDataExistThenAppend("{1:F01CIBCCAT0ACMO0000000002}{2:O9001545190109PNBPUS3NAXXX00000000011901091545N}{4:", append_output);
			 MT199_Modules.ifDataExistThenAppend(":20:190109000352"+SaltString.getRandNo(3)+"", append_output);
			 MT199_Modules.ifDataExistThenAppend(":21:"+GetValueMT103.getdata_WIRE_EXT_REF_NUM(FIle_WORKITEMID)+"", append_output);
			 MT199_Modules.ifDataExistThenAppend(":25:1067516", append_output);
			 MT199_Modules.ifDataExistThenAppend(":32A:190109USD25900,64", append_output);
			 MT199_Modules.ifDataExistThenAppend(":52A:CIBCCATT", append_output);
			 MT199_Modules.ifDataExistThenAppend(":72:/BNF/0800/TIME/02.30", append_output);
			 MT199_Modules.ifDataExistThenAppend("///FEDREF/229B1QGC01C010131", append_output);
			 MT199_Modules.ifDataExistThenAppend("-}{5:}-", append_output);
			System.out.println(append_output.toString().trim());
		return out_put;
	}
	
	public static void main(String[] args) throws Exception{
		CreateFile_MT900("7891325");
		TestMT900UI.PUSHMT900();
		
	}

	
}
