package Utility;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Recordset;

public class CustomReport {
	
	
	public static void Create_report() throws Exception{
//		PreparedStatement statement = connection.prepareStatement(exception_grid);
//		statement = connection.prepareStatement(exception_grid);
//		statement.setString(1, testCaseId);
//		System.out.println("exception_grid:"+exception_grid);
//		rs = statement.executeQuery();
//		if (rs.isBeforeFirst()) {
//			System.out.println("EXCEPTION GRID LOG DETAILS:"+exception_grid);
//			out.println(buildTableString("EXCEPTION GRID LOG DETAILS", rs));
//		}

		Connection connection=null;
		System.setProperty("ROW", "2");
		Connection conn = Data_Connection.getConnection(connection); 
		if (conn==null) {
			System.out.println("Connection has Failed!!");
		}else{
			System.out.println("Connection has Passesd");
			Recordset Source_Records = Data_Connection.executeQuery(conn,"Select * from PAIN001 where RUN_CASE = 'Y'");
			
			while (Source_Records.next()) {
				
				System.out.println(buildTableString("EXCEPTION GRID LOG DETAILS", Source_Records));
			}
		}
	}
	
	public static String buildTableString(String tableName, Recordset resultSet) throws Exception{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<H4>" + tableName + "</H4>");
		stringBuilder.append("<input type='text' id='myInput' onkeyup='myFunction()'  height = '45' size='35' placeholder='Search For Type Test Column...' title='Type in a name'> <br><br>");
		stringBuilder.append("<input type='text' id='myInput2' onkeyup='myFunction2()'  height = '45' size='35' placeholder='Search For Type Workitemid...' title='Type in a name'> <br><br>");
		stringBuilder.append("<TABLE id='myTable' class=\"table2excel\" border=\"1\" align=\"CENTER\">");
		try {
//			ResultSetMetaData rsmd2 = resultSet.getMetaData();
//			int columnCount2 = rsmd2.getColumnCount();
			int columnCount2 = resultSet.getCount();

			stringBuilder.append("<TR>");
			for (int i = 0; i < columnCount2; i++) {
				stringBuilder.append("<TH>" + resultSet.getField("Test_ID") + "</TH>");
			}
			stringBuilder.append("</TR>");

			while (resultSet.next()) {
				stringBuilder.append("<TR>");
				for (int i = 0; i < columnCount2; i++) {
					stringBuilder.append("<TD>" + resultSet.getField("Category") + "</TD>");
				}
				stringBuilder.append("</TR>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		stringBuilder.append("</TABLE></P>");
		
		stringBuilder.append("<script>");
		
		stringBuilder.append("function myFunction() {  "
				+ "var input, filter, table, tr, td, i, txtValue;  "
				+ "input = document.getElementById('myInput');  "
				+ "filter = input.value.toUpperCase();  "
				+ "table = document.getElementById('myTable');  "
				+ "tr = table.getElementsByTagName('tr');  "
				+ "for (i = 0; i < tr.length; i++) {    "
				+ "td = tr[i].getElementsByTagName('td')[2];    "
				+ "if (td) {      "
				+ "txtValue = td.textContent || td.innerText;      "
				+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
				+ "tr[i].style.display = '';      "
				+ "} else {        "
				+ "tr[i].style.display = 'none';      "
				+ "}    "
				+ "} "
				+ "}"
				+ "}");
		stringBuilder.append("function myFunction2() {  "
				+ "var input, filter, table, tr, td, i, txtValue;  "
				+ "input = document.getElementById('myInput2');  "
				+ "filter = input.value.toUpperCase();  "
				+ "table = document.getElementById('myTable');  "
				+ "tr = table.getElementsByTagName('tr');  "
				+ "for (i = 0; i < tr.length; i++) {    "
				+ "td = tr[i].getElementsByTagName('td')[1];    "
				+ "if (td) {      "
				+ "txtValue = td.textContent || td.innerText;      "
				+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
				+ "tr[i].style.display = '';      "
				+ "} else {        "
				+ "tr[i].style.display = 'none';      "
				+ "}    "
				+ "} "
				+ "}"
				+ "}");
		
		stringBuilder.append("</script>");

		return stringBuilder.toString();
	}
	
	public static void main(String[] args) throws Exception{
		Create_report();
	}

}
