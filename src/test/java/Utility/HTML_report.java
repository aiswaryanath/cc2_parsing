package Utility;

import java.io.*;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.dom4j.Document;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
public class HTML_report {

	public static void WriteToFile(String fileContent, String fileName) throws IOException {
        String projectPath = "D:\\Core_ComponentTool\\Results\\";//System.getProperty("user.dir");
        String tempFile = projectPath + File.separator+fileName;
        File file = new File(tempFile);
        // if file does exists, then delete and create a new file
        if (file.exists()) {
            try {
                File newFileName = new File(projectPath + File.separator+ "backup_"+fileName);
                file.renameTo(newFileName);
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //write to file with OutputStreamWriter
        OutputStream outputStream = new FileOutputStream(file.getAbsoluteFile());
        Writer writer=new OutputStreamWriter(outputStream);
        writer.write(fileContent);
        writer.close();

    }
	
public static void main(String[] args) throws FilloException {
try {
//define a HTML String Builder
        StringBuilder htmlStringBuilder=new StringBuilder();
        htmlStringBuilder.append("<html><head><title>CORE COMPONENT II Automation Report</title><H2><center>CORE COMPONENT II Automation Report </center></H2></head>");
//        htmlStringBuilder.append("<div id='chartContainer' style='height: 300px; width: 100%;'></div>");
//        htmlStringBuilder.append("<script src='https://canvasjs.com/assets/script/canvasjs.min.js'></script>");
        
        htmlStringBuilder.append("<input type='text' id='myInput' onkeyup='myFunction()'  height = '45' size='35' placeholder='Search For Parsing Column...' title='Type in a name'> &emsp;");
        htmlStringBuilder.append("<input type='text' id='myInput1' onkeyup='myFunction1()'  height = '45' size='35' placeholder='Search For Business Column...' title='Type in a name'> &emsp;");
        htmlStringBuilder.append("<input type='text' id='myInput2' onkeyup='myFunction2()'  height = '45' size='35' placeholder='Search For Async Column...' title='Type in a name'> <Br> <Br>");
        
        htmlStringBuilder.append("<body>");
        htmlStringBuilder.append("<table id='myTable' border=\"1\" bordercolor=\"#000000\" border=\"1\" align=\"CENTER\">");
        htmlStringBuilder.append("<tr><th>TestId</th><th>Test Case Description</th><th>Scenarios</th><th>Category</th><th>Payment WorkItem ID</th><th>File WorkItem ID</th><th>Parsing Result</th><th>Database Validation</th><th>Async Validation</th></tr>");
        System.setProperty("ROW", "2");
    	Fillo fill=new Fillo();
    	Connection conn=fill.getConnection("D:/Core_ComponentTool/Test_Data/AutomationTest_dataPain001.xlsx");
    	Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
    		 while (rs.next()) {
    			 htmlStringBuilder.append("<tr><td>"+rs.getField("Test_ID")+"</td><td>"+rs.getField("Test_Case_Description")+"</td><td>"+rs.getField("Scenarios")+"</td><td>"+rs.getField("Category")+"</td><td>"+rs.getField("PaymentWorkItem_ID")+"</td><td>"+rs.getField("File_WorkitemID")+"</td> <td>"+rs.getField("Parsing_Result")+"</td> <td>"+rs.getField("Business_Validation")+"</td> <td>"+rs.getField("Async_Validation")+"</td></tr>");
    		 }
    		 htmlStringBuilder.append("</table>");
    	        htmlStringBuilder.append("<script>");
    	        htmlStringBuilder.append("function myFunction() {  "
    					+ "var input, filter, table, tr, td, i, txtValue;  "
    					+ "input = document.getElementById('myInput');  "
    					+ "filter = input.value.toUpperCase();  "
    					+ "table = document.getElementById('myTable');  "
    					+ "tr = table.getElementsByTagName('tr');  "
    					+ "for (i = 0; i < tr.length; i++) {    "
    					+ "td = tr[i].getElementsByTagName('td')[6];    "
    					+ "if (td) {      "
    					+ "txtValue = td.textContent || td.innerText;      "
    					+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
    					+ "tr[i].style.display = '';      "
    					+ "} else {        "
    					+ "tr[i].style.display = 'none'; "
    					+ "}    "
    					+ "} "
    					+ "}"
    					+ "}");
    	        htmlStringBuilder.append("function myFunction1() {  "
    					+ "var input, filter, table, tr, td, i, txtValue;  "
    					+ "input = document.getElementById('myInput1');  "
    					+ "filter = input.value.toUpperCase();  "
    					+ "table = document.getElementById('myTable');  "
    					+ "tr = table.getElementsByTagName('tr');  "
    					+ "for (i = 0; i < tr.length; i++) {    "
    					+ "td = tr[i].getElementsByTagName('td')[7];    "
    					+ "if (td) {      "
    					+ "txtValue = td.textContent || td.innerText;      "
    					+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
    					+ "tr[i].style.display = '';      "
    					+ "} else {        "
    					+ "tr[i].style.display = 'none'; "
    					+ "}    "
    					+ "} "
    					+ "}"
    					+ "}");
/*    	        htmlStringBuilder.append("function myFunction2() {  "
    					+ "var input, filter, table, tr, td, i, txtValue;  "
    					+ "input = document.getElementById('myInput2');  "
    					+ "filter = input.value.toUpperCase();  "
    					+ "table = document.getElementById('myTable');  "
    					+ "tr = table.getElementsByTagName('tr');  "
    					+ "for (i = 0; i < tr.length; i++) {    "
    					+ "td = tr[i].getElementsByTagName('td')[8];    "
    					+ "if (td) {      "
    					+ "txtValue = td.textContent || td.innerText;      "
    					+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
    					+ "tr[i].style.display = '';      "
    					+ "} else {        "
    					+ "tr[i].style.display = 'none'; "
    					+ "}    "
    					+ "} "
    					+ "}"
    					+ "}");
    	        
    	        htmlStringBuilder.append("window.onload = function () "
    	        		+ "{var chart = new CanvasJS.Chart('chartContainer', {"
    	        		+ "	exportEnabled: true,	"
    	        		+ "animationEnabled: true"
    	        		+ ",	title:{		text: 'Core Component II Automation Pie Report'	"
    	        		+ "},"
    	        		+ "legend:{"
    	        		+ "cursor: 'pointer',"
    	        		+ "itemclick: explodePie	}"
    	        		+ ",	data: [{"
    	        		+ "		startAngle: 360,  "
    	        		+ "type: 'pie',		"
    	        		+ "showInLegend: true,		"
    	        		+ "toolTipContent: '{name}: <strong>{y}%</strong>',		"
    	        		+ "indexLabel: '{name} - {y}%',		"
    	        		+ "dataPoints: ["
    	        		+ "{y: 34, name: 'Parsing Validation Result', exploded: true },"
    	        		+ "{y: 60, name: 'Business Validation Result' },"
    	        		+ "{ y: 100, name: 'Async Validation Result' },"
    	        		+ "]"
    	        		+ "}]"
    	        		+ "});"
    	        		+ "chart.render();"
    	        		+ "}");
    	        
    	        htmlStringBuilder.append("function explodePie (e) {	"
    	        		+ "if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === 'undefined' || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {"
    	        		+ "e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;	"
    	        		+ "} else {"
    	        		+ "e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;	"
    	        		+ "}"
    	        		+ "e.chart.render();"
    	        		+ "}");
*/    	        
    	        htmlStringBuilder.append("</script>");

        //close html file
    	htmlStringBuilder.append("</body></html>");
        //write html string content to a file
    	
        WriteToFile(htmlStringBuilder.toString(),"testfile.html");
    } catch (IOException e) {
        e.printStackTrace();
    }
 }

public static  void GenerateHTML_report() throws Exception{
	try {
		//define a HTML String Builder
		        StringBuilder htmlStringBuilder=new StringBuilder();
		        htmlStringBuilder.append("<html><head><title>CORE COMPONENT II Automation Report</title><H2><center>CORE COMPONENT II Automation Report </center></H2></head>");
//		        htmlStringBuilder.append("<div id='chartContainer' style='height: 300px; width: 100%;'></div>");
//		        htmlStringBuilder.append("<script src='https://canvasjs.com/assets/script/canvasjs.min.js'></script>");
		        
		        htmlStringBuilder.append("<input type='text' id='myInput' onkeyup='myFunction()'  height = '45' size='35' placeholder='Search For Parsing Column...' title='Type in a name'> &emsp;");
		        htmlStringBuilder.append("<input type='text' id='myInput1' onkeyup='myFunction1()'  height = '45' size='35' placeholder='Search For Business Column...' title='Type in a name'> &emsp;");
		        htmlStringBuilder.append("<input type='text' id='myInput2' onkeyup='myFunction2()'  height = '45' size='35' placeholder='Search For Async Column...' title='Type in a name'> <Br> <Br>");
		        
		        htmlStringBuilder.append("<body>");
		        htmlStringBuilder.append("<table id='myTable' border=\"1\" bordercolor=\"#000000\" border=\"1\" align=\"CENTER\">");
		        htmlStringBuilder.append("<tr><th>TestId</th><th>Test Case Description</th><th>Scenarios</th><th>Category</th><th>Payment WorkItem ID</th><th>File WorkItem ID</th><th>Parsing Result</th><th>Database Validation</th><th>Async Validation</th></tr>");
		        System.setProperty("ROW", "2");
		    	Fillo fill=new Fillo();
		    	Connection conn=fill.getConnection("D:/Core_ComponentTool/Test_Data/AutomationTest_dataPain001.xlsx");
		    	Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
		    		 while (rs.next()) {
		    			 htmlStringBuilder.append("<tr><td>"+rs.getField("Test_ID")+"</td><td>"+rs.getField("Test_Case_Description")+"</td><td>"+rs.getField("Scenarios")+"</td><td>"+rs.getField("Category")+"</td><td>"+rs.getField("PaymentWorkItem_ID")+"</td><td>"+rs.getField("File_WorkitemID")+"</td> <td>"+rs.getField("Parsing_Result")+"</td> <td>"+rs.getField("Business_Validation")+"</td> <td>"+rs.getField("Async_Validation")+"</td></tr>");
		    		 }
		    		 htmlStringBuilder.append("</table>");
		    	        htmlStringBuilder.append("<script>");
		    	        htmlStringBuilder.append("function myFunction() {  "
		    					+ "var input, filter, table, tr, td, i, txtValue;  "
		    					+ "input = document.getElementById('myInput');  "
		    					+ "filter = input.value.toUpperCase();  "
		    					+ "table = document.getElementById('myTable');  "
		    					+ "tr = table.getElementsByTagName('tr');  "
		    					+ "for (i = 0; i < tr.length; i++) {    "
		    					+ "td = tr[i].getElementsByTagName('td')[6];    "
		    					+ "if (td) {      "
		    					+ "txtValue = td.textContent || td.innerText;      "
		    					+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
		    					+ "tr[i].style.display = '';      "
		    					+ "} else {        "
		    					+ "tr[i].style.display = 'none'; "
		    					+ "}    "
		    					+ "} "
		    					+ "}"
		    					+ "}");
		    	        htmlStringBuilder.append("function myFunction1() {  "
		    					+ "var input, filter, table, tr, td, i, txtValue;  "
		    					+ "input = document.getElementById('myInput1');  "
		    					+ "filter = input.value.toUpperCase();  "
		    					+ "table = document.getElementById('myTable');  "
		    					+ "tr = table.getElementsByTagName('tr');  "
		    					+ "for (i = 0; i < tr.length; i++) {    "
		    					+ "td = tr[i].getElementsByTagName('td')[7];    "
		    					+ "if (td) {      "
		    					+ "txtValue = td.textContent || td.innerText;      "
		    					+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
		    					+ "tr[i].style.display = '';      "
		    					+ "} else {        "
		    					+ "tr[i].style.display = 'none'; "
		    					+ "}    "
		    					+ "} "
		    					+ "}"
		    					+ "}");
		/*    	        htmlStringBuilder.append("function myFunction2() {  "
		    					+ "var input, filter, table, tr, td, i, txtValue;  "
		    					+ "input = document.getElementById('myInput2');  "
		    					+ "filter = input.value.toUpperCase();  "
		    					+ "table = document.getElementById('myTable');  "
		    					+ "tr = table.getElementsByTagName('tr');  "
		    					+ "for (i = 0; i < tr.length; i++) {    "
		    					+ "td = tr[i].getElementsByTagName('td')[8];    "
		    					+ "if (td) {      "
		    					+ "txtValue = td.textContent || td.innerText;      "
		    					+ "if (txtValue.toUpperCase().indexOf(filter) > -1) { "
		    					+ "tr[i].style.display = '';      "
		    					+ "} else {        "
		    					+ "tr[i].style.display = 'none'; "
		    					+ "}    "
		    					+ "} "
		    					+ "}"
		    					+ "}");
		    	        
		    	        htmlStringBuilder.append("window.onload = function () "
		    	        		+ "{var chart = new CanvasJS.Chart('chartContainer', {"
		    	        		+ "	exportEnabled: true,	"
		    	        		+ "animationEnabled: true"
		    	        		+ ",	title:{		text: 'Core Component II Automation Pie Report'	"
		    	        		+ "},"
		    	        		+ "legend:{"
		    	        		+ "cursor: 'pointer',"
		    	        		+ "itemclick: explodePie	}"
		    	        		+ ",	data: [{"
		    	        		+ "		startAngle: 360,  "
		    	        		+ "type: 'pie',		"
		    	        		+ "showInLegend: true,		"
		    	        		+ "toolTipContent: '{name}: <strong>{y}%</strong>',		"
		    	        		+ "indexLabel: '{name} - {y}%',		"
		    	        		+ "dataPoints: ["
		    	        		+ "{y: 34, name: 'Parsing Validation Result', exploded: true },"
		    	        		+ "{y: 60, name: 'Business Validation Result' },"
		    	        		+ "{ y: 100, name: 'Async Validation Result' },"
		    	        		+ "]"
		    	        		+ "}]"
		    	        		+ "});"
		    	        		+ "chart.render();"
		    	        		+ "}");
		    	        
		    	        htmlStringBuilder.append("function explodePie (e) {	"
		    	        		+ "if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === 'undefined' || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {"
		    	        		+ "e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;	"
		    	        		+ "} else {"
		    	        		+ "e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;	"
		    	        		+ "}"
		    	        		+ "e.chart.render();"
		    	        		+ "}");
		*/    	        
		    	        htmlStringBuilder.append("</script>");

		        //close html file
		    	htmlStringBuilder.append("</body></html>");
		        //write html string content to a file
		    	
		        WriteToFile(htmlStringBuilder.toString(),"CC2_AutomationReport_"+ZipFiles.getCustomisedDateTime()+".html");
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
}


}
