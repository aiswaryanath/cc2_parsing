/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ClearCellData */
/* */
/* File Name : ClearCellData.java */
/* */
/* Description :ClearCellData is used to clear the Actual data in Excel*/
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Utility;

import java.io.File;
import java.io.PrintStream;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Api_Operaitons.POST_requestApi;

public class ClearCellData {
	
	public static void clearCellValues(String TESTCASEID) throws Exception{
		try{
		System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			conn.executeUpdate("update PAIN001 SET Actual_GrpSts = '', Actual_FileStatus = '', Actual_Error_Code = '', Actual_Error_Desc = '', Message_ID = '', File_WorkitemID = '' , FILE_NAME = '' , File_Status_Actual = '', Batch_Status_Actual = '' , TXN_StatusActual = '' , Internal_Exception_IDActual = '', InternalExceptionDescriptionActual = '' , External_Exception_IDActual = '' , ExternalExceptionDescriptionActual = '' , WorkItemId = '' , Async_GrpSts_Actual = '' , Async_PmtInfSts_Actual = '' , Async_TxInfAndSts_Actual='' , Async_ErrorCode_Actual = '' , Async_Errordesc_Actual = '' , StsRsnInfRsn_Actual = '' , StsRsnInf_AddtlInfActual = ''    where Test_ID  = '"+TESTCASEID+"'");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void clearActualData() throws Exception{
		
		try{
			System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE  = 'Y'");
//		    POST_requestApi.json_console_add = new PrintStream(new File("D:/Core_ComponentTool/Test_Data/Log.txt"));
//			System.setOut(POST_requestApi.json_console_add);
			while (rs.next()) {
				clearCellValues(rs.getField("Test_ID"));
//				System.out.println(count++);
			}
//			POST_requestApi.json_console_add.close();
		}catch (Exception e) {
//			e.printStackTrace();
			System.out.println("No Records Found for PAIN001 !!");
		}	
	 }
	
	public static void main(String[] args) throws Exception{
		clearActualData();
	}
	
}
