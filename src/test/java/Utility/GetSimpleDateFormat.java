package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GetSimpleDateFormat {
	
	public static String getSimpleDateFormat() throws Exception{
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'13:48:38");
		String dateAsISOString = df.format(date);
		return dateAsISOString;
	}
	
	public static void OpenExcel() throws Exception{
		Runtime.getRuntime().exec("cmd /c start D:/Core_ComponentTool/ISOCodeMappingupdatedErrorLog.xlsx"); 
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println(GetSimpleDateFormat.getSimpleDateFormat());
		OpenExcel();
		
	}
}
