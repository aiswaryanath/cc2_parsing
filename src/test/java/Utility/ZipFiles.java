package Utility;


import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFiles {
	List<String> filesListInDir = new ArrayList<String>();
	
	public static String getCustomisedDateTime(){
 		Date d=new Date();  
 		int year=d.getYear();  
 		int currentYear=year+1900;  
 		int Month = d.getMonth();
 		int _month = Month+1;
 		String _date = d.getDate()+"_"+_month+"_"+currentYear+"_"+d.getHours()+"_"+d.getMinutes()+"_"+d.getSeconds(); 
 		return  _date;
	}
	
	
	public static void WinZip_JSONFiles(){
		File dir = new File("D:\\Core_ComponentTool\\_Output");
        String zipDirName = "D:\\Core_ComponentTool\\Results\\API_Report_"+getCustomisedDateTime()+".zip";
        ZipFiles zipFiles = new ZipFiles();
        zipFiles.zipDirectory(dir, zipDirName);
	}
	
	private void zipDirectory(File dir, String zipDirName) {
        try {
            populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : filesListInDir){
                System.out.println("Zipping "+filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	 private void populateFilesList(File dir) throws IOException {
	        File[] files = dir.listFiles();
	        for(File file : files){
	            if(file.isFile()) filesListInDir.add(file.getAbsolutePath());
	            else populateFilesList(file);
	        }
	    }
	 
	 private static void zipSingleFile(File file, String zipFileName) {
	        try {
	            //create ZipOutputStream to write to the zip file
	            FileOutputStream fos = new FileOutputStream(zipFileName);
	            ZipOutputStream zos = new ZipOutputStream(fos);
	            //add a new Zip Entry to the ZipOutputStream
	            ZipEntry ze = new ZipEntry(file.getName());
	            zos.putNextEntry(ze);
	            //read the file and write to ZipOutputStream
	            FileInputStream fis = new FileInputStream(file);
	            byte[] buffer = new byte[1024];
	            int len;
	            while ((len = fis.read(buffer)) > 0) {
	                zos.write(buffer, 0, len);
	            }

	            //Close the zip entry to write to zip file
	            zos.closeEntry();
	            //Close resources
	            zos.close();
	            fis.close();
	            fos.close();
	            System.out.println(file.getCanonicalPath()+" is zipped to "+zipFileName);
	            
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	    }
	
}
