package Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HandleWebElements {

	public static void SuccessfullClickJS(WebDriver driver,String Locator){
		try{
		int count =0;
		 while(count<10){
			 if (driver.findElement(By.xpath(Locator)).isDisplayed()) {
				 System.out.println("Element found!!");
				 	((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(Locator)));
				 break;
			}else{
				System.out.println(count);
				count ++;
			}
		 }
	}catch (Exception e) {
		System.out.println("Exception checking again!!"+e.toString());
//		HandleWebElements.SuccessfullClickFE(driver, Locator);
	}
  }
	
	public static void SuccessfullClickFE(WebDriver driver,String Locator){
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try{
		int count =0;
		 while(count<10){
			 if (driver.findElement(By.xpath(Locator)).isDisplayed()) {
				 System.out.println("Element found!!");
					WebElement link = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
					link.click();
				 break;
			}else{
				System.out.println(count);
				count ++;
			}
		 }
	}catch (Exception e) {
		System.out.println("Exception checking again!!"+e.toString());
//		HandleWebElements.SuccessfullClickJS(driver, Locator);
	}
  }
	
	
	public static void SuccessfullClickActions(WebDriver driver,String Locator){
		try{
		int count =0;
		 while(count<10){
			 if (driver.findElement(By.xpath(Locator)).isDisplayed()) {
				 System.out.println("Element found!!");
					 Actions actions=new Actions(driver);
					 actions.moveToElement(driver.findElement(By.xpath(Locator))).click().perform();
				 break;
			}else{
				System.out.println(count);
				count ++;
			}
		 }
	}catch (Exception e) {
		System.out.println("Exception checking again!!"+e.toString());
//		HandleWebElements.SuccessfullClickJS(driver, Locator);
	}
  }
	
	
	

	
	
	
	
}
