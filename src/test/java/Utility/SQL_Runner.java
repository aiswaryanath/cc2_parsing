package Utility;

import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import com.ibatis.common.jdbc.ScriptRunner;

import ReadDBConfiguration.ReadDB;

public class SQL_Runner {
	
	public static void PREExecute_QueriesCC2(String SQL_Path) throws Exception {
		Connection connection=null;
		String scriptFilePath = SQL_Path;
		Reader reader = null;
		Statement stmt=null;
		
		String classname = ReadDB.getENV1_OracleDriver();
		String URL = ReadDB.getENV1_ConnName();
		String user = ReadDB.getENV1_Uname();
		String pass = ReadDB.getENV1_Pass();
		
		try {	
				Class.forName(classname);
				connection=DriverManager.getConnection(URL,user,pass);
				stmt = connection.createStatement();
				ScriptRunner scriptExecutor = new ScriptRunner(connection,false,false);
				reader=new BufferedReader(new java.io.FileReader(scriptFilePath));
				scriptExecutor.runScript(reader);
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				 stmt.close();
				 connection.close();
			}	
		}

	public static void main(String argv[]) throws Exception {
		PREExecute_QueriesCC2("D:/Core_ComponentTool/Test_Data/AutomationTest_dataPain001.xlsx");
	}

}
