package Utility;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Constants_pack.Constants;

public class Data_Connection {
	
	public static Connection getConnection(Connection connection) throws FileNotFoundException, IOException {
		try{
			Fillo fill=new Fillo();
			connection = fill.getConnection(Constants._ExcelPath);
		}catch (Exception e) {
			e.getStackTrace();
		}	
		return connection;
	}

	public static final Recordset executeQuery(final Connection connection,String _str) {
	    try {
	        return connection.executeQuery(_str);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
