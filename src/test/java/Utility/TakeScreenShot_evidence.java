package Utility;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class TakeScreenShot_evidence {
	
	public static void Evidences(WebDriver driver,String Evidence_name) throws Exception{
		try{
			File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			Files.copy(src, new File("D:/Core_ComponentTool/_Output/ScreenShot/"+Evidence_name+".png"));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
