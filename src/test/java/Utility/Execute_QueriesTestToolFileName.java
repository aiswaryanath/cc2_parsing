package Utility;

import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Recordset;
import ReadDBConfiguration.ReadDB;
import java.sql.DriverManager;
import Utility.Data_Connection;

public class Execute_QueriesTestToolFileName {
	
	static ArrayList<String> Source_sql =new ArrayList<String>();
	
	public static void Get_SQLQueriesTestToolFile() throws Exception{
		Connection connection=null;
		System.setProperty("ROW", "2");
		Connection conn = Data_Connection.getConnection(connection); 
		if (conn==null) {
			System.out.println("Connection has Failed!!");
		}else{
			System.out.println("Connection has Passesd");
			Recordset Source_Records = Data_Connection.executeQuery(conn,"Select * from PAIN001 where RUN_CASE = 'Y'");
			
			while (Source_Records.next()) {
	    		  String Source_TESTCASEID = Source_Records.getField("Test_ID");
	    		  String Source_FORMATNAME = "JSON";
	    		  String Source_CHANNEL = Source_Records.getField("channelId");
	    		  String Source_LSI = "NULL";
	    		  String Source_CLIENT_FILE_REF = "NULL";
	    		  String Source_BIC_CODE = "NULL";
	    		  String Source_BBDID = "NULL";
	    		  String Source_CUST_REF = "NULL";
	    		  String Source_UNIQUE_ID = "NULL";
	    		  String Source_FILENAME = Source_Records.getField("FILE_NAME");
	    		  String Source_TIMESTAMP = "NULL";
	    		  String Source_APPLIED_RULES = "NULL";
	    		  String Source_ERROR_CODES = "NULL";
	    		  String Source_MODIFIED_FIELD = "NULL";
	    		  String Source_FOLDER_NAME = "GOW_ACCNT";
	    		  String Source_SUITE_NAME = "GOW_ACCNT";

	    		  String _SQL_source = "Insert into test_tool_file_name (TESTCASEID,FORMATNAME,CHANNEL,LSI,CLIENT_FILE_REF,BIC_CODE,BBDID,CUST_REF,UNIQUE_ID,FILENAME,TIMESTAMP,APPLIED_RULES,ERROR_CODES,MODIFIED_FIELD,FOLDER_NAME,SUITE_NAME) values ("
	    					+ "'"+Source_TESTCASEID+"',"
	    					+ "'"+Source_FORMATNAME+"',"
	    					+ "'"+Source_CHANNEL+"',"
	    					+ "'"+Source_LSI+"',"
	    					+ "'"+Source_CLIENT_FILE_REF+"',"
	    					+ "'"+Source_BIC_CODE+"',"
	    					+ "'"+Source_BBDID+"',"
	    					+ "'"+Source_CUST_REF+"',"
	    					+ "'"+Source_UNIQUE_ID+"',"
	    					+ "'"+Source_FILENAME+"',"
	    					+ "null,"
	    					+ "'"+Source_APPLIED_RULES+"',"
	    					+ "'"+Source_ERROR_CODES+"',"
	    					+ "'"+Source_MODIFIED_FIELD+"',"
	    					+ "'"+Source_FOLDER_NAME+"',"
	    					+ "'"+Source_SUITE_NAME+"')";
	    		  Source_sql.add(_SQL_source);
			}
			Execute_QueriesTestToolFileName();
		}	
	}
	
	public static void GetList_SQL() {
		for (int i = 0; i < Source_sql.size(); i++) {
			System.out.println(Source_sql.get(i));
		}
	}

	public static void Execute_QueriesTestToolFileName() throws Exception {
		String classname = ReadDB.getENV1_OracleDriver();
		String URL = ReadDB.getENV1_ConnName();
		String user = ReadDB.getENV1_Uname();
		String pass = ReadDB.getENV1_Pass();
		
		java.sql.Connection connection=null;
		Statement stmt=null;
		try {	
					Class.forName(classname);
					connection= DriverManager.getConnection(URL,user,pass);
					stmt = ((java.sql.Connection) connection).createStatement();
					System.out.println("INSERT QUERY EXECUTE");
					for (int i = 0; i < Source_sql.size(); i++) {
						stmt.executeUpdate(Source_sql.get(i));
					}	
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				 stmt.close();
				 connection.close();
			}	
		}

	public static void main(String[] args) throws Exception{
		Get_SQLQueriesTestToolFile();
		GetList_SQL();
//		Execute_QueriesTestToolFileName();
	}
}
