/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : CdtrAgt */
/* */
/* File Name : CdtrAgt.java */
/* */
/* Description : CdtrAgt POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAgt;

public class CdtrAgt {
	private FinInstnId FinInstnId;
	
	public FinInstnId getFinInstnId() {
		return FinInstnId;
	}

	public void setFinInstnId(FinInstnId finInstnId) {
		FinInstnId = finInstnId;
	}
	
}
