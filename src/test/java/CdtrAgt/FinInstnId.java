/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : FinInstnId */
/* */
/* File Name : FinInstnId.java */
/* */
/* Description : FinInstnId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAgt;

public class FinInstnId {
	
	private String BICFI;
	private String BICFIX;
	private ClrSysMmbId ClrSysMmbId;
	private String Nm;
	private String NmX;
	private CdtrAgtPstlAdr PstlAdr;
	
	public String getBICFIX() {
		return BICFIX;
	}

	public void setBICFIX(String bICFIX) {
		BICFIX = bICFIX;
	}
	
	public String getBICFI() {
		return BICFI;
	}

	public void setBICFI(String bICFI) {
		BICFI = bICFI;
	}
	
	public String getNmX() {
		return NmX;
	}

	public void setNmX(String nmX) {
		NmX = nmX;
	}

	public CdtrAgtPstlAdr getPstlAdr() {
		return PstlAdr;
	}

	public void setPstlAdr(CdtrAgtPstlAdr pstlAdr) {
		PstlAdr = pstlAdr;
	}

	public String getNm() {
		return Nm;
	}

	public void setNm(String nm) {
		Nm = nm;
	}

	public ClrSysMmbId getClrSysMmbId() {
		return ClrSysMmbId;
	}

	public void setClrSysMmbId(ClrSysMmbId clrSysMmbId) {
		ClrSysMmbId = clrSysMmbId;
	}
	
}
