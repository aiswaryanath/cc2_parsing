/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ClrSysMmbId */
/* */
/* File Name : ClrSysMmbId.java */
/* */
/* Description : ClrSysMmbId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAgt;

public class ClrSysMmbId {
	private ClrSysId ClrSysId;
	private String MmbId;  
	private String MmbIdX;
	
	public String getMmbIdX() {
		return MmbIdX;
	}

	public void setMmbIdX(String mmbIdX) {
		MmbIdX = mmbIdX;
	}

	public String getMmbId() {
		return MmbId;
	}

	public void setMmbId(String mmbId) {
		MmbId = mmbId;
	}

	public ClrSysId getClrSysId() {
		return ClrSysId;
	}

	public void setClrSysId(ClrSysId clrSysId) {
		ClrSysId = clrSysId;
	}
}
