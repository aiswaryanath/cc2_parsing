/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : CdtrAgtPstlAdr */
/* */
/* File Name : CdtrAgtPstlAdr.java */
/* */
/* Description : CdtrAgtPstlAdr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAgt;

import java.util.ArrayList;

public class CdtrAgtPstlAdr {
	private String StrtNm;
	private String BldgNb;
	private String PstCd;
	private String TwnNm;
	private String CtrySubDvsn;
	private String Ctry;
	private String CtryX;
	//	private String AdrLine;
	private ArrayList AdrLine;

	
	public String getCtryX() {
		return CtryX;
	}
	public void setCtryX(String ctryX) {
		CtryX = ctryX;
	}

public ArrayList getAdrLine() {
		return AdrLine;
	}
	public void setAdrLine(ArrayList adrLine) {
		AdrLine = adrLine;
	}
	//	public String getAdrLine() {
//		return AdrLine;
//	}
//	public void setAdrLine(String adrLine) {
//		AdrLine = adrLine;
//	}
	public String getStrtNm() {
		return StrtNm;
	}
	public void setStrtNm(String strtNm) {
		StrtNm = strtNm;
	}
	public String getBldgNb() {
		return BldgNb;
	}
	public void setBldgNb(String bldgNb) {
		BldgNb = bldgNb;
	}
	public String getPstCd() {
		return PstCd;
	}
	public void setPstCd(String pstCd) {
		PstCd = pstCd;
	}
	public String getTwnNm() {
		return TwnNm;
	}
	public void setTwnNm(String twnNm) {
		TwnNm = twnNm;
	}
	public String getCtrySubDvsn() {
		return CtrySubDvsn;
	}
	public void setCtrySubDvsn(String ctrySubDvsn) {
		CtrySubDvsn = ctrySubDvsn;
	}
	public String getCtry() {
		return Ctry;
	}
	public void setCtry(String ctry) {
		Ctry = ctry;
	}
}
