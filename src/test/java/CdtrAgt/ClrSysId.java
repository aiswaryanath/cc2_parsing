/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ClrSysId */
/* */
/* File Name : ClrSysId.java */
/* */
/* Description : ClrSysId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/



package CdtrAgt;

public class ClrSysId {
	private String Cd;
	private String CdX;
	
	private String Prtry;
	private String PrtryX;
	

	public String getPrtry() {
		return Prtry;
	}

	public void setPrtry(String prtry) {
		Prtry = prtry;
	}

	public String getPrtryX() {
		return PrtryX;
	}

	public void setPrtryX(String prtryX) {
		PrtryX = prtryX;
	}

	public String getCdX() {
		return CdX;
	}

	public void setCdX(String cdX) {
		CdX = cdX;
	}

	public String getCd() {
		return Cd;
	}

	public void setCd(String cd) {
		Cd = cd;
	}
	
}
