/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : CdtrAcctIBAN */
/* */
/* File Name : CdtrAcctIBAN.java */
/* */
/* Description : CdtrAcctIBAN POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAcctIBAN;

public class CdtrAcctIdIBAN {
	
	private String IBAN;

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}
	
	
}
