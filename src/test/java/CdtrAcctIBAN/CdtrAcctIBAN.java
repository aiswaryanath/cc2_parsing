/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : CdtrAcctIBAN */
/* */
/* File Name : CdtrAcctIBAN.java */
/* */
/* Description : CdtrAcctIBAN POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAcctIBAN;

public class CdtrAcctIBAN {
	private CdtrAcctIdIBAN Id;

	public CdtrAcctIdIBAN getId() {
		return Id;
	}

	public void setId(CdtrAcctIdIBAN id) {
		Id = id;
	}
	
}
