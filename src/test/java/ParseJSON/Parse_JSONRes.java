package ParseJSON;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Api_Operaitons.Update_ExcelResult;
import Utility.ClearCellData;


public class Parse_JSONRes {
	
	public static String readFile(String filename) {
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}

	public static String getErrorCode(String TestCaseID,String Expected_Error_Code) throws Exception{
		System.setProperty("ROW", "2");
		Fillo fill=new Fillo(); 
		Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
		Recordset rs=conn.executeQuery("Select * from PAIN001 where Test_ID = '"+TestCaseID+"'");
		String Error_Code = null;
		while (rs.next()) {
			Error_Code = rs.getField(Expected_Error_Code);
		}	
		return Error_Code;	
	}

	public static String _JSONParse(String TestCaseID) throws Exception{
	    try{		
			StringBuilder errorLogs = new StringBuilder();
			String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_pain001_RESP.json");
			JSONObject json_object=new JSONObject(toString_RespJSON_Structureval);
			
			String Document = json_object.getJSONObject("Document").toString(); 
			JSONObject Document_ob=new JSONObject(Document);
			
			String CstmrPmtStsRpt  = Document_ob.getJSONObject("CstmrPmtStsRpt").toString();
			
			//GrpHdr
			JSONObject GrpHdr_ob = new JSONObject(CstmrPmtStsRpt);
			String GrpHdr_obval = GrpHdr_ob.getJSONObject("GrpHdr").toString();
//			System.out.println("GrpHdr_obval:"+GrpHdr_obval);
			
			JSONObject MsgId_val = new JSONObject(GrpHdr_obval);
			String MsgId_valC = MsgId_val.get("MsgId").toString();
			String CreDtTmC = MsgId_val.get("CreDtTm").toString();
//			System.out.println(MsgId_valC);
//			System.out.println(CreDtTmC);
			
			String InitgPty_val_ob= MsgId_val.getJSONObject("InitgPty").toString();
//			System.out.println("InitgPty:"+InitgPty_val_ob);
			
//			JSONObject Nm_val = new JSONObject(InitgPty_val_ob);
//			String Nm_valC = Nm_val.get("Nm").toString(); 
//			System.out.println(Nm_valC);
			
//			String Id_OB = Nm_val.getJSONObject("Id").toString();
//			System.out.println(Id_OB);
			
//			JSONObject OrgId_OB = new JSONObject(Id_OB);
//			String OrgId_valC = OrgId_OB.getJSONObject("OrgId").toString();
			
//			JSONObject BICOrBEI_VAL = new JSONObject(OrgId_valC);
//			System.out.println("BICOrBEI:"+BICOrBEI_VAL.get("BICOrBEI"));
			
//			OrgnlGrpInfAndSts
			JSONObject OrgnlGrpInfAndSts = new JSONObject(CstmrPmtStsRpt);
			String OrgnlGrpInfAndSts_ob = OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts").toString();
//			System.out.println(OrgnlGrpInfAndSts_ob);
			
			JSONObject obj_NACK = new JSONObject(OrgnlGrpInfAndSts_ob);
			
			String _Nack = obj_NACK.getString("OrgnlMsgNmId"); 
//			System.out.println(_Nack);
			
			//Update Excel NACK
			Update_ExcelResult.Update_DataReportAck(TestCaseID, _Nack, "Actual_Acknowledge");
			
			String _RJCT = obj_NACK.getString("GrpSts"); 
//			System.out.println(_RJCT);
			
			Update_ExcelResult.Update_DataReportAck(TestCaseID, _RJCT, "Actual_GrpSts");
			
			JSONArray StsRsnInf_arr = obj_NACK.getJSONArray("StsRsnInf"); 
			
//			System.out.println(StsRsnInf_arr.length());
			ArrayList<String> StsRsnInf_res=new ArrayList<String>();
			StsRsnInf_res.add(StsRsnInf_arr.getJSONObject(0).get("AddtlInf").toString());
//			System.out.println(StsRsnInf_res.get(0));
			
			Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_res.get(0), "Actual_FileStatus");
			
//			ArrayList<String> StsRsnInf_respCode=new ArrayList<String>();
//			ArrayList<String> StsRsnInf_respDesc=new ArrayList<String>();
			
				for (int i = 1; i < StsRsnInf_arr.length(); i++) {
//					StsRsnInf_respDesc.add(StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString());
//					StsRsnInf_respCode.add(StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString());
					
					if (getErrorCode(TestCaseID,"Expected_Error_Code").equalsIgnoreCase(StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString())) {
						Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Actual_Error_Code");
					}
					
					if (getErrorCode(TestCaseID,"Expected_Error_Desc").equalsIgnoreCase(StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString())) {
						Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString(), "Actual_Error_Desc");
					}
				}
				
//Payment
if (OrgnlGrpInfAndSts.has("OrgnlPmtInfAndSts")) {
		String OrgnlPmtInfAndSts_ob = OrgnlGrpInfAndSts.getJSONObject("OrgnlPmtInfAndSts").toString();
//		System.out.println("OrgnlPmtInfAndSts_ob:\n"+OrgnlPmtInfAndSts_ob);
		JSONObject StsRsnInf_obj = new JSONObject(OrgnlPmtInfAndSts_ob);
				
		JSONArray StsRsnInfpmt_arr=null;
		if (StsRsnInf_obj.has("StsRsnInf")) {
			
				StsRsnInfpmt_arr = StsRsnInf_obj.getJSONArray("StsRsnInf");
//				System.out.println(StsRsnInfpmt_arr);
				
				for (int i = 0; i < StsRsnInfpmt_arr.length(); i++) {
					
					if (getErrorCode(TestCaseID,"Expected_Error_Code").equalsIgnoreCase(StsRsnInfpmt_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString())) {
						Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInfpmt_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Actual_Error_Code");
					}
					
					if (getErrorCode(TestCaseID,"Expected_Error_Desc").equalsIgnoreCase(StsRsnInfpmt_arr.getJSONObject(i).get("AddtlInf").toString())) {
						Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInfpmt_arr.getJSONObject(i).get("AddtlInf").toString(), "Actual_Error_Desc");
					}
				}
	    }
		
//Transaction 
				//changed
				JSONObject TxInfAndSts_obj = new JSONObject(OrgnlPmtInfAndSts_ob);
				
				JSONArray StsRsnInf_Txnarr = null;
				if (TxInfAndSts_obj.has("StsRsnInf")) {
					StsRsnInf_Txnarr=TxInfAndSts_obj.getJSONArray("StsRsnInf");
					
					for (int i = 0; i < StsRsnInf_Txnarr.length(); i++) {
						
						if (getErrorCode(TestCaseID,"Expected_Error_Code").equalsIgnoreCase(StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString())) {
							Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Actual_Error_Code");
						}
						
						if (getErrorCode(TestCaseID,"Expected_Error_Desc").equalsIgnoreCase(StsRsnInf_Txnarr.getJSONObject(i).get("AddtlInf").toString())) {
							Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_Txnarr.getJSONObject(i).get("AddtlInf").toString(), "Actual_Error_Desc");
						}
					}

				}
		}else{
			if (OrgnlGrpInfAndSts.has("TxInfAndSts")) {
				JSONObject TxInfAndSts_obj = OrgnlGrpInfAndSts.getJSONObject("TxInfAndSts");
				
				if (TxInfAndSts_obj.has("StsRsnInf")) {
					JSONArray StsRsnInf_Txnarr = null;
					StsRsnInf_Txnarr=TxInfAndSts_obj.getJSONArray("StsRsnInf");
					for (int i = 0; i < StsRsnInf_Txnarr.length(); i++) {
						
						if (getErrorCode(TestCaseID,"Expected_Error_Code").equalsIgnoreCase(StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString())) {
							Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Actual_Error_Code");
						}
						
						if (getErrorCode(TestCaseID,"Expected_Error_Desc").equalsIgnoreCase(StsRsnInf_Txnarr.getJSONObject(i).get("AddtlInf").toString())) {
							Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_Txnarr.getJSONObject(i).get("AddtlInf").toString(), "Actual_Error_Desc");
						}
					}
				}
			}else{
//					System.out.println("Start Reading StsRsnInf");
//					System.out.println(OrgnlGrpInfAndSts_ob);
//					JSONObject TxInfAndSts_obj = OrgnlGrpInfAndSts.getJSONObject("TxInfAndSts");
					if (OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts").has("StsRsnInf")) {
//						System.out.println(OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts"));
						
						JSONArray StsRsnInf_Txnarr = null;
						StsRsnInf_Txnarr=OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts").getJSONArray("StsRsnInf");
//						System.out.println(StsRsnInf_Txnarr);
						for (int i = 1; i < StsRsnInf_Txnarr.length(); i++) {
							
							if (getErrorCode(TestCaseID,"Expected_Error_Code").equalsIgnoreCase(StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString())) {
//								System.out.println("PMT Error Code Expected :"+getErrorCode(TestCaseID,"Expected_Error_Code"));
//								System.out.println("PMT Error Code Actual  :"+StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString());
								Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_Txnarr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Actual_Error_Code");
							}
							
							if (getErrorCode(TestCaseID,"Expected_Error_Desc").equalsIgnoreCase(StsRsnInf_Txnarr.getJSONObject(i).get("AddtlInf").toString())) {
								Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_Txnarr.getJSONObject(i).get("AddtlInf").toString(), "Actual_Error_Desc");
							}
						}
						
					}
					
			}
		}
		
/*				if (StsRsnInf_obj.has("StsRsnInf")) {
					
					StsRsnInf_Txnarr = TxInfAndSts_Str.getJSONArray("StsRsnInf");
					System.out.println(StsRsnInfpmt_arr);
					
					for (int i = 0; i < StsRsnInfpmt_arr.length(); i++) {
						
						if (getErrorCode(TestCaseID,"Expected_Error_Code").equalsIgnoreCase(StsRsnInfpmt_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString())) {
							System.out.println("PMT Error Code Expected :"+getErrorCode(TestCaseID,"Expected_Error_Code"));
							System.out.println("PMT Error Code Actual  :"+StsRsnInfpmt_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString());
							Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInfpmt_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Actual_Error_Code");
						}
						
						if (getErrorCode(TestCaseID,"Expected_Error_Desc").equalsIgnoreCase(StsRsnInfpmt_arr.getJSONObject(i).get("AddtlInf").toString())) {
							Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInfpmt_arr.getJSONObject(i).get("AddtlInf").toString(), "Actual_Error_Desc");
						}
					}
		    }
*/				
				
//			System.out.println("CHECK FROM ERROR:1:"+StsRsnInf_respCode.get(0)+":"+StsRsnInf_respDesc.get(0));
//			Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_respDesc.get(0), "Actual_Error_Desc");
//			Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_respCode.get(0), "Actual_Error_Code");
			
			}catch (Exception e) {
			//	System.out.println(e.toString());
				Update_ExcelResult.Update_DataReportAck(TestCaseID, e.toString(), "Actual_Error_Desc");
				Update_ExcelResult.Update_DataReportAck(TestCaseID, e.toString(), "Actual_Error_Code");
		  }
		 return "";
		}	
		
	public static String _JsonCheckpoint(String TestCaseID) throws Exception{
		String _acknowledge=null;
	    try{		
			StringBuilder errorLogs = new StringBuilder();
			String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_pain001_RESP.json");
			
			JSONObject json_object=new JSONObject(toString_RespJSON_Structureval);
			
			String Document = json_object.getJSONObject("Document").toString(); 
			JSONObject Document_ob=new JSONObject(Document);
			
			String CstmrPmtStsRpt  = Document_ob.getJSONObject("CstmrPmtStsRpt").toString();
			
			//GrpHdr
			JSONObject GrpHdr_ob = new JSONObject(CstmrPmtStsRpt);
			String GrpHdr_obval = GrpHdr_ob.getJSONObject("GrpHdr").toString();
//			System.out.println("GrpHdr_obval:"+GrpHdr_obval);
			
			JSONObject MsgId_val = new JSONObject(GrpHdr_obval);
			String MsgId_valC = MsgId_val.get("MsgId").toString();
			String CreDtTmC = MsgId_val.get("CreDtTm").toString();
//			System.out.println(MsgId_valC);
//			System.out.println(CreDtTmC);
		if (MsgId_val.has("InitgPty")) {
			String InitgPty_val_ob= MsgId_val.getJSONObject("InitgPty").toString();
//			System.out.println("InitgPty:"+InitgPty_val_ob);
			
			JSONObject Nm_val = new JSONObject(InitgPty_val_ob);
			
			if (Nm_val.has("Nm")) {
				String Nm_valC = Nm_val.get("Nm").toString(); 
//				System.out.println(Nm_valC);
				if(Nm_val.has("Id")){	
					String Id_OB = Nm_val.getJSONObject("Id").toString();
//					System.out.println(Id_OB);
					JSONObject OrgId_OB = new JSONObject(Id_OB);
					String OrgId_valC = OrgId_OB.getJSONObject("OrgId").toString();
					JSONObject BICOrBEI_VAL = new JSONObject(OrgId_valC);
//					System.out.println("BICOrBEI:"+BICOrBEI_VAL.get("AnyBIC"));
				}
			}
		}			
//			OrgnlGrpInfAndSts
			JSONObject OrgnlGrpInfAndSts = new JSONObject(CstmrPmtStsRpt);
			String OrgnlGrpInfAndSts_ob = OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts").toString();
//			System.out.println(OrgnlGrpInfAndSts_ob);
			
			JSONObject obj_NACK = new JSONObject(OrgnlGrpInfAndSts_ob);
			
//			_acknowledge = obj_NACK.getString("OrgnlMsgNmId");
			_acknowledge = obj_NACK.getString("GrpSts");
			
//			System.out.println(_acknowledge);
			
	    }catch (Exception e) {
			e.printStackTrace();
			Update_ExcelResult.Update_DataReportAck(TestCaseID, e.getLocalizedMessage(), "Actual_Error_Desc");
		}
		return _acknowledge;
	}
	
	public static String _NegativeJsonresponse(String TestCaseID) throws Exception{
		String _acknowledge=null;
	    try{		
			StringBuilder errorLogs = new StringBuilder();
			String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_pain001_RESP.json");
			JSONObject json_object=new JSONObject(toString_RespJSON_Structureval);
			JSONArray StsRsnInf_arr = json_object.getJSONArray("StsRsnInf"); 
			//System.out.println(StsRsnInf_arr.length());
			ArrayList<String> StsRsnInf_res=new ArrayList<String>();
			StsRsnInf_res.add(StsRsnInf_arr.getJSONObject(0).get("AddtlInf").toString());
			//System.out.println(StsRsnInf_res.get(0));
	    }catch (Exception e) {
			e.printStackTrace();
		}
		return _acknowledge;
	}
	
	public static String _GetMsgid_resp(String TestCaseID) throws Exception{
		String OrgnlMsgId_val=null;
	    try{		
			StringBuilder errorLogs = new StringBuilder();
			String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_pain001_RESP.json");
			JSONObject json_object=new JSONObject(toString_RespJSON_Structureval);
			
			String Document = json_object.getJSONObject("Document").toString(); 
			JSONObject Document_ob=new JSONObject(Document);
			
			String CstmrPmtStsRpt  = Document_ob.getJSONObject("CstmrPmtStsRpt").toString();
			
			//GrpHdr
			JSONObject GrpHdr_ob = new JSONObject(CstmrPmtStsRpt);
			String GrpHdr_obval = GrpHdr_ob.getJSONObject("GrpHdr").toString();
			//System.out.println("GrpHdr_obval:"+GrpHdr_obval);
			
			JSONObject MsgId_val = new JSONObject(GrpHdr_obval);
			String MsgId_valC = MsgId_val.get("MsgId").toString();
			String CreDtTmC = MsgId_val.get("CreDtTm").toString();
			//System.out.println(MsgId_valC);
			//System.out.println(CreDtTmC);
		if (MsgId_val.has("InitgPty")) {
			
			String InitgPty_val_ob= MsgId_val.getJSONObject("InitgPty").toString();
			//System.out.println("InitgPty:"+InitgPty_val_ob);
			
			JSONObject Nm_val = new JSONObject(InitgPty_val_ob);
			
			if (Nm_val.has("Nm")) {
				String Nm_valC = Nm_val.get("Nm").toString(); 
				System.out.println(Nm_valC);
				if(Nm_val.has("Id")){	
					String Id_OB = Nm_val.getJSONObject("Id").toString();
					//System.out.println(Id_OB);
					JSONObject OrgId_OB = new JSONObject(Id_OB);
					String OrgId_valC = OrgId_OB.getJSONObject("OrgId").toString();
					JSONObject BICOrBEI_VAL = new JSONObject(OrgId_valC);
					//System.out.println("BICOrBEI:"+BICOrBEI_VAL.get("AnyBIC"));
				}
			}
		}			
//			OrgnlGrpInfAndSts
			JSONObject OrgnlGrpInfAndSts = new JSONObject(CstmrPmtStsRpt);
			String OrgnlGrpInfAndSts_ob = OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts").toString();
			//System.out.println(OrgnlGrpInfAndSts_ob);
			
			JSONObject StsRsnInf = new JSONObject(OrgnlGrpInfAndSts_ob);
			
			OrgnlMsgId_val = StsRsnInf.get("OrgnlMsgId").toString();
			//System.out.println("OrgnlMsgId_val:"+OrgnlMsgId_val);
//			System.out.println(MsgId_valC);
	    }catch (Exception e) {
			e.printStackTrace();
		}
	    return OrgnlMsgId_val;
	}
	
	public static void main(String[] args) throws Exception{
/*		System.setProperty("ROW", "2");
		ClearCellData.clearActualData();
		Fillo fill=new Fillo();
		Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
		Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
		 while (rs.next()) {
			 _JSONParse(rs.getField("Test_ID"));
		 }
*/
				_JSONParse("TC243");	
	}
}
