package ParseJSON;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ParseRecordExcel.Update_ExcelResult;

public class StructureValidations {
	public static boolean isJSONValid(String test) throws Exception{
	    try {
	        new JSONObject(test);
	    } catch (JSONException ex) {
	        try {
	            new JSONArray(test);
	        } catch (JSONException ex1) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static String getSaltString(int _NumLength) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < _NumLength) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return "AUTO"+saltStr;
    }
	
	public static String readFile(String filename) {
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
	public static String getSimpleDateFormat() throws Exception{
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String dateAsISOString = df.format(date);
		//System.out.println(dateAsISOString);
		return dateAsISOString;
	}	
	
public static String _JSONStructure_Validation(String TestCaseID) throws Exception{
    try{		
		StringBuilder errorLogs = new StringBuilder();
		String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_pain001_RESP.json");
		JSONObject json_object=new JSONObject(toString_RespJSON_Structureval);
		
		String Document = json_object.getJSONObject("Document").toString(); 
		JSONObject Document_ob=new JSONObject(Document);
		
		String CstmrPmtStsRpt  = Document_ob.getJSONObject("CstmrPmtStsRpt").toString();

		//GrpHdr
		JSONObject GrpHdr_ob = new JSONObject(CstmrPmtStsRpt);
		String GrpHdr_obval = GrpHdr_ob.getJSONObject("GrpHdr").toString();
//		System.out.println("GrpHdr_obval:"+GrpHdr_obval);

		JSONObject MsgId_val = new JSONObject(GrpHdr_obval);
		String MsgId_valC = MsgId_val.get("MsgId").toString();
		String CreDtTmC = MsgId_val.get("CreDtTm").toString();
//		System.out.println(MsgId_valC);
//		System.out.println(CreDtTmC);
		
		String InitgPty_val_ob= MsgId_val.getJSONObject("InitgPty").toString();
//		System.out.println("InitgPty:"+InitgPty_val_ob);
		
		JSONObject Nm_val = new JSONObject(InitgPty_val_ob);
		if (Nm_val.has("Nm")) {
			String Nm_valC = Nm_val.get("Nm").toString(); 
//			System.out.println(Nm_valC);
			if (Nm_val.has("Id")) {
				String Id_OB = Nm_val.getJSONObject("Id").toString();
//				System.out.println(Id_OB);
				
				JSONObject OrgId_OB = new JSONObject(Id_OB);
				String OrgId_valC = OrgId_OB.getJSONObject("OrgId").toString();
				
//				JSONObject BICOrBEI_VAL = new JSONObject(OrgId_valC);
//				System.out.println("BICOrBEI:"+BICOrBEI_VAL.get("BICOrBEI"));
			}
		}
		
//		OrgnlGrpInfAndSts
		JSONObject OrgnlGrpInfAndSts = new JSONObject(CstmrPmtStsRpt);
		String OrgnlGrpInfAndSts_ob = OrgnlGrpInfAndSts.getJSONObject("OrgnlGrpInfAndSts").toString();
//		System.out.println(OrgnlGrpInfAndSts_ob);
		
		JSONObject StsRsnInf = new JSONObject(OrgnlGrpInfAndSts_ob);
		
//		String OrgnlMsgId_val = StsRsnInf.get("OrgnlMsgId").toString();
//		System.out.println("OrgnlMsgId_val:"+OrgnlMsgId_val);
		
		String OrgnlMsgNmId_val = StsRsnInf.get("OrgnlMsgNmId").toString();
//		System.out.println("OrgnlMsgNmId_val:"+OrgnlMsgNmId_val);
		
		String GrpSts_val = StsRsnInf.get("GrpSts").toString();
//		System.out.println("GrpSts_val:"+GrpSts_val);
		
		Update_ExcelResult.Update_DataReportAck(TestCaseID, GrpSts_val, "Actual_GrpSts");
		
		//Update ACK Result in Excel
		Update_ExcelResult.Update_DataReportAck(TestCaseID, OrgnlMsgNmId_val, "Actual_Acknowledge");
		
		JSONArray StsRsnInf_arr = StsRsnInf.getJSONArray("StsRsnInf"); 
		
		String _Ob = StsRsnInf_arr.get(0).toString();
//		System.out.println(_Ob);
		
		JSONObject _Ob_result = new JSONObject(_Ob);
		
		String _ObKey = _Ob_result.getString("AddtlInf");
//		System.out.println(_ObKey);
		
		//Update File Accepted Result in Excel
		if (_ObKey.equals("File Accepted")) {
//			System.out.println("AddtlInf: ''File Accepted'' Msg Valid");
			Update_ExcelResult.Update_DataReportAck(TestCaseID, _ObKey, "Actual_FileStatus");
		}else{
			Update_ExcelResult.Update_DataReportAck(TestCaseID, _ObKey, "Actual_FileStatus");
		}
		
//OrgnlPmtInfAndSts
//		JSONObject OrgnlPmtInfAndSts = new JSONObject(CstmrPmtStsRpt);
//		String OrgnlPmtInfAndSts_ob = OrgnlPmtInfAndSts.getString("OrgnlPmtInfAndSts");
//		
//		JSONObject TxInfAndSts_ob = new JSONObject(OrgnlPmtInfAndSts_ob);
//		String TxInfAndSts_val = TxInfAndSts_ob.getString("TxInfAndSts").toString(); 
//		System.out.println(TxInfAndSts_val);
//		
//		JSONObject OrgnlInstrId = new JSONObject(TxInfAndSts_val);
//		String OrgnlInstrId_val= OrgnlInstrId.get("OrgnlInstrId").toString();
//		
//		JSONObject OrgnlEndToEndId = new JSONObject(TxInfAndSts_val);
//		String OrgnlEndToEndId_val= OrgnlEndToEndId.get("OrgnlEndToEndId").toString();
//
//		System.out.println(OrgnlInstrId_val+"\n"+OrgnlEndToEndId_val);
//		
//		JSONObject OrgnlPmtInfId_val = new JSONObject(OrgnlPmtInfAndSts_ob);
//
//		String OrgnlPmtInfId_kv= OrgnlPmtInfId_val.get("OrgnlPmtInfId").toString();
//
//		System.out.println(OrgnlPmtInfId_kv);

		}catch (Exception e) {
			System.out.println(e.toString());
	  }
	 return "";
	}	
	
	public static void main(String[] args) throws Exception{
		_JSONStructure_Validation("JSON_Pain001v05_CMO_POSITIVE");
//		getSimpleDateFormat();
	}
}

