/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : Update_ExcelResult */
/* */
/* File Name : Update_ExcelResult.java */
/* */
/* Description :Update the record in excel sheet for Expected Results */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Api_Operaitons;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Constants_pack.Constants;

public class Update_ExcelResult {
	
	public static void Update_DataReportAck(String TESTCASEID,String Actual_Result,String column_name) throws Exception{
		try{
		System.setProperty("ROW", "2");
		Fillo fill=new Fillo();
		Connection conn =fill.getConnection(Constants._ExcelPath);
		conn.executeUpdate("update PAIN001 SET "+column_name+" = '"+Actual_Result+"' where Test_ID  = '"+TESTCASEID+"'");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void clearCellValues(String column_name ,String TESTCASEID) throws Exception{
		try{
		System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			conn.executeUpdate("update PAIN001 SET "+column_name+" = ''  where Test_ID  = '"+TESTCASEID+"'");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void clearCellValues(String TESTCASEID) throws Exception{
		try{
		System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			conn.executeUpdate("update PAIN001 SET Actual_Error_Desc = '', Actual_Error_Code = '', Actual_FileStatus = '', Actual_GrpSts = '', Actual_Acknowledge = '', Server_ResponseCode = ''  where Test_ID  = '"+TESTCASEID+"'");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getCountRow() throws Exception{
		try{
			FileInputStream Fip = new FileInputStream(new File(Constants_pack.Constants._ExcelPath));
			HSSFWorkbook workbook = new HSSFWorkbook(Fip);
			HSSFSheet sheet =workbook.getSheet("PAIN001");
//			HSSFRow row=sheet.createRow(0);
//			Cell cell = row.createCell(0);
			int noOfColumns = sheet.getLastRowNum();
			System.out.println(noOfColumns);
			workbook.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void clearActualData() throws Exception{
		try{
			System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE  = 'Y'");
			int count=0;
			while (rs.next()) {
				clearCellValues(rs.getField("Test_ID"));
				System.out.println(count++);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}	
	 }
	}
