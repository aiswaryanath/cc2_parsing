/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : POST_requestApi */
/* */
/* File Name : POST_requestApi.java */
/* */
/* Description :POST API from Input Stream and get response */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Api_Operaitons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.management.openmbean.InvalidOpenTypeException;
import javax.swing.plaf.synth.SynthSeparatorUI;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import Api_Operaitons.Update_ExcelResult;
import Busness_Validation.FileProcOpnValidation;
import JSONStructure_PAIN001.Document;
import ParseJSON.Parse_JSONRes;
import ParseJSON.StructureValidations;
import ReadDBConfiguration.ReadDB;
import groovy.transform.Undefined;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class POST_requestApi2 {
	public static ArrayList<Integer> ob=new ArrayList<Integer>();
	public static Response response;
	public static PrintStream json_console_add = null;
//	static String _salt_Key  = StructureValidations.getSaltString(10);
	public static String readFile(String filename) {
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
	
public static void ExecutePOST_API(String TestCaseID,String API_URL,String channelId,String service_type,String flow_id) throws Exception {		
	boolean flag_res = true;
   try{
	    String toString_addJSON = readFile("D:\\Core_ComponentTool\\_Output\\"+TestCaseID+"_Pain001_REQ.json");
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setBody(toString_addJSON);
		builder.setContentType("application/json; charset=UTF-8");
		RequestSpecification request = builder.build();
		request.headers("channelId",channelId);
//		request.headers("requestId",Payment_pain001.MSG_ID_Copy);
//		request.headers("requestId",requestId);
//		request.headers("service_type",service_type);
		request.headers("flow_id",flow_id);

		Thread.sleep(1000); 
		    response = RestAssured.given().spec(request).when().post(API_URL);
//		    json_console_add = new PrintStream(new File("D:/Core_ComponentTool/_Output/"+TestCaseID+"_pain001_RESP.json"));
//			System.setOut(json_console_add);
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(response.body().asString());
			String prettyJsonString = gson.toJson(je);

//			System.out.println(prettyJsonString);
//			JSONObject json = new JSONObject(prettyJsonString);
//			String xml = XML.toString(json);
//			 	Source xmlInput = new StreamSource(new StringReader(xml));
//		        StringWriter stringWriter = new StringWriter();
//		        StreamResult xmlOutput = new StreamResult(stringWriter);
//		        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//		        transformerFactory.setAttribute("indent-number", 2);
//		        Transformer transformer = transformerFactory.newTransformer(); 
//		        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//		        transformer.transform(xmlInput, xmlOutput);
//		        System.out.println(xmlOutput.getWriter().toString());
//			String result = java.net.URLDecoder.decode(prettyJsonString, "UTF-8");

			FileWriter fw=new FileWriter("D:\\Core_ComponentTool\\_Output\\"+TestCaseID+"_pain001_RESP.json");    
				fw.write(prettyJsonString);
			fw.close();    
			
		    json_console_add = new PrintStream(new File("D:/Core_ComponentTool/Test_Data/Log.txt"));
			System.setOut(json_console_add);
//			json_console_add.close();

//	System.out.println(prettyJsonString);
	System.out.println(Integer.parseInt(ReadDB.getWAITIME()));		
	Thread.sleep(Integer.parseInt(ReadDB.getWAITIME()));
	System.out.println("getStatusCode:"+response.getStatusCode());
	int Status_code = response.getStatusCode();
	System.out.println("Respopnse Time:"+response.getTime());
	String Status_code_Str = Integer.toString(Status_code);
	Update_ExcelResult.Update_DataReportAck(TestCaseID, Status_code_Str, "Server_ResponseCode");
	try{
		String _Acknowledge = Parse_JSONRes._JsonCheckpoint(TestCaseID);
		System.out.println(_Acknowledge);
		
		if(StructureValidations.isJSONValid(prettyJsonString)){	
				switch (_Acknowledge) {
				case "ACCP":
						StructureValidations._JSONStructure_Validation(TestCaseID);
						FileProcOpnValidation.getdata_FileProcOpn(TestCaseID,Parse_JSONRes._GetMsgid_resp(TestCaseID),_Acknowledge);
					break;
				case "RJCT":
						Parse_JSONRes._JSONParse(TestCaseID);
						FileProcOpnValidation.getdata_FileProcOpn(TestCaseID,Parse_JSONRes._GetMsgid_resp(TestCaseID),_Acknowledge);
					break;
				default:
					break;
				}
		}else{
			System.out.println("Invalid JSON Request :: Need to report in Excel");
		  }
		json_console_add.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}catch(Exception ae){
		ae.printStackTrace();
	}
}

public static void main(String[] args) throws Exception {
	FileProcOpnValidation.getdata_FileProcOpn("JSON_Pain001v05_CMO_BV-MCM_TC449",Parse_JSONRes._GetMsgid_resp("JSON_Pain001v05_CMO_BV-MCM_TC449"),"ACCP");
//	StructureValidations._JSONStructure_Validation("JSON_Pain001v05_CMO_BV-MCM_TC471");
//	Parse_JSONRes._JSONParse("JSON_Pain001v05_CMO_BV-MCM_TC447");
  }
}






