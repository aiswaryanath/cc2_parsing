package UIAction_Implimentations;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class UIOperation {


	public void perfform(String operation,String XPATH_Locator,String objectType,String value,WebDriver driver) throws Exception{

		switch (operation.toUpperCase()) {
		case "CLICK":
			driver.findElement(this.getObject(objectType,XPATH_Locator)).click();
			break;
		case "SETTEXT":
			driver.findElement(this.getObject(objectType,XPATH_Locator)).sendKeys(value);
			break;
		case "CLEAR":
			driver.findElement(this.getObject(objectType,XPATH_Locator)).clear();
			break;
		case "SWITCHTOWINDOW":
			driver.findElement(this.getObject(objectType,XPATH_Locator)).clear();
			break;
/*		case "SELECT":
			new Select(driver.findElement(this.getObject(XPATH_Locator,objectName,objectType))).selectByValue(value);
			break;
		case "GOTOURL":
			//Get url of application
			driver.get(XPATH_Locator);
			driver.manage().window().maximize();
			break;
		case "GETTEXT":
			//Get text of an element
			driver.findElement(this.getObject(XPATH_Locator,objectName,objectType)).getText();
			break;
		case "SETTEXTNUM":
			//Set text on control
			driver.findElement(this.getObject(XPATH_Locator,objectName,objectType)).sendKeys(value);
			driver.findElement(this.getObject(XPATH_Locator,objectName,objectType)).sendKeys(Keys.BACK_SPACE);
			driver.findElement(this.getObject(XPATH_Locator,objectName,objectType)).sendKeys(Keys.BACK_SPACE);
			driver.findElement(this.getObject(XPATH_Locator,objectName,objectType)).sendKeys(Keys.TAB);
			break;
*/		default:
			break;
		}
	}
	
	private By getObject(String objectType,String XPATH_Locator) throws Exception{
		//Find by xpath
		if(objectType.equalsIgnoreCase("XPATH")){
			return By.xpath(XPATH_Locator);
		}
		//find by class
		else if(objectType.equalsIgnoreCase("CLASSNAME")){
			return By.className(XPATH_Locator);
		}
		//find by name
		else if(objectType.equalsIgnoreCase("NAME")){
			return By.name(XPATH_Locator);
		}
		//Find by css
		else if(objectType.equalsIgnoreCase("CSS")){
			return By.cssSelector(XPATH_Locator);
		}
		//find by link
		else if(objectType.equalsIgnoreCase("LINK")){
			return By.linkText(XPATH_Locator);
		}
		else if(objectType.equalsIgnoreCase("ID")){
			System.out.println(XPATH_Locator);
			return By.id(XPATH_Locator);
		}
		//find by partial link
		else if(objectType.equalsIgnoreCase("PARTIALLINK")){
			return By.partialLinkText(XPATH_Locator);
		}else
		{
			throw new Exception("Wrong object type");
		}
	}
}
