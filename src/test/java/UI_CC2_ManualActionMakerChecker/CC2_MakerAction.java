package UI_CC2_ManualActionMakerChecker;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Constants_pack.Constants;
import Utility.SaltString;
import Utility.TakeScreenShot_evidence;

public class CC2_MakerAction {
	
	
	public static void MarkAsCompleted(WebDriver driver,String IWS_StatusCode) throws Exception{
		//ICN
		driver.findElement(By.id("P162943174-309768222")).sendKeys(SaltString.getRandNo(5));
		
		//IWS Status Code
		driver.findElement(By.id("P162943174-526740975")).sendKeys(IWS_StatusCode);
		
		//UEFTR No
		driver.findElement(By.id("P162943174-1026198110")).sendKeys("20190501-1234-4321-abcd-190501105051");
		
		TakeScreenShot_evidence.Evidences(driver, "DOWNSTREAM_SYSTEM_STATUS");
		
		Thread.sleep(2000);
		//CLICK ON EXCEPTION
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='P162943174-TD303'])")));
		
		//Click on Expand All
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-1]")));
		
		TakeScreenShot_evidence.Evidences(driver, "EXCEPTION");
		
		Thread.sleep(2000);
		//CLICK ON USER REMARKS
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='P162943174-TD336'])")));
		
		//Click on Expand All
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("(//*[@id='PEAL'])[last()]")));

		driver.findElement(By.id("P162943174-335820275")).sendKeys("Send Maker");

		driver.findElement(By.id("P162943174-565944470")).click();

		driver.switchTo().defaultContent();

		WebElement frame2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
		driver.switchTo().frame(frame2);
		System.out.println("Parent Frame:"+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

		driver.switchTo().frame("IwBottomFrame");
		System.out.println("IwBottomFrame:"
				+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
		
		TakeScreenShot_evidence.Evidences(driver, "USER_REMARKS");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-561275385")));

	}

	public static void MarkAsRejected(WebDriver driver) throws Exception{
		//ICN
//		driver.findElement(By.id("P162943174-309768222")).sendKeys(SaltString.getRandNo(5));
		
		//IWS Status Code
//		driver.findElement(By.id("P162943174-526740975")).sendKeys(IWS_StatusCode);
		
		//UEFTR No
//		driver.findElement(By.id("P162943174-1026198110")).sendKeys("20190501-1234-4321-abcd-190501105051");
		
		TakeScreenShot_evidence.Evidences(driver, "DOWNSTREAM_SYSTEM_STATUS");
		
		Thread.sleep(2000);
		//CLICK ON EXCEPTION
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='P162943174-TD303'])")));
		
		//Click on Expand All
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-1]")));
		
		TakeScreenShot_evidence.Evidences(driver, "EXCEPTION");
		
		Thread.sleep(2000);
		//CLICK ON USER REMARKS
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='P162943174-TD336'])")));
		
		//Click on Expand All
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='PEAL'])[last()]")));

		driver.findElement(By.id("P162943174-335820275")).sendKeys("Send Maker");

		driver.findElement(By.id("P162943174-565944470")).click();

		driver.switchTo().defaultContent();

		WebElement frame2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
		driver.switchTo().frame(frame2);
		System.out.println("Parent Frame:"+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

		driver.switchTo().frame("IwBottomFrame");
		System.out.println("IwBottomFrame:"
				+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
		
		TakeScreenShot_evidence.Evidences(driver, "USER_REMARKS");
//		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-561275385")));
		
		//Mark as rejected
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-316706176")));

	}
	
	
	
	public static void MarkAsSIP(WebDriver driver,String IWS_StatusCode) throws Exception{
		try{
		//ICN
		driver.findElement(By.id("P162943174-309768222")).sendKeys(SaltString.getRandNo(5));
		
		//IWS Status Code
		driver.findElement(By.id("P162943174-526740975")).sendKeys(IWS_StatusCode);
		
		//UEFTR No
		driver.findElement(By.id("P162943174-1026198110")).sendKeys("20190501-1234-4321-abcd-190501105051");
		
		TakeScreenShot_evidence.Evidences(driver, "DOWNSTREAM_SYSTEM_STATUS");
		
		Thread.sleep(2000);
		//CLICK ON EXCEPTION
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='P162943174-TD303'])")));
		
		//Click on Expand All
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-1]")));
		
		TakeScreenShot_evidence.Evidences(driver, "EXCEPTION");
		
		Thread.sleep(2000);
		//CLICK ON USER REMARKS
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='P162943174-TD336'])")));
		
		//Click on Expand All
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@id='PEAL'])[last()]")));

		driver.findElement(By.id("P162943174-335820275")).sendKeys("Send Maker");

		driver.findElement(By.id("P162943174-565944470")).click();

		driver.switchTo().defaultContent();

		WebElement frame2 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
		driver.switchTo().frame(frame2);
		System.out.println("Parent Frame:"+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

		driver.switchTo().frame("IwBottomFrame");
		System.out.println("IwBottomFrame:"
				+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
		
		TakeScreenShot_evidence.Evidences(driver, "USER_REMARKS");
//		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-561275385")));
		
		//Mark As SIP
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.id("P162943174-561275384")));
		}catch (Exception e) {
			System.out.println("Error At MarkAsSIP");
		}
	}
	
	
	
	public static void MakerExecute(WebDriver driver ,String Workitemid) throws Exception {
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("TabFrame");
		driver.findElement(By.xpath("//*[text()='Wires']")).click();
		driver.switchTo().defaultContent();

		driver.switchTo().frame("MenuFrame");
		driver.findElement(By.xpath("//*[@id='img_mnuWFMNU_ST1_MD1_SM1_phase2wire']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()='Suspense']")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id='td_val_mnuWFQ61261##MNU_ST1_MD1_SM1_p2sus##363543844']/span")).click();
		driver.switchTo().defaultContent();

		// Click On Search WorkitemID
		Thread.sleep(10000);
		
		driver.switchTo().frame("SearchFrame");
		String currentFrameName2 = (String) ((JavascriptExecutor) driver)
				.executeScript("return window.frameElement.name");
		System.out.println("currentFrameName2" + currentFrameName2);

		WebElement Search_WitemId = driver
				.findElement(By.xpath("(//*[@class=' x-form-text x-form-field x-box-item'])[last()-24]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='"+Workitemid+"';", Search_WitemId);

		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				driver.findElement(By.xpath("(//*[@class='x-grid3-hd-btn'])[last()-25]")));
		Thread.sleep(1000);

		WebElement ele = driver.findElement(By.xpath("(//*[@class='x-menu-list-item'])[last()-0]"));
		Actions ref = new Actions(driver);
		ref.moveToElement(ele);
		ref.click().build().perform();

		WebElement ele1 = driver.findElement(By.xpath("(//*[@class='x-menu-item-icon '])[last()-7]"));
		Actions ref1 = new Actions(driver);
		ref1.moveToElement(ele1);
		ref1.click().build().perform();

		Thread.sleep(3000);

		driver.findElement(By.xpath("(//*[contains(text(),'"+Workitemid+"')])[last()-1]")).click();

		driver.switchTo().defaultContent();

		Thread.sleep(10000);
		System.out.println("After Click on Work Item ID:" + driver.getTitle());

		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			driver.switchTo().window(handle);
			Thread.sleep(5000);
			System.out.println("Title of the new window: " + driver.getTitle());
			if (driver.getTitle().equalsIgnoreCase("Label Entry Window")) {
				driver.switchTo().window(handle);
				System.out.println("Switch Successfull!!: " + driver.getTitle());
				System.out.println("Check Switch is Successfull:" + driver.getTitle());
				// IFrame_WK_TAB_270308
				Thread.sleep(10000);

				WebElement frame1 = driver.findElement(By.xpath("//div[@id='TAB_0-body']//iframe"));
				driver.switchTo().frame(frame1);
				System.out.println("Parent Frame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));

				driver.switchTo().frame("IwMiddleFrame");
				System.out.println("IwMiddleFrame:"
						+ (String) ((JavascriptExecutor) driver).executeScript("return window.frameElement.name"));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-4]")));
				
				TakeScreenShot_evidence.Evidences(driver, "Payment_information");
				
//				driver.findElement(By.id("P162943174-357506513")).click();
//				
//				Robot robo=new Robot();
//				for (int i = 0; i < 10; i++) {
//					robo.keyPress(KeyEvent.VK_DOWN);
////					robo.keyRelease(KeyEvent.VK_DOWN);
//					Thread.sleep(2000);
//				}
				
				//CLICK ON ROUTING
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD231'])")));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-3]")));
				
				TakeScreenShot_evidence.Evidences(driver, "ROUTING");
				Thread.sleep(2000);
				
				//CLICK ON DOWNSTREAM SYSTEM STATUS
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='P162943174-TD242'])")));
				
				//Click on Expand All
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
						driver.findElement(By.xpath("(//*[@id='PEAL'])[last()-2]")));
				Thread.sleep(2000);

				Fillo fill = new Fillo();
				Connection conn=fill.getConnection(Constants._ExcelPath);
				Recordset rs=conn.executeQuery("Select * from PAIN001 where PaymentWorkItem_ID = '"+Workitemid+"'");
				while(rs.next()){
					
					if (rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("01") || rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("02")  || rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("03")) {
						MarkAsCompleted(driver,rs.getField("IWS_StatusCodeUI"));
					}
					
					if (rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("04") || rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("05") || rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("06") ) {
						MarkAsSIP(driver,rs.getField("IWS_StatusCodeUI"));
					}
					
					if (rs.getField("IWS_StatusCodeUI").equalsIgnoreCase("Reject") ) {
						MarkAsRejected(driver);
					}
			}	
			try{	
				TakeScreenShot_evidence.Evidences(driver, "Error_Popup");
			}catch (Exception e) {
				System.out.println("Error Caught!!");
			}
				Thread.sleep(2000);
				driver.quit();
			}
		}
	}
}
