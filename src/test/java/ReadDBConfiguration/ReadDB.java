package ReadDBConfiguration;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Constants_pack.Constants;

public class ReadDB {

public static String getENV1_OracleDriver() throws FilloException{
	System.setProperty("ROW", "2");
	String batch_path = null;
	try {
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'ENV1_OracleDriver'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getMsgId_Prefix() throws FilloException{
	System.setProperty("ROW", "2");
	String batch_path = null;
	try {
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'MsgId_Prefix'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getENV1_ConnName() throws FilloException{
	System.setProperty("ROW", "2");
	String batch_path = null;
	try {
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'ENV1_ConnName'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}
		

public static String getENV1_Uname() throws FilloException{
	System.setProperty("ROW", "2");
	String batch_path = null;
	try {
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'ENV1_Uname'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getENV1_Pass() throws FilloException{
	System.setProperty("ROW", "2");
	String batch_path = null;
	try {
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'ENV1_Pass'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getAPI_URL() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'API_URL'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getchannelId() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'channelId'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getservice_type() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'service_type'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getflow_id() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'flow_id'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getWAITIME() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'WAIT_TIME'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}


public static String getPSH_TestLink() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'PSH_TestLink'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getMT103_Stub() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'MT103_Stub'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}

public static String getMT199_MT900_Stub() {
	String batch_path = null;
	try {
		System.setProperty("ROW", "2");
		Fillo fill = new Fillo();
		Connection conn = fill.getConnection(Constants._ExcelPath);
		Recordset rs = conn.executeQuery("Select * from Configuration_Setup where List_Name = 'MT199_MT900_Stub'");
		while (rs.next()) {
			batch_path = rs.getField("Local_Path");
		}
	}catch (Exception e) {
			e.printStackTrace();
	}	
	return batch_path;
}
	
public static void main(String[] args) throws Exception{
	
	System.out.println(getPSH_TestLink());
	System.out.println(getMT103_Stub());
	System.out.println(getMT199_MT900_Stub());
	
//	System.out.println(getchannelId());
//	System.out.println(getservice_type());
//	System.out.println(getflow_id());
//	System.out.println(getWAITIME());
//	System.out.println(getMsgId_Prefix());	
//	System.out.println(getENV1_ConnName());
//	System.out.println(getENV1_Uname());
//	System.out.println(getENV1_Pass());
 }
}
