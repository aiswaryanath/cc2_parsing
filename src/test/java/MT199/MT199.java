package MT199;

import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import MT103UI.GetValueMT103;
import MT199UI.TestMT199UI;
import Utility.SaltString;

public class MT199 {
	static PrintStream json_console_add = null;
	public static StringBuffer append_output=null;
	public static String CreateFile_MT199(String FIle_WORKITEMID) throws Exception {
		
		 String out_put=null;
		     append_output=new StringBuffer();
		 	 MT199_Modules.ifDataExistThenAppend("IWSL    PSH4L199              TO"+new SimpleDateFormat("yyMMdd").format(new Date()).toString()+"775555"+SaltString.getRandNo(3)+"I1650 27CIBCCATT XXX000010000 00001083318   01179199 02    ", append_output);
			 MT199_Modules.ifDataExistThenAppend("{1:F01CIBCCAT0ACMO0000000004}", append_output);
			 MT199_Modules.ifDataExistThenAppend("{2:o1991523170531PNBPUS3NAXXX00000000001705311523N}", append_output);
			 MT199_Modules.ifDataExistThenAppend("{3:", append_output);
			 MT199_Modules.ifDataExistThenAppend("{121:xxxxxxxx-xxxx-4xxx-1234-18121103xxxx}}", append_output);
			 MT199_Modules.ifDataExistThenAppend("{4:", append_output);
			 MT199_Modules.ifDataExistThenAppend(":20:ICN123456789"+SaltString.getRandNo(3)+"", append_output);
			 MT199_Modules.ifDataExistThenAppend(":21:"+GetValueMT103.getdata_WIRE_EXT_REF_NUM(FIle_WORKITEMID)+"", append_output);
			 MT199_Modules.ifDataExistThenAppend(":79:/SWF/IGNORE REF AS CODE CLR SYS IS SWF", append_output);
			 MT199_Modules.ifDataExistThenAppend("-}{5:}", append_output);
			 
			//MT199 Modules
//			json_console_add = new PrintStream(new File("C:\\Users\\varun.paranganath\\Desktop\\TEST Data Bkp\\MT199\\MT199 Process doc\\TESTMT199.txt"));
//			System.setOut(json_console_add);
			System.out.println(append_output.toString().trim());
		return out_put;
	}
	
	public static void main(String[] args) throws Exception{
		MT199.CreateFile_MT199("7891325");
		TestMT199UI.PUSHMT199();
	}
	
}
