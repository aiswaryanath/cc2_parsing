package MT199;

import java.io.PrintStream;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class MT199_Modules {

	public static String BLOCK1_exe(String TEST_CASEID) throws Exception{
		 String out_putFormat=null;
		 Fillo fillo =new Fillo();
		 Connection conn=fillo.getConnection("D:\\Inward_Outward\\Test_Data\\Test_data.xlsx");
		 Recordset rs=conn.executeQuery("Select * from MT199 where TEST_CASEID = '"+TEST_CASEID+"'");
		 while (rs.next()) {
			 String Application_ID = rs.getField("Application_ID");
			 String Service_ID = rs.getField("Service_ID");
			 String Logical_Terminal = rs.getField("Logical_Terminal");
			 String Session_Number = rs.getField("Session_Number");
			 String Sequence_Number = rs.getField("Sequence_Number");
			 out_putFormat = Application_ID+Service_ID+Logical_Terminal+Session_Number+Sequence_Number;
		 }
		 return out_putFormat;
	}

	
	public static String BLOCK2_exe(String TEST_CASEID) throws Exception{
		 String out_putFormat=null;
		 Fillo fillo =new Fillo();
		 Connection conn=fillo.getConnection("D:\\Inward_Outward\\Test_Data\\Test_data.xlsx");
		 Recordset rs=conn.executeQuery("Select * from MT199 where TEST_CASEID = '"+TEST_CASEID+"'");
		 while (rs.next()) {
			 String Input_Output_Identifier = rs.getField("Input_Output_Identifier");
			 String Message_Type = rs.getField("Message_Type");
			 String Message_Recipient_Address = rs.getField("Message_Recipient_Address");
			 String Message_priority = rs.getField("Message_priority");
			 String Delivery_Monitoring = rs.getField("Delivery_Monitoring");
			 String Obsolesce_Period = rs.getField("Obsolesce_Period");
			 out_putFormat = Input_Output_Identifier+Message_Type+Message_Recipient_Address+Message_priority+Delivery_Monitoring+Obsolesce_Period;
		 }
		 return out_putFormat;
	}
	
	
	public static StringBuffer BLOCK4_exe(String TEST_CASEID) throws Exception{
		 String out_putFormat=null;
		 StringBuffer s=new StringBuffer();
		 Fillo fillo =new Fillo();
		 Connection conn=fillo.getConnection("D:\\Inward_Outward\\Test_Data\\Test_data.xlsx");
		 Recordset rs=conn.executeQuery("Select * from MT199 where TEST_CASEID = '"+TEST_CASEID+"'");
		 while (rs.next()) {
			 
			 String Block4_val = rs.getField("Block4_val");
			 ifDataExistThenAppend(Block4_val, s);
			 
			 String Transaction_Reference_Tag20_var = rs.getField("Transaction_Reference_Tag20_var");
			 String Transaction_Reference_Tag20_value = rs.getField("Transaction_Reference_Tag20_value");
			 ifDataExistThenAppend(Transaction_Reference_Tag20_var,Transaction_Reference_Tag20_value, s);
			 
			 String Related_Reference_Tag21_var = rs.getField("Related_Reference_Tag21_var");
			 String Related_Reference_Tag21_value = rs.getField("Related_Reference_Tag21_value");
			 ifDataExistThenAppend(Related_Reference_Tag21_var,Related_Reference_Tag21_value, s);
			 
			 String Narrative_var = rs.getField("Narrative_var");
			 ifDataExistThenAppend(Narrative_var, s);
			 
			 String Narrative_Line1  = rs.getField("Narrative_Line1");
			 ifDataExistThenAppend(Narrative_Line1, s);
			 
			 String Narrative_Line2  = rs.getField("Narrative_Line2");
			 ifDataExistThenAppend(Narrative_Line2, s);
			 
			 String Narrative_Line3  = rs.getField("Narrative_Line3");
			 ifDataExistThenAppend(Narrative_Line3, s);
			 
			 String Narrative_Line4  = rs.getField("Narrative_Line4");
			 ifDataExistThenAppend(Narrative_Line4, s);
			 
			 String Narrative_Line5  = rs.getField("Narrative_Line5");
			 ifDataExistThenAppend(Narrative_Line5, s);
			 
			 String Narrative_Line6  = rs.getField("Narrative_Line6");
			 ifDataExistThenAppend(Narrative_Line6, s);
			 
			 String Narrative_Line7  = rs.getField("Narrative_Line7");
			 ifDataExistThenAppend(Narrative_Line7, s);
			 
			 String Narrative_Line8  = rs.getField("Narrative_Line8");
			 ifDataExistThenAppend(Narrative_Line8, s);
			 
			 String Narrative_Line9  = rs.getField("Narrative_Line9");
			 ifDataExistThenAppend(Narrative_Line9, s);
			 
			 String Narrative_Line10  = rs.getField("Narrative_Line10");
			 ifDataExistThenAppend(Narrative_Line10, s);
			 
			 String Narrative_Line11  = rs.getField("Narrative_Line11");
			 ifDataExistThenAppend(Narrative_Line11, s);
			 
			 String Narrative_Line12  = rs.getField("Narrative_Line12");
			 ifDataExistThenAppend(Narrative_Line12, s);
			 
			 String Narrative_Line13  = rs.getField("Narrative_Line13");
			 ifDataExistThenAppend(Narrative_Line13, s);
			 
			 String Narrative_Line14  = rs.getField("Narrative_Line14");
			 ifDataExistThenAppend(Narrative_Line14, s);
			 
			 String Narrative_Line15  = rs.getField("Narrative_Line15");
			 ifDataExistThenAppend(Narrative_Line15, s);
			 
			 String Narrative_Line16  = rs.getField("Narrative_Line16");
			 ifDataExistThenAppend(Narrative_Line16, s);
			 
			 String Narrative_Line17  = rs.getField("Narrative_Line17");
			 ifDataExistThenAppend(Narrative_Line17, s);
			 
			 String Narrative_Line18  = rs.getField("Narrative_Line18");
			 ifDataExistThenAppend(Narrative_Line18, s);
			 
			 String Narrative_Line19  = rs.getField("Narrative_Line19");
			 ifDataExistThenAppend(Narrative_Line19, s);
			 
			 String Narrative_Line20  = rs.getField("Narrative_Line20");
			 ifDataExistThenAppend(Narrative_Line20, s);
			 
			 String Narrative_Line21  = rs.getField("Narrative_Line21");
			 ifDataExistThenAppend(Narrative_Line21, s);
			 
			 String Narrative_Line22  = rs.getField("Narrative_Line22");
			 ifDataExistThenAppend(Narrative_Line22, s);
			 
			 String Narrative_Line23  = rs.getField("Narrative_Line23");
			 ifDataExistThenAppend(Narrative_Line23, s);
			 
			 String Narrative_Line24  = rs.getField("Narrative_Line24");
			 ifDataExistThenAppend(Narrative_Line24, s);
			 
			 String Narrative_Line25  = rs.getField("Narrative_Line25");
			 ifDataExistThenAppend(Narrative_Line25, s);
			 
			 String Narrative_Line26  = rs.getField("Narrative_Line26");
			 ifDataExistThenAppend(Narrative_Line26, s);
			 
			 String Narrative_Line27  = rs.getField("Narrative_Line27");
			 ifDataExistThenAppend(Narrative_Line27, s);
			 
			 String Narrative_Line28  = rs.getField("Narrative_Line28");
			 ifDataExistThenAppend(Narrative_Line28, s);
			 
			 String Narrative_Line29  = rs.getField("Narrative_Line29");
			 ifDataExistThenAppend(Narrative_Line29, s);
			 
			 String Narrative_Line30  = rs.getField("Narrative_Line30");
			 ifDataExistThenAppend(Narrative_Line30, s);
			 
			 String Narrative_Line31  = rs.getField("Narrative_Line31");
			 ifDataExistThenAppend(Narrative_Line31, s);
			 
			 String Narrative_Line32  = rs.getField("Narrative_Line32");
			 ifDataExistThenAppend(Narrative_Line32, s);
			 
			 String Narrative_Line33  = rs.getField("Narrative_Line33");
			 ifDataExistThenAppend(Narrative_Line33, s);
			 
			 String Narrative_Line34  = rs.getField("Narrative_Line34");
			 ifDataExistThenAppend(Narrative_Line34, s);
			 
			 String Narrative_Line35  = rs.getField("Narrative_Line35");
			 ifDataExistThenAppend(Narrative_Line35, s);
			 
			 String Narrative_Line36  = rs.getField("Narrative_Line36");
			 ifDataExistThenAppend(Narrative_Line36, s);
			 
			 String Narrative_Line37  = rs.getField("Narrative_Line37");
			 ifDataExistThenAppend(Narrative_Line37, s);
			 
			 String Narrative_Line38  = rs.getField("Narrative_Line38");
			 ifDataExistThenAppend(Narrative_Line38, s);
			 
			 String Narrative_Line39  = rs.getField("Narrative_Line39");
			 ifDataExistThenAppend(Narrative_Line39, s);
			 
			 String Narrative_Line40  = rs.getField("Narrative_Line40");
			 ifDataExistThenAppend(Narrative_Line40, s);
			 
			 String Narrative_Line41  = rs.getField("Narrative_Line41");
			 ifDataExistThenAppend(Narrative_Line41, s);
			 
			 String Narrative_Line42  = rs.getField("Narrative_Line42");
			 ifDataExistThenAppend(Narrative_Line42, s);
			 
			 String Narrative_Line43  = rs.getField("Narrative_Line43");
			 ifDataExistThenAppend(Narrative_Line43, s);
			 
			 String Narrative_Line44  = rs.getField("Narrative_Line44");
			 ifDataExistThenAppend(Narrative_Line44, s);
			 
			 String Narrative_Line45  = rs.getField("Narrative_Line45");
			 ifDataExistThenAppend(Narrative_Line45, s);
			 
			 String Narrative_Line46  = rs.getField("Narrative_Line46");
			 ifDataExistThenAppend(Narrative_Line46, s);
			 
			 String Narrative_Line47  = rs.getField("Narrative_Line47");
			 ifDataExistThenAppend(Narrative_Line47, s);
			 
			 String Narrative_Line48  = rs.getField("Narrative_Line48");
			 ifDataExistThenAppend(Narrative_Line48, s);
			 
			 String Narrative_Line49  = rs.getField("Narrative_Line49");
			 ifDataExistThenAppend(Narrative_Line49, s);
			 
			 String Narrative_Line50  = rs.getField("Narrative_Line50");
			 ifDataExistThenAppend(Narrative_Line50, s);
			 
			 String Block4_End_Tag=rs.getField("Block4_End_Tag");
			 ifDataExistThenAppend_endTag(Block4_End_Tag.trim(),s);
			 
		 }
		 return s;
	}

	
	public static StringBuffer TagNarrativeLine_concatenate(String TEST_CASEID) throws Exception{
		 StringBuffer s=new StringBuffer();
		 Fillo fillo =new Fillo();
		 Connection conn=fillo.getConnection("D:\\Inward_Outward\\Test_Data\\Test_data.xlsx");
		 Recordset rs=conn.executeQuery("Select * from MT199 where TEST_CASEID = '"+TEST_CASEID+"'");
		 while (rs.next()) {
			 String Narrative_Line1  = rs.getField("Narrative_Line1");
			 String Narrative_Line2  = rs.getField("Narrative_Line2");
			 String Narrative_Line3  = rs.getField("Narrative_Line3");
			 String Narrative_Line4  = rs.getField("Narrative_Line4");
			 String Narrative_Line5  = rs.getField("Narrative_Line5");
			 String Narrative_Line6  = rs.getField("Narrative_Line6");
			 String Narrative_Line7  = rs.getField("Narrative_Line7");
			 String Narrative_Line8  = rs.getField("Narrative_Line8");
			 String Narrative_Line9  = rs.getField("Narrative_Line9");
			 String Narrative_Line10  = rs.getField("Narrative_Line10");
			 String Narrative_Line11  = rs.getField("Narrative_Line11");
			 String Narrative_Line12  = rs.getField("Narrative_Line12");
			 String Narrative_Line13  = rs.getField("Narrative_Line13");
			 String Narrative_Line14  = rs.getField("Narrative_Line14");
			 String Narrative_Line15  = rs.getField("Narrative_Line15");
			 String Narrative_Line16  = rs.getField("Narrative_Line16");
			 String Narrative_Line17  = rs.getField("Narrative_Line17");
			 String Narrative_Line18  = rs.getField("Narrative_Line18");
			 String Narrative_Line19  = rs.getField("Narrative_Line19");
			 String Narrative_Line20  = rs.getField("Narrative_Line20");
			 String Narrative_Line21  = rs.getField("Narrative_Line21");
			 String Narrative_Line22  = rs.getField("Narrative_Line22");
			 String Narrative_Line23  = rs.getField("Narrative_Line23");
			 String Narrative_Line24  = rs.getField("Narrative_Line24");
			 String Narrative_Line25  = rs.getField("Narrative_Line25");
			 String Narrative_Line26  = rs.getField("Narrative_Line26");
			 String Narrative_Line27  = rs.getField("Narrative_Line27");
			 String Narrative_Line28  = rs.getField("Narrative_Line28");
			 String Narrative_Line29  = rs.getField("Narrative_Line29");
			 String Narrative_Line30  = rs.getField("Narrative_Line30");
			 String Narrative_Line31  = rs.getField("Narrative_Line31");
			 String Narrative_Line32  = rs.getField("Narrative_Line32");
			 String Narrative_Line33  = rs.getField("Narrative_Line33");
			 String Narrative_Line34  = rs.getField("Narrative_Line34");
			 String Narrative_Line35  = rs.getField("Narrative_Line35");
			 String Narrative_Line36  = rs.getField("Narrative_Line36");
			 String Narrative_Line37  = rs.getField("Narrative_Line37");
			 String Narrative_Line38  = rs.getField("Narrative_Line38");
			 String Narrative_Line39  = rs.getField("Narrative_Line39");
			 String Narrative_Line40  = rs.getField("Narrative_Line40");
			 String Narrative_Line41  = rs.getField("Narrative_Line41");
			 String Narrative_Line42  = rs.getField("Narrative_Line42");
			 String Narrative_Line43  = rs.getField("Narrative_Line43");
			 String Narrative_Line44  = rs.getField("Narrative_Line44");
			 String Narrative_Line45  = rs.getField("Narrative_Line45");
			 String Narrative_Line46  = rs.getField("Narrative_Line46");
			 String Narrative_Line47  = rs.getField("Narrative_Line47");
			 String Narrative_Line48  = rs.getField("Narrative_Line48");
			 String Narrative_Line49  = rs.getField("Narrative_Line49");
			 String Narrative_Line50  = rs.getField("Narrative_Line50");
			 
			 if (!Narrative_Line1.isEmpty()) {
				 s.append(Narrative_Line1).append(System.getProperty("line.separator"));
			}
			 
			 if (!Narrative_Line2.isEmpty()) {
				 s.append(Narrative_Line2).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line3.isEmpty()) {
				 s.append(Narrative_Line3).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line4.isEmpty()) {
				 s.append(Narrative_Line4).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line5.isEmpty()) {
				 s.append(Narrative_Line5).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line6.isEmpty()) {
				 s.append(Narrative_Line6).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line7.isEmpty()) {
				 s.append(Narrative_Line7).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line8.isEmpty()) {
				 s.append(Narrative_Line8).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line9.isEmpty()) {
				 s.append(Narrative_Line9).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line10.isEmpty()) {
				 s.append(Narrative_Line10).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line11.isEmpty()) {
				 s.append(Narrative_Line11).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line12.isEmpty()) {
				 s.append(Narrative_Line12).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line13.isEmpty()) {
				 s.append(Narrative_Line13).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line14.isEmpty()) {
				 s.append(Narrative_Line14).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line15.isEmpty()) {
				 s.append(Narrative_Line16).append(System.getProperty("line.separator"));
			} 
			 
			 if (!Narrative_Line16.isEmpty()) {
				 s.append(Narrative_Line16).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line17.isEmpty()) {
				 s.append(Narrative_Line17).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line18.isEmpty()) {
				 s.append(Narrative_Line18).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line19.isEmpty()) {
				 s.append(Narrative_Line19).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line20.isEmpty()) {
				 s.append(Narrative_Line20).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line21.isEmpty()) {
				 s.append(Narrative_Line21).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line22.isEmpty()) {
				 s.append(Narrative_Line22).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line23.isEmpty()) {
				 s.append(Narrative_Line23).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line24.isEmpty()) {
				 s.append(Narrative_Line24).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line25.isEmpty()) {
				 s.append(Narrative_Line25).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line26.isEmpty()) {
				 s.append(Narrative_Line26).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line27.isEmpty()) {
				 s.append(Narrative_Line27).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line28.isEmpty()) {
				 s.append(Narrative_Line28).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line29.isEmpty()) {
				 s.append(Narrative_Line29).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line30.isEmpty()) {
				 s.append(Narrative_Line30).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line31.isEmpty()) {
				 s.append(Narrative_Line31).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line32.isEmpty()) {
				 s.append(Narrative_Line32).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line33.isEmpty()) {
				 s.append(Narrative_Line33).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line34.isEmpty()) {
				 s.append(Narrative_Line34).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line35.isEmpty()) {
				 s.append(Narrative_Line36).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line37.isEmpty()) {
				 s.append(Narrative_Line37).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line38.isEmpty()) {
				 s.append(Narrative_Line38).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line39.isEmpty()) {
				 s.append(Narrative_Line39).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line40.isEmpty()) {
				 s.append(Narrative_Line40).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line41.isEmpty()) {
				 s.append(Narrative_Line41).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line42.isEmpty()) {
				 s.append(Narrative_Line42).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line43.isEmpty()) {
				 s.append(Narrative_Line43).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line44.isEmpty()) {
				 s.append(Narrative_Line44).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line45.isEmpty()) {
				 s.append(Narrative_Line45).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line46.isEmpty()) {
				 s.append(Narrative_Line46).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line47.isEmpty()) {
				 s.append(Narrative_Line47).append(System.getProperty("line.separator"));
			} 
			 if (!Narrative_Line48.isEmpty()) {
				 s.append(Narrative_Line48).append(System.getProperty("line.separator"));
			}
			 if (!Narrative_Line49.isEmpty()) {
				 s.append(Narrative_Line49).append(System.getProperty("line.separator"));
			 }
			 if (!Narrative_Line50.isEmpty()) {
				 s.append(Narrative_Line50).append(System.getProperty("line.separator"));
			}
			 
		 }
		 return s;
	}
	
public static StringBuffer BLOCK5_exe(String TEST_CASEID) throws Exception{
//		 String out_putFormat=null;
		 StringBuffer out_putFormat=new StringBuffer();
		 Fillo fillo =new Fillo();
		 Connection conn=fillo.getConnection("D:\\Inward_Outward\\Test_Data\\Test_data.xlsx");
		 Recordset rs=conn.executeQuery("Select * from MT199 where TEST_CASEID = '"+TEST_CASEID+"'");
		 while (rs.next()) {
			 String Block_5val = rs.getField("Block_5val");
			 String Message_Authentication_Code_var      = rs.getField("Message_Authentication_Code_var");
			 String Message_Authentication_Code_value    = rs.getField("Message_Authentication_Code_value"); 
			 String CheckSum_var                         = rs.getField("CheckSum_var");
			 String CheckSum_value                       = rs.getField("CheckSum_value");
			 String Possible_Duplicate_Emission_var      = rs.getField("Possible_Duplicate_Emission_var");
			 String Possible_Duplicate_Emission_value    = rs.getField("Possible_Duplicate_Emission_value");
			 String Possible_Duplicate_Message_var       = rs.getField("Possible_Duplicate_Message_var");
			 String Possible_Duplicate_Message_val       = rs.getField("Possible_Duplicate_Message_val");
			 String Delayed_Message_var                  = rs.getField("Delayed_Message_var");
			 String Delayed_Message_value                  = rs.getField("Delayed_Message_value");
			 
			 ifDataExistThenAppend_endTag(Block_5val, out_putFormat);
			 ifDataExistThenAppend_endTag(Message_Authentication_Code_var, out_putFormat);
			 ifDataExistThenAppend_endTag(Message_Authentication_Code_value, out_putFormat);
			 ifDataExistThenAppend_endTag(CheckSum_var, out_putFormat);
			 ifDataExistThenAppend_endTag(CheckSum_value, out_putFormat);
			 ifDataExistThenAppend_endTag(Possible_Duplicate_Emission_var, out_putFormat);
			 ifDataExistThenAppend_endTag(Possible_Duplicate_Emission_value, out_putFormat);
			 ifDataExistThenAppend_endTag(Possible_Duplicate_Message_var, out_putFormat);
			 ifDataExistThenAppend_endTag(Possible_Duplicate_Message_val, out_putFormat);
			 ifDataExistThenAppend_endTag(Delayed_Message_var, out_putFormat);
			 ifDataExistThenAppend_endTag(Delayed_Message_value, out_putFormat);
		 }
		 return out_putFormat;
}

public static String BLOCK3_exe(String TEST_CASEID) throws Exception{
	 String out_putFormat=null;
	 Fillo fillo =new Fillo();
	 Connection conn=fillo.getConnection("D:\\Inward_Outward\\Test_Data\\Test_data.xlsx");
	 Recordset rs=conn.executeQuery("Select * from MT199 where TEST_CASEID = '"+TEST_CASEID+"'");
	 while (rs.next()) {
		 String BLOCK3_Tag = rs.getField("BLOCK3_Tag");
		 String Service_LevelService_Code = rs.getField("Service_LevelService_Code");
		 String Banking_Priority = rs.getField("Banking_Priority");
		 String Validation_Tag = rs.getField("Validation_Tag");
		 String Message_User_Reference = rs.getField("Message_User_Reference");
		 String Receiver_Information = rs.getField("Receiver_Information");
		 
		 out_putFormat = BLOCK3_Tag+Service_LevelService_Code+Banking_Priority+Validation_Tag+Message_User_Reference+Receiver_Information;
	 }
	 return out_putFormat;
}	

public static void ifDataExistThenAppend(String key,String value,StringBuffer s) {
	 if ((key!=null && !key.isEmpty()) || (value!=null && !value.isEmpty())) {
		 s.append(key+value).append(System.getProperty("line.separator"));
	 }
}

public static void ifDataExistThenAppend_endTag(String Line,StringBuffer s) {
	 if ((Line!=null && !Line.isEmpty())) {
		 s.append(Line);
	 }
}

public static void ifDataExistThenAppend(String Line,StringBuffer s) {
	 if ((Line!=null && !Line.isEmpty())) {
		 s.append(Line).append(System.getProperty("line.separator"));
	 }	 
}

}
