package APIFrameworkGsonController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import APIGsonKeyValueConditions.AuthstnGson;
import APIGsonKeyValueConditions.GrpHdrGson;
import APIGsonKeyValueConditions.InitgPtyGson;
import APIGsonKeyValueConditions.PmTpInfSvcLvlGson;
import APIGsonKeyValueConditions.PmtInfGsonkeyValue;
import APIGsonKeyValueConditions.RmtInfGson;
import APIGsonKeyValueConditions.SplmntryCntsGson;
import APIGsonKeyValueConditions.cdtrAgtpstladdress;
import APIGsonKeyValueConditions.cdtrPstlAddress;
import APIGsonKeyValueConditions.dbtrPstlAddress;
import Amt.Amt;
import Amt.EqvtAmt;
import Amt.InstdAmt;
import Api_Operaitons.POST_requestApi;
import Cdtr.Cdtr;
import Cdtr.CdtrPstlAdr;
import CdtrAcct.CdtrAcct;
import CdtrAcct.CdtrAcctId;
import CdtrAcct.CdtrAcctOthr;
import CdtrAgt.CdtrAgt;
import CdtrAgt.CdtrAgtPstlAdr;
import CdtrAgt.ClrSysId;
import CdtrAgt.ClrSysMmbId;
import CdtrAgt.FinInstnId;
import ChrgsAcct.ChrgsAcctId;
import ChrgsAcct.ChrgsAcctOthr;
import Constants_pack.Constants;
import Dbtr.Dbtr;
import Dbtr.DbtrId;
import Dbtr.DbtrOrgId;
import Dbtr.DbtrOthr;
import Dbtr.PstlAdr;
import DbtrAcct.DbtrAcct;
import DbtrAcct.DbtrAcctId;
import DbtrAcct.DbtrAcctOthr;
import DbtrAgt.DbtrAgt;
import DbtrAgt.DbtrAgtBrnchId;
import DbtrAgt.DbtrAgtFinInstnId;
import DbtrAgt.DbtrAgtOthr;
import DbtrAgt.DbtrAgtPstlAdr;
import InitgPty.Id;
import InitgPty.InitgPty;
import InitgPty.OrgId;
import InitgPty.Othr;
import IntrmyAgt1.IntrmyAgt1;
import IntrmyAgt1.IntrmyAgt1FinInstnId;
import JSONStructure_PAIN001.Authstn;
import JSONStructure_PAIN001.CdtTrfTxInf;
import JSONStructure_PAIN001.CstmrCdtTrfInitn;
import JSONStructure_PAIN001.Document;
import JSONStructure_PAIN001.GrpHdr;
import JSONStructure_PAIN001.PmtInf;
import JSONStructure_PAIN001.RootClass;
import ParseRecordExcel.Update_ExcelResult;
import PmtId.PmtId;
import PmtTpInf.LclInstrm;
import PmtTpInf.PmtTpInf;
import PmtTpInf.SvcLvl;
import Purp.Purp;
import ReadDBConfiguration.ReadDB;
import RmtInf.RmtInf;
import SplmtryData.Envlp;
import SplmtryData.SplmtryData;
import Utility.GetSimpleDateFormat;
import Utility.SaltString;
import XchgRateInf.XchgRateInf;

public class GsonAPI_DataManipulationSplmntry {
	/*static PrintStream json_console = null;

	static String _API_Link = ReadDB.getAPI_URL();
	static String channelId = ReadDB.getchannelId();
	static String service_type = ReadDB.getservice_type();
	static String flow_id = ReadDB.getflow_id();

	public static void main(String[] args) throws FileNotFoundException, FilloException,Exception {
		System.setProperty("ROW", "2");
		Fillo fill=new Fillo(); 
		Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
		Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
		 while (rs.next()) {
			String TestCaseID = rs.getField("Test_ID");
			
			String _MsgId = null;
			String _CreDtTm = null;
			if (rs.getField("Custom_Flag_Var").equalsIgnoreCase("Y")) {
				_MsgId = SaltString.getSaltString(10);
				_CreDtTm = GetSimpleDateFormat.getSimpleDateFormat();
			}else{
				_MsgId = rs.getField("MsgId_value");
				_CreDtTm = rs.getField("CreDtTm_value");
			}
			
			GrpHdr GrpHdr =new GrpHdr();
//			Update_ExcelResult.Update_DataReportAck(TestCaseID, _MsgId, "Message_ID");
			GrpHdr.setMsgId(_MsgId);
			GrpHdr.setCreDtTm(_CreDtTm);
			
			//NbOfTxs
			GrpHdrGson.GetgrpHdrsetNbOfTxs(rs, GrpHdr);
			//CtrlSum
			GrpHdrGson.GetsetCtrlSum(rs, GrpHdr);
			
			Authstn Authstn=new Authstn();
			AuthstnGson.GetAuthstn(rs, Authstn);
			
			GrpHdr.setAuthstn(Authstn);

			//InitgPty
			Othr Othr=new Othr();
			InitgPtyGson.InitgPtyOthrGson(rs, Othr);
			
			OrgId OrgId=new OrgId();
			OrgId.setOthr(Othr);
			
			Id Id=new Id();
			Id.setOrgId(OrgId);
			
			InitgPty InitgPty=new InitgPty();
			InitgPtyGson.InitgPtyIdGson(rs, InitgPty);
			InitgPty.setId(Id);
			
			GrpHdr.setInitgPty(InitgPty);
	//****************************************************PmtInf****************************************************
			String TC_PmtInf_ID = rs.getField("TC_PmtInf_ID");
			
			Fillo fill3 = new Fillo();
			Connection conn3=fill3.getConnection(Constants._ExcelPath);
			Recordset rs3=conn3.executeQuery("Select * from PMTINF where TC_PmtInf_ID = '"+TC_PmtInf_ID+"'");
			ArrayList<PmtInf> _addPmtInf = new ArrayList<PmtInf>();
			CstmrCdtTrfInitn CstmrCdtTrfInitn=null;
		while(rs3.next()){
			CstmrCdtTrfInitn=new CstmrCdtTrfInitn();
			CstmrCdtTrfInitn.setGrpHdr(GrpHdr);
//			PmtTpInf Starts here
			PmtInf PmtInf =new PmtInf();
			PmtInfGsonkeyValue.PmtInfIdGson(rs3, PmtInf);
//			PmtTpInf Ends here

			SvcLvl SvcLvl=new SvcLvl();
			
		if (!rs3.getField("PmtTpInf_SvcLvl").isEmpty()) {
			PmTpInfSvcLvlGson.getSvcLvl(rs3, SvcLvl);
		}	
			LclInstrm LclInstrm=new LclInstrm();
			
		if (!rs3.getField("PmtTpInf_LclInstrm").isEmpty()) {
			PmTpInfSvcLvlGson.getLclInstrm(rs3, LclInstrm);
		}	
			
		PmtTpInf PmtTpInf=new PmtTpInf();
		if (!rs3.getField("PmtTpInf_LclInstrm").isEmpty()) {	
			PmtTpInf.setLclInstrm(LclInstrm);
		}
		if (!rs3.getField("PmtTpInf_SvcLvl").isEmpty()) {	
			PmtTpInf.setSvcLvl(SvcLvl);
		}	
			
		PmtInf.setPmtTpInf(PmtTpInf);
			
//			PmtTpInf Ends here		
			
//			Dbtr Starts Here
			
			PstlAdr PstlAdr=new PstlAdr();
			PstlAdr.setStrtNm(rs3.getField("StrtNm_value"));
			PstlAdr.setBldgNb(rs3.getField("BldgNb_value"));
			PstlAdr.setPstCd(rs3.getField("PstCd_value"));
			PstlAdr.setTwnNm(rs3.getField("TwnNm_value"));
			PstlAdr.setCtrySubDvsn(rs3.getField("CtrySubDvsn_value"));
			PstlAdr.setCtry(rs3.getField("Ctry_value"));
			
			ArrayList Address = new ArrayList();
			dbtrPstlAddress.getDbtrPstlAddress(rs3, Address);
			if (!Address.isEmpty()) {
				PstlAdr.setAdrLine(Address);
			}
			
			DbtrOthr DbtrOthr=new DbtrOthr();
			DbtrOthr.setId(rs3.getField("Id_value2"));
			
			DbtrOrgId DbtrOrgId=new DbtrOrgId();
			DbtrOrgId.setOthr(DbtrOthr);
			
			DbtrId  DbtrId=new DbtrId();
			DbtrId.setOrgId(DbtrOrgId);
			
			Dbtr Dbtr=new Dbtr();
			Dbtr.setId(DbtrId);
			Dbtr.setNm(rs3.getField("Nm_value"));
			Dbtr.setPstlAdr(PstlAdr);
			
			PmtInf.setDbtr(Dbtr);
//			Dbtr Ends here
			
//			ChrgsAcct Starts here
			ChrgsAcctOthr Othr1=new ChrgsAcctOthr();
			Othr1.setId(rs3.getField("Id_1value"));
			
			ChrgsAcctId  Id1=new ChrgsAcctId();
			Id1.setOthr(Othr1);
			
			ChrgsAcct.ChrgsAcct ChrgsAcct = new ChrgsAcct.ChrgsAcct();
			ChrgsAcct.setId(Id1);		
			
			PmtInf.setChrgsAcct(ChrgsAcct);
//			ChrgsAcct Ends here
			
//			DbtrAcct Starts here
			
			DbtrAcctOthr DbtrAcctOthr=new DbtrAcctOthr();
			DbtrAcctOthr.setId(rs3.getField("Id_2value"));
			
			DbtrAcctId DbtrAcctId=new DbtrAcctId();
			DbtrAcctId.setOthr(DbtrAcctOthr);
			
			DbtrAcct DbtrAcct=new DbtrAcct();
			DbtrAcct.setId(DbtrAcctId);
			
			PmtInf.setDbtrAcct(DbtrAcct);
//			DbtrAcct Ends here
			
//			DbtrAgt Starts here
			DbtrAgtBrnchId BrnchId=new DbtrAgtBrnchId();
			BrnchId.setId(rs3.getField("Id_3valueF"));
			
			DbtrAgtPstlAdr DbtrAgtPstlAdr=new DbtrAgtPstlAdr();
			DbtrAgtPstlAdr.setCtry(rs3.getField("Ctry_value1"));
			
			DbtrAgtOthr DbtrAgtOthr=new DbtrAgtOthr();
			DbtrAgtOthr.setId(rs3.getField("Id_3value"));
			
			DbtrAgtFinInstnId DbtrAgtFinInstnId=new DbtrAgtFinInstnId();
			DbtrAgtFinInstnId.setBICFI(rs3.getField("BICFI_value"));
			DbtrAgtFinInstnId.setNm(rs3.getField("Nm_3value"));
			DbtrAgtFinInstnId.setPstlAdr(DbtrAgtPstlAdr);
			DbtrAgtFinInstnId.setOthr(DbtrAgtOthr);
			
			DbtrAgt DbtrAgt=new DbtrAgt();
			DbtrAgt.setBrnchId(BrnchId);
			DbtrAgt.setFinInstnId(DbtrAgtFinInstnId);
			PmtInf.setDbtrAgt(DbtrAgt);
					
			
            System.setProperty("ROW", "2");
			Fillo fill1=new Fillo();
			Connection conn1=fill1.getConnection(Constants._ExcelPath);
			Recordset rs1=conn1.executeQuery("Select * from SplmtryData where Splmtry_ID = '"+TC_PmtInf_ID+"'");
			ArrayList<SplmtryData> al =  new ArrayList<SplmtryData>();
			while(rs1.next()){
				SplmtryData SplmtryData1 =null;
					Envlp Envlp1=new Envlp();
					Envlp1.setCnts(rs1.getField("Cnts_Value"));
					SplmtryData1=new SplmtryData();
					SplmtryData1.setEnvlp(Envlp1);
					al.add(SplmtryData1);
					CstmrCdtTrfInitn.setSplmtryData(al);
			}
			
			_addPmtInf.add(PmtInf);			
			CstmrCdtTrfInitn.setPmtInf1(_addPmtInf);
	//Ends PmtInf here		
			String CdtTrfTxInf_ID = rs3.getField("CdtTrfTxInf_ID");
			Fillo fill5 = new Fillo();
			Connection conn5=fill5.getConnection(Constants._ExcelPath);
			Recordset rs5=conn5.executeQuery("Select * from CdtTrfTxInf where CdtTrfTxInf_ID = '"+CdtTrfTxInf_ID+"'");
			ArrayList<CdtTrfTxInf> _addCdtTrfTxInf=new ArrayList<CdtTrfTxInf>();
			while (rs5.next()) {
				
//*********SplmtryData Starts here
	            System.setProperty("ROW", "2");
				Fillo fill1=new Fillo();
//				Connection conn1=fill1.getConnection(Constants._ExcelPath);
//				Recordset rs1=conn1.executeQuery("Select * from SplmtryData where Splmtry_ID = '"+TC_PmtInf_ID+"'");
				ArrayList<SplmtryData> al =  new ArrayList<SplmtryData>();
				ArrayList<Envlp> al1 =  new ArrayList<Envlp>();
//				while(rs1.next()){
					SplmtryData SplmtryData1 =null;
					SplmtryData SplmtryData2 =null;
						Envlp Envlp1=new Envlp();
						Envlp1.setCnts("hello");
						Envlp Envlp2=new Envlp();
						Envlp2.setCnts("hii");
						
						SplmtryData1=new SplmtryData();
						SplmtryData1.setEnvlp(Envlp1);
						SplmtryData2=new SplmtryData();
						SplmtryData2.setEnvlp(Envlp2);
						al.add(SplmtryData1);
						al.add(SplmtryData2);
						CstmrCdtTrfInitn.setSplmtryData(al);
//				}
//				}
	//*********SplmtryData ends here
	
				
				
				
//			CdtTrfTxInf starts here
			CdtTrfTxInf CdtTrfTxInf=new CdtTrfTxInf();
			
			//* ChrgBr
			CdtTrfTxInf.setChrgBr(rs5.getField("ChrgBr_value"));
			
			//* InstrForDbtrAgt
			CdtTrfTxInf.setInstrForDbtrAgt(rs5.getField("InstrForDbtrAgt"));
			
			//* PmtId
			PmtId PmtId=new PmtId();
			PmtId.setInstrId(rs5.getField("InstrId_value"));
			PmtId.setEndToEndId(rs5.getField("EndToEndId_value"));
			
			CdtTrfTxInf.setPmtId(PmtId);
			
			//Cdtr
			CdtrPstlAdr CdtrPstlAdr=new CdtrPstlAdr();
			CdtrPstlAdr.setCtry(rs5.getField("Ctry_value1"));
			CdtrPstlAdr.setStrtNm(rs5.getField("StrtNm_value1"));
			
			ArrayList CdtrPstlAdr_Address=new ArrayList();
			
			cdtrPstlAddress.getCdtrPstlAddress(rs5, CdtrPstlAdr_Address);
			
			if (!CdtrPstlAdr_Address.isEmpty()) {
				CdtrPstlAdr.setAdrLine(CdtrPstlAdr_Address);
			}
			
			Cdtr Cdtr=new Cdtr();
			Cdtr.setNm(rs5.getField("Nm_value1"));
			Cdtr.setPstlAdr(CdtrPstlAdr);
			
			CdtTrfTxInf.setCdtr(Cdtr);
			
			//Amt
			EqvtAmt EqvtAmt=new EqvtAmt();
			EqvtAmt.setAmt(rs5.getField("Eqt_Amt"));
			EqvtAmt.setCcyOfTrf(rs5.getField("Eqt_AmtCcyTransfer"));
			
			InstdAmt InstdAmt=new InstdAmt();
			InstdAmt.setAmt(rs5.getField("Amt_value"));
			InstdAmt.setCcy(rs5.getField("Ccy_value"));
			
			Amt Amt = new Amt();
			Amt.setEqvtAmt(EqvtAmt);
			Amt.setInstdAmt(InstdAmt);
			
			CdtTrfTxInf.setAmt(Amt);
			
			//Amt ends here
			
//			XchgRateInf starts here
			
			XchgRateInf XchgRateInf=new XchgRateInf();
			XchgRateInf.setXchgRate(rs5.getField("XchgRate_value"));
			XchgRateInf.setRateTp(rs5.getField("RateTp_value"));
			XchgRateInf.setCtrctId(rs5.getField("CtrctId_value"));
			
			CdtTrfTxInf.setXchgRateInf(XchgRateInf);
			
//			XchgRateInf Ends here		
			IntrmyAgt1FinInstnId IntrmyAgt1FinInstnId =new IntrmyAgt1FinInstnId();
			IntrmyAgt1FinInstnId.setBICFI(rs5.getField("BICFI_val"));
			
			IntrmyAgt1 IntrmyAgt1=new IntrmyAgt1();
			IntrmyAgt1.setFinInstnId(IntrmyAgt1FinInstnId);
			
			CdtTrfTxInf.setIntrmyAgt1(IntrmyAgt1);
			
//			CdtrAgt Starts here
			
			ClrSysId ClrSysId=new ClrSysId();
			ClrSysId.setCd(rs5.getField("Cd_ClrSysId_value"));
			
			ClrSysMmbId ClrSysMmbId=new ClrSysMmbId();
			ClrSysMmbId.setMmbId(rs5.getField("MmbId_value"));
			ClrSysMmbId.setClrSysId(ClrSysId);
			
			FinInstnId FinInstnId=new FinInstnId();
			FinInstnId.setClrSysMmbId(ClrSysMmbId);
			FinInstnId.setNm(rs5.getField("Nm_value"));
			
			CdtrAgtPstlAdr CdtrAgtPstlAdr=new CdtrAgtPstlAdr();
			CdtrAgtPstlAdr.setCtry(rs5.getField("Ctry_value"));
			CdtrAgtPstlAdr.setStrtNm("ELM ST");
			CdtrAgtPstlAdr.setBldgNb("420");
			CdtrAgtPstlAdr.setPstCd("1R1R1R");
			CdtrAgtPstlAdr.setTwnNm("NEW YORK");
			CdtrAgtPstlAdr.setCtrySubDvsn("NY");
			CdtrAgtPstlAdr.setCtry("US");
			
			ArrayList AddressCdtrAgt = new ArrayList();
			cdtrAgtpstladdress.getCdtrAgtPstlAdrAddress(rs5, AddressCdtrAgt);
			if (!AddressCdtrAgt.isEmpty()) {
				CdtrAgtPstlAdr.setAdrLine(AddressCdtrAgt);
			}	
			
			FinInstnId.setPstlAdr(CdtrAgtPstlAdr);
			
			CdtrAgt CdtrAgt=new CdtrAgt();
			CdtrAgt.setFinInstnId(FinInstnId);
			
			CdtTrfTxInf.setCdtrAgt(CdtrAgt);
			
			//CdtrAcct
			CdtrAcctOthr CdtrAcctOthr=new CdtrAcctOthr();
			CdtrAcctOthr.setId(rs5.getField("Id_value1"));
			CdtrAcctId  CdtrAcctId=new CdtrAcctId();
			CdtrAcctId.setOthr(CdtrAcctOthr);
			
			CdtrAcct CdtrAcct=new CdtrAcct();
			CdtrAcct.setId(CdtrAcctId);
			
			CdtTrfTxInf.setCdtrAcct(CdtrAcct);
			
//			CdtrAgt Ends here		
			
			
//	     RmtInf Starts here
			RmtInf RmtInf=new RmtInf();
			
			RmtInfGson.getRmtInf(rs5, RmtInf);
			
			CdtTrfTxInf.setRmtInf(RmtInf);
//		     RmtInf Ends here

//			SplmtryData Starts here
			Envlp Envlp=new Envlp();
			SplmntryCntsGson.getSplmtryData_Cnts(rs5, Envlp);
			
			SplmtryData SplmtryData=new SplmtryData();
			SplmtryData.setEnvlp(Envlp);
			
			CdtTrfTxInf.setSplmtryData(SplmtryData);
//			SplmtryData Ends here
			
//			Purp
			Purp Purp=new Purp();
			Purp.setPrtry(rs5.getField("Prtry_value"));
			
			CdtTrfTxInf.setPurp(Purp);
			
//			CdtTrfTxInf CdtTrfTxInf1[]={CdtTrfTxInf};
//			PmtInf.setCdtTrfTxInf(CdtTrfTxInf1);
			_addCdtTrfTxInf.add(CdtTrfTxInf);
			PmtInf.setCdtTrfTxInf1(_addCdtTrfTxInf);
		}	
	}
	//CdtTrfTxInf Ends here here
			
			Document Document=new Document();
			Document.setCstmrCdtTrfInitn(CstmrCdtTrfInitn);
			
			RootClass RootClass=new RootClass();
			RootClass.setDocument(Document);
			
			Gson Gson =new GsonBuilder().setPrettyPrinting().create();
			JsonElement str=Gson.toJsonTree(RootClass);
//			String str2= Gson.toJson(str);
			String prettyJsonString = Gson.toJson(str);
			
			json_console = new PrintStream(new File("D:\\Core_ComponentTool\\_Output\\"+TestCaseID+"_Pain001_REQ.json"));
			System.setOut(json_console);
			System.out.println(prettyJsonString);
			
//			Gson.toJson(RootClass, new FileWriter("D:\\Core_ComponentTool\\_Output\\"+TestCaseID+"_Pain001_REQ.json"));
//			String jsonInString = Gson.toJson(RootClass);
			
//			POST_requestApi.ExecutePOST_API(TestCaseID,_API_Link,channelId,service_type,flow_id);
		}	
		
	}
*/	
}
