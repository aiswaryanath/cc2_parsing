package APIFrameworkGsonController;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class ExtentDemo {
	static ExtentTest test;
	static ExtentReports report;

	@BeforeClass
	public static void startTest() {
		report = new ExtentReports("D:\\Core_ComponentTool\\Results\\ExtentReportResults.html");
		test = report.startTest("ExtentDemo");
		System.out.println("Hello");
	}

	@Test
	public void extentReportsDemo() {
		System.setProperty("webdriver.chrome.driver","D:/Core_ComponentTool/IE_Driver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.co.in");
		if (driver.getTitle().equals("Google")) {
			test.log(LogStatus.PASS, "Navigated to the specified URL");
		} else {
			test.log(LogStatus.FAIL, "Test Failed");
		}
		
		if (driver.getTitle().equals("Google1")) {
			test.log(LogStatus.PASS, "Navigated to the specified URL");
		} else {
			test.log(LogStatus.FAIL, "Test Failed");
		}
		
	System.out.println("TEST ONE");	
	}

	@AfterClass
	public static void endTest() {
		report.endTest(test);
		report.flush();
	}
	
}