package APIFrameworkGsonController;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Utility.GetSimpleDateFormat;

import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;

public class API_GsonFrame extends JFrame {

	public static JPanel contentPane;
	public static JProgressBar progressBar;
	static JProgressBar progressBar_1;
	/**
	 * Launch the application.
	 **/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					API_GsonFrame frame = new API_GsonFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void fill(JProgressBar progressBar) 
    { 
        int i = 0; 
        try { 
            while (i <= 100) { 
                // fill the menu bar
            	Thread.sleep(1000);
                progressBar.setValue(i + 10);
                System.out.println(i + 10);
                i += 20; 
            } 
        } 
        catch (Exception e) { 
        } 
    } 
	/**
	 * Create the frame.
	 */
	public API_GsonFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("CoreComponent - Automation Suite");
		lblNewLabel.setBackground(new Color(102, 205, 170));
		lblNewLabel.setBounds(106, 11, 240, 28);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Execute AutoamtionTest Suite");
		btnNewButton.addActionListener(new ActionListener() {
			int count=0;
			public void actionPerformed(ActionEvent e) {
				String []array=null;
				try {
					fill(progressBar_1);
//					GsonAPI_DataManipulation.main(array);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(95, 63, 276, 23);
		contentPane.add(btnNewButton);
		JButton btnSetConfigurationSuite = new JButton("Set Configuration Suite");
		btnSetConfigurationSuite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				GetSimpleDateFormat.OpenExcel();
				}catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnSetConfigurationSuite.setBounds(116, 111, 230, 23);
		contentPane.add(btnSetConfigurationSuite);
		
		JButton btnGetAutomationResult = new JButton("Get Automation Result");
		btnGetAutomationResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					GetSimpleDateFormat.OpenExcel();
				}catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		
		btnGetAutomationResult.setBounds(116, 166, 230, 23);
		contentPane.add(btnGetAutomationResult);
		
		progressBar_1 = new JProgressBar();
		progressBar_1.setBounds(147, 200, 146, 14);
		progressBar_1.setStringPainted(true);
		contentPane.add(progressBar_1);
		
	}
}
