/***************************************************************************************************/
/* Copyright © 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : GsonAPI_DataManipulation */
/* */
/* File Name : GsonAPI_DataManipulation.java */
/* */
/* Description :Manipulates data by Retieving data from Excel for Generating JSON in PAIN001 Format*/
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIFrameworkGsonController;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import APIGsonKeyValueConditions.AuthstnGson;
import APIGsonKeyValueConditions.CdtrAcctGson;
import APIGsonKeyValueConditions.CdtrGson;
import APIGsonKeyValueConditions.ChrgsAcctGson;
import APIGsonKeyValueConditions.ClrSysIdGson;
import APIGsonKeyValueConditions.ClrSysMmbIdGson;
import APIGsonKeyValueConditions.DbtrAccGson;
import APIGsonKeyValueConditions.DbtrAgtBrnchIdGson;
import APIGsonKeyValueConditions.DbtrAgtGson;
import APIGsonKeyValueConditions.DbtrOrgIdGson;
import APIGsonKeyValueConditions.DbtrPstAdlGson;
import APIGsonKeyValueConditions.EqvtAmtGson;
import APIGsonKeyValueConditions.FinInstIdGson;
import APIGsonKeyValueConditions.GetKeyChrBr;
import APIGsonKeyValueConditions.GrpHdrGson;
import APIGsonKeyValueConditions.InitgPtyGson;
import APIGsonKeyValueConditions.InitialPrtyNmGson;
import APIGsonKeyValueConditions.InstdAmtGson;
import APIGsonKeyValueConditions.IntrmyAgtGson;
import APIGsonKeyValueConditions.PmTpInfSvcLvlGson;
import APIGsonKeyValueConditions.PmtIdGson;
import APIGsonKeyValueConditions.PmtInfGsonkeyValue;
import APIGsonKeyValueConditions.RmtInfGson;
import APIGsonKeyValueConditions.SplmntryCntsGson;
import APIGsonKeyValueConditions.XchgRateInfGson;
import APIGsonKeyValueConditions.cdtrAgtpstladdress;
import APIGsonKeyValueConditions.cdtrPstlAddress;
import APIGsonKeyValueConditions.dbtrPstlAddress;
import Amt.Amt;
import Amt.EqvtAmt;
import Amt.InstdAmt;
import Api_Operaitons.POST_requestApi;
import Cdtr.Cdtr;
import Cdtr.CdtrPstlAdr;
import CdtrAcct.CdtrAcct;
import CdtrAcct.CdtrAcctId;
import CdtrAcct.CdtrAcctOthr;
import CdtrAgt.CdtrAgt;
import CdtrAgt.CdtrAgtPstlAdr;
import CdtrAgt.ClrSysId;
import CdtrAgt.ClrSysMmbId;
//import ClrSysMmbId.ClrSysMmbId;
import CdtrAgt.FinInstnId;
import ChrgsAcct.ChrgsAcctId;
import ChrgsAcct.ChrgsAcctOthr;
import Constants_pack.Constants;
import Dbtr.Dbtr;
import Dbtr.DbtrId;
import Dbtr.DbtrOrgId;
import Dbtr.DbtrOthr;
import Dbtr.PstlAdr;
import DbtrAcct.DbtrAcct;
import DbtrAcct.DbtrAcctId;
import DbtrAcct.DbtrAcctOthr;
import DbtrAgt.DbtrAgt;
import DbtrAgt.DbtrAgtBrnchId;
import DbtrAgt.DbtrAgtFinInstnId;
import DbtrAgt.DbtrAgtOthr;
import DbtrAgt.DbtrAgtPstlAdr;
import InitgPty.Id;
import InitgPty.InitgPty;
import InitgPty.OrgId;
import InitgPty.Othr;
import IntrmyAgt1.IntrmyAgt1;
import IntrmyAgt1.IntrmyAgt1FinInstnId;
import JSONStructure_PAIN001.Authstn;
import JSONStructure_PAIN001.CdtTrfTxInf;
import JSONStructure_PAIN001.CstmrCdtTrfInitn;
import JSONStructure_PAIN001.Document;
import JSONStructure_PAIN001.GrpHdr;
import JSONStructure_PAIN001.PmtInf;
import JSONStructure_PAIN001.RootClass;
import MT103UI.GetValueMT103;
import MT103UI.TestMT103UI;
import MT199.MT199;
import MT199UI.TestMT199UI;
import MT900UI.MT900;
import MT900UI.TestMT900UI;
import ParseRecordExcel.Update_ExcelResult;
import PmtId.PmtId;
import PmtTpInf.LclInstrm;
import PmtTpInf.PmtTpInf;
import PmtTpInf.SvcLvl;
import Purp.Purp;
import ReadDBConfiguration.ReadDB;
import RmtInf.RmtInf;
import SplmtryData.Cnts;
import SplmtryData.Envlp;
import SplmtryData.SplmtryData;
import Utility.ClearCellData;
//import UI_CC2_ManualAction.CC2_LinearScript;
import Utility.GetSimpleDateFormat;
import Utility.HTML_report;
import Utility.SQL_Runner;
import Utility.SaltString;
import XchgRateInf.XchgRateInf;

public class GsonAPI_DataManipulation {
static PrintStream json_console = null;
static String _API_Link = ReadDB.getAPI_URL();
static String channelId = ReadDB.getchannelId();
static String service_type = ReadDB.getservice_type();
static String flow_id = ReadDB.getflow_id();

public static void main(String[] args) throws FileNotFoundException, FilloException,Exception {
try{
	System.setProperty("ROW", "2");
	System.out.println("CC2 Automation in progress..");
	SQL_Runner.PREExecute_QueriesCC2(System.getProperty("user.dir")+"//Core_ComponentTool//Test_Data//CC2_PreQueries.sql");
//	ClearCellData.clearActualData();
	   Timestamp timestamp = new Timestamp(System.currentTimeMillis());
       System.out.println("Start time:"+timestamp);
	Fillo fill=new Fillo();
	Connection conn=fill.getConnection(System.getProperty("user.dir")+Constants_pack.Constants._ExcelPath);
	Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
		 while (rs.next()) {
			GrpHdr GrpHdr =new GrpHdr();
			String TestCaseID = rs.getField("Test_ID");
			System.out.println("Current Testcase:"+TestCaseID);
			String _MsgId = null;
			if (rs.getField("Custom_Flag_Var").equalsIgnoreCase("Y")) {
				_MsgId = SaltString.getSaltString(10);
				 GrpHdr.setMsgId(_MsgId);
				String _CreDtTm = GetSimpleDateFormat.getSimpleDateFormat();
//				String _CreDtTm = "2019-06-21T15:48:38";
				GrpHdr.setCreDtTm(_CreDtTm);
			}else{
				GrpHdrGson.getGrpHdrMsgId(rs, GrpHdr);
			}

			Update_ExcelResult.Update_DataReportAck(TestCaseID, _MsgId, "Message_ID");
			//NbOfTxs
			GrpHdrGson.GetgrpHdrsetNbOfTxs(rs, GrpHdr);
			//CtrlSum
			GrpHdrGson.GetsetCtrlSum(rs, GrpHdr);
			
			Authstn Authstn=new Authstn();
			AuthstnGson.GetAuthstn(rs, Authstn);
			//WIP Here Varun
			if (!rs.getField("Prtry_value").isEmpty()) {
				GrpHdr.setAuthstn(Authstn);
			}

			//InitgPty
			Othr Othr=new Othr();
			InitgPtyGson.InitgPtyOthrGson(rs, Othr);
			
			OrgId OrgId=new OrgId();
			OrgId.setOthr(Othr);
			
			Id Id=new Id();
			Id.setOrgId(OrgId);
			InitgPty InitgPty=new InitgPty();
			InitgPtyGson.InitgPtyIdGson(rs, InitgPty);
			if (!rs.getField("Id_value").isEmpty()) {
				InitgPty.setId(Id);
			}
			GrpHdr.setInitgPty(InitgPty);
	//****************************************************PmtInf****************************************************
			String TC_PmtInf_ID = rs.getField("TC_PmtInf_ID");
			Fillo fill3 = new Fillo();
			Connection conn3=fill3.getConnection(System.getProperty("user.dir")+Constants._ExcelPath);
			Recordset rs3=conn3.executeQuery("Select * from PMTINF where TC_PmtInf_ID = '"+TC_PmtInf_ID+"'");
			ArrayList<PmtInf> _addPmtInf = new ArrayList<PmtInf>();
			CstmrCdtTrfInitn CstmrCdtTrfInitn=null;
		while(rs3.next()){
//			System.out.println(""+rs3.getField("TC_PmtInf_ID"));
			CstmrCdtTrfInitn=new CstmrCdtTrfInitn();
			CstmrCdtTrfInitn.setGrpHdr(GrpHdr);
//			PmtTpInf Starts here
			PmtInf PmtInf =new PmtInf();
			PmtInfGsonkeyValue.PmtInfIdGson(rs3, PmtInf);

//			PmtTpInf Ends here
			SvcLvl SvcLvl=new SvcLvl();
			
		if (!rs3.getField("PmtTpInf_SvcLvl").isEmpty()) {
			PmTpInfSvcLvlGson.getSvcLvl(rs3, SvcLvl);
		}	
			LclInstrm LclInstrm=new LclInstrm();
			
		if (!rs3.getField("PmtTpInf_LclInstrm").isEmpty()) {
			PmTpInfSvcLvlGson.getLclInstrm(rs3, LclInstrm);
		}	

		PmtTpInf PmtTpInf=new PmtTpInf();
		if (!rs3.getField("PmtTpInf_LclInstrm").isEmpty()) {	
			PmtTpInf.setLclInstrm(LclInstrm);
		}
		if (!rs3.getField("PmtTpInf_SvcLvl").isEmpty()) {	
			PmtTpInf.setSvcLvl(SvcLvl);
		}	

		PmtInf.setPmtTpInf(PmtTpInf);
//		PmtTpInf Ends here

//			Dbtr Starts Here
			PstlAdr PstlAdr=new PstlAdr();
			//Varun WIP
			DbtrPstAdlGson.getDbtrPstlAdr(rs3, PstlAdr);
			
			ArrayList Address = new ArrayList();
			dbtrPstlAddress.getDbtrPstlAddress(rs3, Address);
			if (!Address.isEmpty()) {
				PstlAdr.setAdrLine(Address);
			}

			DbtrOthr DbtrOthr=new DbtrOthr();
			DbtrOrgIdGson.getDbtrOrgId(rs3, DbtrOthr);
			
			DbtrOrgId DbtrOrgId=new DbtrOrgId();
			//DbtrOrgId.setOthr(DbtrOthr);
			//DbtrOrgId.;
			//VarunUpdated
			getAnyBIC(rs3, DbtrOrgId);
		
			DbtrId  DbtrId=new DbtrId();
			DbtrId.setOrgId(DbtrOrgId);
			
			Dbtr Dbtr=new Dbtr();
			if (!rs3.getField("AnyBIC").isEmpty()) {
				Dbtr.setId(DbtrId);
			}
			
			InitialPrtyNmGson.getInitgPtyNameGson(rs3, Dbtr);
			Dbtr.setPstlAdr(PstlAdr);

			PmtInf.setDbtr(Dbtr);
//			Dbtr Ends here

//			ChrgsAcct Starts here
			ChrgsAcctOthr Othr1=new ChrgsAcctOthr();
			
			ChrgsAcctId  Id1=new ChrgsAcctId();
			Id1.setOthr(Othr1);
			
			ChrgsAcct.ChrgsAcct ChrgsAcct = new ChrgsAcct.ChrgsAcct();
			//added varunToday
			if (!rs3.getField("Id_1value").isEmpty()){	
				SetChargesAccountId(rs3, Othr1);
				ChrgsAcct.setId(Id1);
			}
			
			if (!rs3.getField("ChargesAccount_ccy").isEmpty()) {
				ChrgsAcctGson.getChrgsAcctCcy(rs3, ChrgsAcct);
			}
		
		if (!rs3.getField("ChargesAccount_ccy").isEmpty() || !rs3.getField("Id_1value").isEmpty()) {
			PmtInf.setChrgsAcct(ChrgsAcct);
			if (!rs3.getField("ChargesAccount_ccy").isEmpty() && !rs3.getField("Id_1value").isEmpty()) {
				PmtInf.setChrgsAcct(ChrgsAcct);
			}
		}

//			DbtrAcct Starts here
			
			DbtrAcctOthr DbtrAcctOthr=new DbtrAcctOthr();
			DbtrAccGson.getDbtrAcct(rs3, DbtrAcctOthr);
			
			DbtrAcctId DbtrAcctId=new DbtrAcctId();
			DbtrAcctId.setOthr(DbtrAcctOthr);
			
			DbtrAcct DbtrAcct=new DbtrAcct();
//			Added below to hide debtor Account
			if (!rs3.getField("Id_2value").isEmpty()) {
				DbtrAcct.setId(DbtrAcctId);
			}
			DbtrAccGson.getDbtrAcctNm(rs3, DbtrAcct);
			
// 		Varun	
//		if (!rs3.getField("Id_2value").isEmpty()) {
			PmtInf.setDbtrAcct(DbtrAcct);
//		}

//			DbtrAgt Starts here
			DbtrAgtBrnchId BrnchId=new DbtrAgtBrnchId();
			DbtrAgtBrnchIdGson.getDbtrAgtBrnchId(rs3, BrnchId);

			DbtrAgtPstlAdr DbtrAgtPstlAdr=new DbtrAgtPstlAdr();
			getDbtrCtry(rs3, DbtrAgtPstlAdr);
			
			DbtrAgtOthr DbtrAgtOthr=new DbtrAgtOthr();
			DbtrAgtSetID(rs3, DbtrAgtOthr);
			
			DbtrAgtFinInstnId DbtrAgtFinInstnId=new DbtrAgtFinInstnId();
			DbtrAgtGson.getDbtrAgt(rs3, DbtrAgtFinInstnId);
			
			if (!rs3.getField("Ctry_value1").isEmpty()) {
				DbtrAgtFinInstnId.setPstlAdr(DbtrAgtPstlAdr);
			}
			
			if (!rs3.getField("Id_3value").isEmpty()) {
				DbtrAgtFinInstnId.setOthr(DbtrAgtOthr);
			}
			
			DbtrAgt DbtrAgt=new DbtrAgt();
			if (!rs3.getField("Id_3valueF").isEmpty()) {
				DbtrAgt.setBrnchId(BrnchId);
			}
			
			DbtrAgt.setFinInstnId(DbtrAgtFinInstnId);
			PmtInf.setDbtrAgt(DbtrAgt);
					
			System.setProperty("ROW", "2");
			Fillo fill1=new Fillo();
			Connection conn1=fill1.getConnection(System.getProperty("user.dir")+Constants._ExcelPath);
			Recordset rs1=conn1.executeQuery("Select * from SplmtryData where Splmtry_ID = '"+TC_PmtInf_ID+"'");
			ArrayList<SplmtryData> al =  new ArrayList<SplmtryData>();
			//varunchanged ---v1.0
			while(rs1.next()){
				SplmtryData SplmtryData1 =null;
					Envlp Envlp1=new Envlp();
					Cnts Cnts=new Cnts();
					//ash30
					SplmntryCntsGson.getPANID(rs1, Cnts);
					SplmntryCntsGson.getFXId(rs1, Cnts);
					
					Envlp1.setCnts(Cnts);
					SplmtryData1=new SplmtryData();
					SplmtryData1.setEnvlp(Envlp1);
					al.add(SplmtryData1);
					
				if (!rs1.getField("PANID").isEmpty() || !rs1.getField("FXId").isEmpty()) {
					CstmrCdtTrfInitn.setSplmtryData(al);
						if (!rs1.getField("PANID").isEmpty() && !rs1.getField("FXId").isEmpty()) {
						CstmrCdtTrfInitn.setSplmtryData(al);
						}
					}
			}
			_addPmtInf.add(PmtInf);
			CstmrCdtTrfInitn.setPmtInf1(_addPmtInf);
	//Ends PmtInf here
			String CdtTrfTxInf_ID = rs3.getField("CdtTrfTxInf_ID");
			Fillo fill5 = new Fillo();
			Connection conn5=fill5.getConnection(System.getProperty("user.dir")+Constants._ExcelPath);
			Recordset rs5=conn5.executeQuery("Select * from CdtTrfTxInf where CdtTrfTxInf_ID = '"+CdtTrfTxInf_ID+"'");
			ArrayList<CdtTrfTxInf> _addCdtTrfTxInf=new ArrayList<CdtTrfTxInf>();
			while (rs5.next()) {
//			System.out.println(rs5.getField("CdtTrfTxInf_ID"));
//			CdtTrfTxInf starts here
			CdtTrfTxInf CdtTrfTxInf=new CdtTrfTxInf();
			
			// ChrgBr
			GetKeyChrBr.getChrBrGson(rs5, CdtTrfTxInf);
			
			// InstrForDbtrAgt
			getInstrForDbtrAgt(rs5, CdtTrfTxInf);
			
			// PmtId
			PmtId PmtId=new PmtId();
			//WIP Varun

			PmtIdGson.getPmtID(rs5, PmtId);
			CdtTrfTxInf.setPmtId(PmtId);
			
			//Cdtr
			CdtrPstlAdr CdtrPstlAdr=new CdtrPstlAdr();
			CdtrGson.getCdtrPstlAdr(rs5, CdtrPstlAdr);
			
			ArrayList CdtrPstlAdr_Address=new ArrayList();
			cdtrPstlAddress.getCdtrPstlAddress(rs5, CdtrPstlAdr_Address);
			
			if (!CdtrPstlAdr_Address.isEmpty()) {
				CdtrPstlAdr.setAdrLine(CdtrPstlAdr_Address);
			}

			Cdtr Cdtr=new Cdtr();
			CdtrGson.getCdtrNm(rs5, Cdtr);
			Cdtr.setPstlAdr(CdtrPstlAdr);
			CdtTrfTxInf.setCdtr(Cdtr);

			//Amt
			EqvtAmt EqvtAmt=new EqvtAmt();
			EqvtAmtGson.getEqvtAmt(rs5, EqvtAmt);
			
			InstdAmt InstdAmt=new InstdAmt();
			InstdAmtGson.getInstdAmt(rs5, InstdAmt);
			
			Amt Amt = new Amt();
			if ((!rs5.getField("Eqt_Amt").isEmpty()) || (!rs5.getField("Eqt_AmtCcyTransfer").isEmpty())  || (!rs5.getField("Eqt_AmtCcy").isEmpty()) ) {
				Amt.setEqvtAmt(EqvtAmt);
				if ((!rs5.getField("Eqt_Amt").isEmpty()) && (!rs5.getField("Eqt_AmtCcyTransfer").isEmpty())  && (!rs5.getField("Eqt_AmtCcy").isEmpty())  ) {
					Amt.setEqvtAmt(EqvtAmt);
				}
			}

			if ((!rs5.getField("Ccy_value").isEmpty()) || (!rs5.getField("Amt_value").isEmpty())) {
				Amt.setInstdAmt(InstdAmt);
				if ((!rs5.getField("Ccy_value").isEmpty()) && (!rs5.getField("Amt_value").isEmpty())) {
					Amt.setInstdAmt(InstdAmt);
				}
			}
			CdtTrfTxInf.setAmt(Amt);
//			Amt ends here
			
//			XchgRateInf starts here

			XchgRateInf XchgRateInf=new XchgRateInf();
			XchgRateInfGson.getXchgRateInf(rs5, XchgRateInf);

			if ((!rs5.getField("XchgRate_value").isEmpty()) || (!rs5.getField("RateTp_value").isEmpty())  || (!rs5.getField("CtrctId_value").isEmpty())) {
					CdtTrfTxInf.setXchgRateInf(XchgRateInf);
				if ((!rs5.getField("XchgRate_value").isEmpty()) && (!rs5.getField("RateTp_value").isEmpty())  && (!rs5.getField("CtrctId_value").isEmpty())) {
					CdtTrfTxInf.setXchgRateInf(XchgRateInf);
				}
			}

//			XchgRateInf Ends here		
			IntrmyAgt1FinInstnId IntrmyAgt1FinInstnId =new IntrmyAgt1FinInstnId();
			IntrmyAgtGson.getIntrmyAgt1(rs5, IntrmyAgt1FinInstnId);
			
			IntrmyAgt1 IntrmyAgt1=new IntrmyAgt1();
			IntrmyAgt1.setFinInstnId(IntrmyAgt1FinInstnId);
			ClrSysId ClrSysId1=new ClrSysId();
			ClrSysId1.setCd("CACPA");
			ClrSysMmbId ClrSysMmbId1=new ClrSysMmbId();
			ClrSysMmbId1.setClrSysId(ClrSysId1);
			ClrSysMmbId1.setMmbId("12345");
			//IntrmyAgt1.setClrSysMmbId(ClrSysMmbId1);
			
			if(!rs5.getField("BICFI_val").isEmpty()){
				CdtTrfTxInf.setIntrmyAgt1(IntrmyAgt1);
			}

//			CdtrAgt Starts here
			ClrSysId ClrSysId=new ClrSysId();
			ClrSysIdGson.getClrSysId(rs5, ClrSysId);
			
			ClrSysMmbId ClrSysMmbId=new ClrSysMmbId();
			ClrSysMmbIdGson.getClrSysMmbId(rs5, ClrSysMmbId);
			
			if (!rs5.getField("Cd_ClrSysId_value").isEmpty()) {
				ClrSysMmbId.setClrSysId(ClrSysId);
			}
			
			if (!rs5.getField("Prtry_ClrSysId_value").isEmpty()) {
				ClrSysMmbId.setClrSysId(ClrSysId);
			}
			
			FinInstnId FinInstnId=new FinInstnId();
			if (!rs5.getField("MmbId_value").isEmpty()) {
				FinInstnId.setClrSysMmbId(ClrSysMmbId);
			}

			FinInstIdGson.getFinInstnIdSetNm(rs5, FinInstnId);
			
			CdtrAgtPstlAdr CdtrAgtPstlAdr=new CdtrAgtPstlAdr();
			cdtrAgtpstladdress.getCtrAgtPstAdr(rs5, CdtrAgtPstlAdr);

			ArrayList AddressCdtrAgt = new ArrayList();
			cdtrAgtpstladdress.getCdtrAgtPstlAdrAddress(rs5, AddressCdtrAgt);
			if (!AddressCdtrAgt.isEmpty()) {
				CdtrAgtPstlAdr.setAdrLine(AddressCdtrAgt);
			}	

			FinInstnId.setPstlAdr(CdtrAgtPstlAdr);
			getBICFI(rs5, FinInstnId);

			CdtrAgt CdtrAgt=new CdtrAgt();
			CdtrAgt.setFinInstnId(FinInstnId);
			
			CdtTrfTxInf.setCdtrAgt(CdtrAgt);

			//CdtrAcct
			//added varun 29.8.2019
			if(!rs5.getField("Id_value1").isEmpty()) {
				CdtrAcctOthr CdtrAcctOthr=new CdtrAcctOthr();
				CdtrAcctGson.getCdtrAccIdNm(rs5, CdtrAcctOthr);
				
				CdtrAcctId  CdtrAcctId=new CdtrAcctId();
				CdtrAcctId.setOthr(CdtrAcctOthr);
				
				CdtrAcct CdtrAcct=new CdtrAcct();
				CdtrAcct.setId(CdtrAcctId);
				CdtTrfTxInf.setCdtrAcct(CdtrAcct);
			}
			
			if(!rs5.getField("IBAN").isEmpty()) {
				
				CdtrAcctId  CdtrAcctId=new CdtrAcctId();
				getIBAN(rs5, CdtrAcctId);
				
				CdtrAcct CdtrAcct=new CdtrAcct();
				CdtrAcct.setId(CdtrAcctId);
				CdtTrfTxInf.setCdtrAcct(CdtrAcct);
			}
//		  	CdtrAgt Ends here //	        RmtInf Starts here
			RmtInf RmtInf=new RmtInf();
			RmtInfGson.getRmtInf(rs5, RmtInf); //Varun WIP
			if (!rs5.getField("RmtInf_value").isEmpty()){
					CdtTrfTxInf.setRmtInf(RmtInf);
			}
//		     RmtInf Ends here//			SplmtryData Starts here
			Envlp Envlp=new Envlp();
			Cnts Cnts =new Cnts();
			SplmntryCntsGson.getStlMth(rs5, Cnts);
			SplmntryCntsGson.getPymtAmt(rs5, Cnts);
			SplmntryCntsGson.getCcy(rs5, Cnts);
			SplmntryCntsGson.getDbAmt(rs5, Cnts);

			Envlp.setCnts(Cnts);
			SplmtryData SplmtryData=new SplmtryData();
			SplmtryData.setEnvlp(Envlp);
			
			if (!rs5.getField("StlmMth").isEmpty() || !rs5.getField("PymtAmt").isEmpty() || !rs5.getField("Ccy").isEmpty() || !rs5.getField("DbAmt").isEmpty()){
				CdtTrfTxInf.setSplmtryData(SplmtryData);
				if (!rs5.getField("StlmMth").isEmpty() && !rs5.getField("PymtAmt").isEmpty() && !rs5.getField("Ccy").isEmpty() && !rs5.getField("DbAmt").isEmpty()){
					CdtTrfTxInf.setSplmtryData(SplmtryData);
				}
			}
//			SplmtryData Ends here
			Purp Purp=new Purp();
			getPurp(rs5, Purp);
			if (!rs5.getField("Prtry_value").isEmpty()){
				CdtTrfTxInf.setPurp(Purp);
			}
			_addCdtTrfTxInf.add(CdtTrfTxInf);
			PmtInf.setCdtTrfTxInf1(_addCdtTrfTxInf);
		}	
	}

	//CdtTrfTxInf Ends here here
			Document Document=new Document();
			Document.setCstmrCdtTrfInitn(CstmrCdtTrfInitn);

			RootClass RootClass=new RootClass();
			RootClass.setDocument(Document);

			Gson Gson =new GsonBuilder().setPrettyPrinting().create();
			JsonElement str=Gson.toJsonTree(RootClass);
			String prettyJsonString = Gson.toJson(str);

		     FileWriter fw=new FileWriter(System.getProperty("user.dir")+"\\Core_ComponentTool\\_Output\\"+TestCaseID+"_Pain001_REQ.json");    
	           fw.write(prettyJsonString);   
	           System.out.println(TestCaseID+" END");
	         fw.close();    

		    POST_requestApi.ExecutePOST_API(TestCaseID,_API_Link,channelId,service_type,flow_id);
		    Timestamp timestamp1 = new Timestamp(System.currentTimeMillis());
		    System.out.println("end time of"+TestCaseID +":"+timestamp1);
		
		 	
		   	//Here
		}
	 }catch (Exception e) {
	 	 System.out.println("No Records Found for PAIN001 !!");
	 	 e.printStackTrace();
	 }
finally
{
	   Timestamp timestamp = new Timestamp(System.currentTimeMillis());
       System.out.println("end time full execution:"+timestamp);
}
/*	HTML_report ob=new HTML_report();
	ob.GenerateHTML_report();
//		 MTFileUIAction.PUSHMTFiles();
//		 Execute_QueriesTestToolFileName.Get_SQLQueriesTestToolFile();
//		 FileCopy.CopyTestDataExcel();
		 ZipFiles.WinZip_JSONFiles();*/
}


public static void DbtrAgtSetID(Recordset rs, DbtrAgtOthr DbtrAgtOthr) throws FilloException {
	
	if (!rs.getField("Id_3value").isEmpty() && !rs.getField("Id_3value").equalsIgnoreCase("M") && !rs.getField("Id_3value").equalsIgnoreCase("I") && !rs.getField("Id_3value").contains("#I") && !rs.getField("Id_3value").contains("#S")) {
		DbtrAgtOthr.setId(rs.getField("Id_3value"));
	}else if(rs.getField("Id_3value").equalsIgnoreCase("M")) {
		DbtrAgtOthr.setId("");
	}else if(rs.getField("Id_3value").isEmpty()) {
		
	}else if(rs.getField("Id_3value").contains("#I")){
		DbtrAgtOthr.setIdX(rs.getField("Id_3value").substring(2,rs.getField("Id_3value").length()));
	}else if(rs.getField("Id_3value").contains("#S")){
		DbtrAgtOthr.setIdX(rs.getField("Id_3value").substring(2,rs.getField("Id_3value").length()));
		DbtrAgtOthr.setId(rs.getField("Id_3value").substring(2,rs.getField("Id_3value").length()));
	}
	
}

public static void getDbtrCtry(Recordset rs, DbtrAgtPstlAdr DbtrAgtPstlAdr) throws FilloException {
	
	if (!rs.getField("Ctry_value1").isEmpty() && !rs.getField("Ctry_value1").equalsIgnoreCase("M") && !rs.getField("Ctry_value1").equalsIgnoreCase("I") && !rs.getField("Ctry_value1").contains("#I") && !rs.getField("Ctry_value1").contains("#S")) {
		DbtrAgtPstlAdr.setCtry(rs.getField("Ctry_value1"));
	}else if(rs.getField("Ctry_value1").equalsIgnoreCase("M")) {
		DbtrAgtPstlAdr.setCtry("");
	}else if(rs.getField("Ctry_value1").isEmpty()) {
		
	}else if(rs.getField("Ctry_value1").contains("#I")){
		DbtrAgtPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
	}else if(rs.getField("Ctry_value1").contains("#S")){
		DbtrAgtPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
		DbtrAgtPstlAdr.setCtry(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
	}
}

public static void getIBAN(Recordset rs, CdtrAcctId CdtrAcctId) throws FilloException {
	
	if (!rs.getField("IBAN").isEmpty() && !rs.getField("IBAN").equalsIgnoreCase("M") && !rs.getField("IBAN").equalsIgnoreCase("I") && !rs.getField("IBAN").contains("#I") && !rs.getField("IBAN").contains("#S")) {
		CdtrAcctId.setIBAN(rs.getField("IBAN"));
	}else if(rs.getField("IBAN").equalsIgnoreCase("M")) {
		CdtrAcctId.setIBAN("");
	}else if(rs.getField("IBAN").isEmpty()) {
		
	}else if(rs.getField("IBAN").contains("#I")){
		CdtrAcctId.setIBANX(rs.getField("IBAN").substring(2,rs.getField("IBAN").length()));
	}else if(rs.getField("IBAN").contains("#S")){
		CdtrAcctId.setIBANX(rs.getField("IBAN").substring(2,rs.getField("IBAN").length()));
		CdtrAcctId.setIBAN(rs.getField("IBAN").substring(2,rs.getField("IBAN").length()));
	}
}

public static void getAnyBIC(Recordset rs, DbtrOrgId DbtrOrgId) throws FilloException {
		
		if (!rs.getField("AnyBIC").isEmpty() && !rs.getField("AnyBIC").equalsIgnoreCase("M") && !rs.getField("AnyBIC").equalsIgnoreCase("I") && !rs.getField("AnyBIC").contains("#I") && !rs.getField("AnyBIC").contains("#S")) {
			DbtrOrgId.setAnyBIC(rs.getField("AnyBIC"));
		}else if(rs.getField("AnyBIC").equalsIgnoreCase("M")) {
			DbtrOrgId.setAnyBIC("");
		}else if(rs.getField("AnyBIC").isEmpty()) {
			
		}else if(rs.getField("AnyBIC").contains("#I")){
			DbtrOrgId.setAnyBICX(rs.getField("AnyBIC").substring(2,rs.getField("AnyBIC").length()));
		}else if(rs.getField("AnyBIC").contains("#S")){
			DbtrOrgId.setAnyBICX(rs.getField("AnyBIC").substring(2,rs.getField("AnyBIC").length()));
			DbtrOrgId.setAnyBIC(rs.getField("AnyBIC").substring(2,rs.getField("AnyBIC").length()));
		}
	}

public static void SetChargesAccountId(Recordset rs, ChrgsAcctOthr Othr1) throws FilloException {
		if (!rs.getField("Id_1value").isEmpty() && !rs.getField("Id_1value").equalsIgnoreCase("M") && !rs.getField("Id_1value").equalsIgnoreCase("I") && !rs.getField("Id_1value").contains("#I") && !rs.getField("Id_1value").contains("#S")) {
			Othr1.setId(rs.getField("Id_1value"));
		}else if(rs.getField("Id_1value").equalsIgnoreCase("M")) {
			Othr1.setId("");
		}else if(rs.getField("Id_1value").isEmpty()) {
			
		}else if(rs.getField("Id_1value").contains("#I")){
			Othr1.setIdX(rs.getField("Id_1value").substring(2,rs.getField("Id_1value").length()));
		}else if(rs.getField("Id_1value").contains("#S")){
			Othr1.setIdX(rs.getField("Id_1value").substring(2,rs.getField("Id_1value").length()));
			Othr1.setId(rs.getField("Id_1value").substring(2,rs.getField("Id_1value").length()));
		}
	}

public static void getBICFI(Recordset rs, FinInstnId FinInstnId) throws FilloException {
	
	if (!rs.getField("_BICFI").isEmpty() && !rs.getField("_BICFI").equalsIgnoreCase("M") && !rs.getField("_BICFI").equalsIgnoreCase("I") && !rs.getField("_BICFI").contains("#I") && !rs.getField("_BICFI").contains("#S")) {
		FinInstnId.setBICFI(rs.getField("_BICFI"));
	}else if(rs.getField("_BICFI").equalsIgnoreCase("M")) {
		FinInstnId.setBICFI("");
	}else if(rs.getField("_BICFI").isEmpty()) {
		
	}else if(rs.getField("_BICFI").contains("#I")){
		FinInstnId.setBICFIX(rs.getField("_BICFI").substring(2,rs.getField("_BICFI").length()));
	}else if(rs.getField("_BICFI").contains("#S")){
		FinInstnId.setBICFIX(rs.getField("_BICFI").substring(2,rs.getField("_BICFI").length()));
		FinInstnId.setBICFI(rs.getField("_BICFI").substring(2,rs.getField("_BICFI").length()));
	}
}

public static void getPurp(Recordset rs, Purp Purp) throws FilloException {
	
	if (!rs.getField("Prtry_value").isEmpty() && !rs.getField("Prtry_value").equalsIgnoreCase("M") && !rs.getField("Prtry_value").equalsIgnoreCase("I") && !rs.getField("Prtry_value").contains("#I") && !rs.getField("Prtry_value").contains("#S")) {
		Purp.setPrtry(rs.getField("Prtry_value"));
	}else if(rs.getField("Prtry_value").equalsIgnoreCase("M")) {
		Purp.setPrtry("");
	}else if(rs.getField("Prtry_value").isEmpty()) {
		
	}else if(rs.getField("Prtry_value").contains("#I")){
		Purp.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
	}else if(rs.getField("Prtry_value").contains("#S")){
		Purp.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
		Purp.setPrtry(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
	}
}

public static void getInstrForDbtrAgt(Recordset rs, CdtTrfTxInf CdtTrfTxInf) throws FilloException {
	if (!rs.getField("InstrForDbtrAgt").isEmpty() && !rs.getField("InstrForDbtrAgt").equalsIgnoreCase("M") && !rs.getField("InstrForDbtrAgt").equalsIgnoreCase("I") && !rs.getField("InstrForDbtrAgt").contains("#I") && !rs.getField("InstrForDbtrAgt").contains("#S")) {
		CdtTrfTxInf.setInstrForDbtrAgt(rs.getField("InstrForDbtrAgt"));
	}else if(rs.getField("InstrForDbtrAgt").equalsIgnoreCase("M")) {
		CdtTrfTxInf.setInstrForDbtrAgt("");
	}else if(rs.getField("InstrForDbtrAgt").isEmpty()) {
		
	}else if(rs.getField("InstrForDbtrAgt").contains("#I")){
		CdtTrfTxInf.setInstrForDbtrAgtX(rs.getField("InstrForDbtrAgt").substring(2,rs.getField("InstrForDbtrAgt").length()));
	}else if(rs.getField("InstrForDbtrAgt").contains("#S")){
		CdtTrfTxInf.setInstrForDbtrAgtX(rs.getField("InstrForDbtrAgt").substring(2,rs.getField("InstrForDbtrAgt").length()));
		CdtTrfTxInf.setInstrForDbtrAgt(rs.getField("InstrForDbtrAgt").substring(2,rs.getField("InstrForDbtrAgt").length()));
	}
 }
}
