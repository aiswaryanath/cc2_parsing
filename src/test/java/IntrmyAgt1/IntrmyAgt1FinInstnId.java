package IntrmyAgt1;

public class IntrmyAgt1FinInstnId {
	private String BICFI;
	private String BICFIX;

	public String getBICFIX() {
		return BICFIX;
	}

	public void setBICFIX(String bICFIX) {
		BICFIX = bICFIX;
	}

	public String getBICFI() {
		return BICFI;
	}

	public void setBICFI(String bICFI) {
		BICFI = bICFI;
	}
	
}
