/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : IntrmyAgtGson */
/* */
/* File Name : IntrmyAgtGson.java */
/* */
/* Description : set value for IntrmyAgtGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import IntrmyAgt1.IntrmyAgt1FinInstnId;

public class IntrmyAgtGson {
	
	public static void getIntrmyAgt1(Recordset rs, IntrmyAgt1FinInstnId IntrmyAgt1FinInstnId) throws FilloException {
		if (!rs.getField("BICFI_val").isEmpty() && !rs.getField("BICFI_val").equalsIgnoreCase("M") && !rs.getField("BICFI_val").equalsIgnoreCase("I") && !rs.getField("BICFI_val").contains("#I") && !rs.getField("BICFI_val").contains("#S")) {
			IntrmyAgt1FinInstnId.setBICFI(rs.getField("BICFI_val"));
		}else if(rs.getField("BICFI_val").equalsIgnoreCase("M")) {
			IntrmyAgt1FinInstnId.setBICFI("");
		}else if(rs.getField("BICFI_val").isEmpty()) {
			
		}else if(rs.getField("BICFI_val").contains("#I")){
			IntrmyAgt1FinInstnId.setBICFIX(rs.getField("BICFI_val").substring(2,rs.getField("BICFI_val").length()));
		}else if(rs.getField("BICFI_val").contains("#S")){
			IntrmyAgt1FinInstnId.setBICFIX(rs.getField("BICFI_val").substring(2,rs.getField("BICFI_val").length()));
			IntrmyAgt1FinInstnId.setBICFI(rs.getField("BICFI_val").substring(2,rs.getField("BICFI_val").length()));
		}
	}
}
