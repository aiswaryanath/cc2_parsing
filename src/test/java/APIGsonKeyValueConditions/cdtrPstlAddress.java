/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : cdtrPstlAddress */
/* */
/* File Name : cdtrPstlAddress.java */
/* */
/* Description : set value for cdtrPstlAddress */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

public class cdtrPstlAddress {
	public static void getCdtrPstlAddress(Recordset rs5, ArrayList CdtrPstlAdr_Address) throws FilloException {
		if (!rs5.getField("AdrLine1_value1").isEmpty()) {
			CdtrPstlAdr_Address.add(rs5.getField("AdrLine1_value1"));
		}
		
		if (!rs5.getField("AdrLine1_value2").isEmpty()) {
			CdtrPstlAdr_Address.add(rs5.getField("AdrLine1_value2"));
		}
		
		if (!rs5.getField("AdrLine1_value3").isEmpty()) {
			CdtrPstlAdr_Address.add(rs5.getField("AdrLine1_value3"));
		}
	}
}
