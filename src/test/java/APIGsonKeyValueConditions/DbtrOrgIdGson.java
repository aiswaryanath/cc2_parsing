/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrOrgIdGson */
/* */
/* File Name : DbtrOrgIdGson.java */
/* */
/* Description : set value for DbtrOrgIdGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Dbtr.DbtrOthr;

public class DbtrOrgIdGson {
	
	public static void getDbtrOrgId(Recordset rs, DbtrOthr DbtrOthr) throws FilloException {
//		if (!rs3.getField("Id_value2").isEmpty()) {
//			DbtrOthr.setId(rs3.getField("Id_value2"));
//		}
		if (!rs.getField("Id_value2").isEmpty() && !rs.getField("Id_value2").equalsIgnoreCase("M") && !rs.getField("Id_value2").equalsIgnoreCase("I") && !rs.getField("Id_value2").contains("#I") && !rs.getField("Id_value2").contains("#S")) {
			DbtrOthr.setId(rs.getField("Id_value2"));
		}else if(rs.getField("Id_value2").equalsIgnoreCase("M")) {
			DbtrOthr.setId("");
		}else if(rs.getField("Id_value2").isEmpty()) {
			
		}else if(rs.getField("Id_value2").contains("#I")){
			DbtrOthr.setIdX(rs.getField("Id_value2").substring(2,rs.getField("Id_value2").length()));
		}else if(rs.getField("Id_value2").contains("#S")){
			DbtrOthr.setIdX(rs.getField("Id_value2").substring(2,rs.getField("Id_value2").length()));
			DbtrOthr.setId(rs.getField("Id_value2").substring(2,rs.getField("Id_value2").length()));
		}
		
	}

}
