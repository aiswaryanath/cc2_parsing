/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : SplmntryCntsGson */
/* */
/* File Name : SplmntryCntsGson.java */
/* */
/* Description : set value for SplmntryCntsGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import SplmtryData.Cnts;
import SplmtryData.Envlp;

public class SplmntryCntsGson {
/*	public static void getSplmtryData_Cnts(Recordset rs, Envlp Envlp) throws FilloException {
		
		if (!rs.getField("SplmtryData_Cnts").isEmpty() && !rs.getField("SplmtryData_Cnts").equalsIgnoreCase("M") && !rs.getField("SplmtryData_Cnts").equalsIgnoreCase("I") && !rs.getField("SplmtryData_Cnts").contains("#I") && !rs.getField("SplmtryData_Cnts").contains("#S")) {
			Envlp.setCnts(rs.getField("SplmtryData_Cnts"));
		}else if(rs.getField("SplmtryData_Cnts").equalsIgnoreCase("M")) {
			Envlp.setCnts("");
		}else if(rs.getField("SplmtryData_Cnts").isEmpty()) {
			
		}else if(rs.getField("SplmtryData_Cnts").contains("#I")){
			Envlp.setCntsX(rs.getField("SplmtryData_Cnts").substring(2,rs.getField("SplmtryData_Cnts").length()));
		}else if(rs.getField("SplmtryData_Cnts").contains("#S")){
			Envlp.setCnts(rs.getField("SplmtryData_Cnts").substring(2,rs.getField("SplmtryData_Cnts").length()));
			Envlp.setCntsX(rs.getField("SplmtryData_Cnts").substring(2,rs.getField("SplmtryData_Cnts").length()));
		}
	}*/

	public static void getStlMth(Recordset rs, Cnts Cnts) throws FilloException {
		
		if (!rs.getField("StlmMth").isEmpty() && !rs.getField("StlmMth").equalsIgnoreCase("M") && !rs.getField("StlmMth").equalsIgnoreCase("I") && !rs.getField("StlmMth").contains("#I") && !rs.getField("StlmMth").contains("#S")) {
			Cnts.setStlmMth(rs.getField("StlmMth"));
		}else if(rs.getField("StlmMth").equalsIgnoreCase("M")) {
			Cnts.setStlmMth("");
		}else if(rs.getField("StlmMth").isEmpty()) {
			
		}else if(rs.getField("StlmMth").contains("#I")){
			Cnts.setStlmMthX(rs.getField("StlmMth").substring(2,rs.getField("StlmMth").length()));
		}else if(rs.getField("StlmMth").contains("#S")){
			Cnts.setStlmMth(rs.getField("StlmMth").substring(2,rs.getField("StlmMth").length()));
			Cnts.setStlmMthX(rs.getField("StlmMth").substring(2,rs.getField("StlmMth").length()));
		}
		
	}	

	public static void getPymtAmt(Recordset rs, Cnts Cnts) throws FilloException {
		if (!rs.getField("PymtAmt").isEmpty() && !rs.getField("PymtAmt").equalsIgnoreCase("M") && !rs.getField("PymtAmt").equalsIgnoreCase("I") && !rs.getField("PymtAmt").contains("#I") && !rs.getField("PymtAmt").contains("#S")) {
			Cnts.setPymtAmt(rs.getField("PymtAmt"));
		}else if(rs.getField("PymtAmt").equalsIgnoreCase("M")) {
			Cnts.setPymtAmt("");
		}else if(rs.getField("PymtAmt").isEmpty()) {
			
		}else if(rs.getField("PymtAmt").contains("#I")){
			Cnts.setPymtAmtX(rs.getField("PymtAmt").substring(2,rs.getField("PymtAmt").length()));
		}else if(rs.getField("PymtAmt").contains("#S")){
			Cnts.setPymtAmt(rs.getField("PymtAmt").substring(2,rs.getField("PymtAmt").length()));
			Cnts.setPymtAmtX(rs.getField("PymtAmt").substring(2,rs.getField("PymtAmt").length()));
		}
	}
	
	public static void getCcy(Recordset rs, Cnts Cnts) throws FilloException {
		if (!rs.getField("Ccy").isEmpty() && !rs.getField("Ccy").equalsIgnoreCase("M") && !rs.getField("Ccy").equalsIgnoreCase("I") && !rs.getField("Ccy").contains("#I") && !rs.getField("Ccy").contains("#S")) {
			Cnts.setCcy(rs.getField("Ccy"));
		}else if(rs.getField("Ccy").equalsIgnoreCase("M")) {
			Cnts.setCcy("");
		}else if(rs.getField("Ccy").isEmpty()) {
			
		}else if(rs.getField("Ccy").contains("#I")){
			Cnts.setCcyX(rs.getField("Ccy").substring(2,rs.getField("Ccy").length()));
		}else if(rs.getField("Ccy").contains("#S")){
			Cnts.setCcy(rs.getField("Ccy").substring(2,rs.getField("Ccy").length()));
			Cnts.setCcyX(rs.getField("Ccy").substring(2,rs.getField("Ccy").length()));
		}
	}
	
	
	public static void getPANID(Recordset rs, Cnts Cnts) throws FilloException {
		if (!rs.getField("PANID").isEmpty() && !rs.getField("PANID").equalsIgnoreCase("M") && !rs.getField("PANID").equalsIgnoreCase("I") && !rs.getField("PANID").contains("#I") && !rs.getField("PANID").contains("#S")) {
			Cnts.setPANID(rs.getField("PANID"));
		}else if(rs.getField("PANID").equalsIgnoreCase("M")) {
			Cnts.setPANID("");
		}else if(rs.getField("PANID").isEmpty()) {
			
		}else if(rs.getField("PANID").contains("#I")){
			Cnts.setPANIDX(rs.getField("PANID").substring(2,rs.getField("PANID").length()));
		}else if(rs.getField("PANID").contains("#S")){
			Cnts.setPANID(rs.getField("PANID").substring(2,rs.getField("PANID").length()));
			Cnts.setPANIDX(rs.getField("PANID").substring(2,rs.getField("PANID").length()));
		}
	}
	
	public static void getFXId(Recordset rs, Cnts Cnts) throws FilloException {

		if (!rs.getField("FXId").isEmpty() && !rs.getField("FXId").equalsIgnoreCase("M") && !rs.getField("FXId").equalsIgnoreCase("I") && !rs.getField("FXId").contains("#I") && !rs.getField("FXId").contains("#S")) {
			Cnts.setFXId(rs.getField("FXId"));
		}else if(rs.getField("FXId").equalsIgnoreCase("M")) {
			Cnts.setFXId("");
		}else if(rs.getField("FXId").isEmpty()) {
			
		}else if(rs.getField("FXId").contains("#I")){
			Cnts.setFXIdX(rs.getField("FXId").substring(2,rs.getField("FXId").length()));
		}else if(rs.getField("FXId").contains("#S")){
			Cnts.setFXId(rs.getField("FXId").substring(2,rs.getField("FXId").length()));
			Cnts.setFXIdX(rs.getField("FXId").substring(2,rs.getField("FXId").length()));
		}
	}
	
	public static void getDbAmt(Recordset rs, Cnts Cnts) throws FilloException {
		if (!rs.getField("DbAmt").isEmpty() && !rs.getField("DbAmt").equalsIgnoreCase("M") && !rs.getField("DbAmt").equalsIgnoreCase("I") && !rs.getField("DbAmt").contains("#I") && !rs.getField("DbAmt").contains("#S")) {
			Cnts.setDbAmt(rs.getField("DbAmt"));
		}else if(rs.getField("DbAmt").equalsIgnoreCase("M")) {
			Cnts.setDbAmt("");
		}else if(rs.getField("DbAmt").isEmpty()) {
			
		}else if(rs.getField("DbAmt").contains("#I")){
			Cnts.setDbAmtX(rs.getField("DbAmt").substring(2,rs.getField("DbAmt").length()));
		}else if(rs.getField("DbAmt").contains("#S")){
			Cnts.setDbAmt(rs.getField("DbAmt").substring(2,rs.getField("DbAmt").length()));
			Cnts.setDbAmtX(rs.getField("DbAmt").substring(2,rs.getField("DbAmt").length()));
		}
	}
}
