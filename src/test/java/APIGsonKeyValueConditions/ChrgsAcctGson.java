/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ChrgsAcctGson */
/* */
/* File Name : ChrgsAcctGson.java */
/* */
/* Description : set value for ChrgsAcctGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

public class ChrgsAcctGson {

	public static void getChrgsAcctCcy(Recordset rs, ChrgsAcct.ChrgsAcct ChrgsAcct) throws FilloException {
		if (!rs.getField("ChargesAccount_ccy").isEmpty() && !rs.getField("ChargesAccount_ccy").equalsIgnoreCase("M") && !rs.getField("ChargesAccount_ccy").equalsIgnoreCase("I") && !rs.getField("ChargesAccount_ccy").contains("#I") && !rs.getField("ChargesAccount_ccy").contains("#S")) {
			ChrgsAcct.setCcy(rs.getField("ChargesAccount_ccy"));
		}else if(rs.getField("ChargesAccount_ccy").equalsIgnoreCase("M")) {
			ChrgsAcct.setCcy("");
		}else if(rs.getField("ChargesAccount_ccy").isEmpty()) {
			
		}else if(rs.getField("ChargesAccount_ccy").contains("#I")){
			ChrgsAcct.setCcyX(rs.getField("ChargesAccount_ccy").substring(2,rs.getField("ChargesAccount_ccy").length()));
		}else if(rs.getField("ChargesAccount_ccy").contains("#S")){
			ChrgsAcct.setCcyX(rs.getField("ChargesAccount_ccy").substring(2,rs.getField("ChargesAccount_ccy").length()));
			ChrgsAcct.setCcy(rs.getField("ChargesAccount_ccy").substring(2,rs.getField("ChargesAccount_ccy").length()));
		}

	}
	
}
