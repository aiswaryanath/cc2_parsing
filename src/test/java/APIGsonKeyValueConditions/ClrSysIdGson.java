/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ClrSysIdGson */
/* */
/* File Name : ClrSysIdGson.java */
/* */
/* Description : set value for ClrSysIdGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import CdtrAgt.ClrSysId;

public class ClrSysIdGson {
	public static void getClrSysId(Recordset rs, ClrSysId ClrSysId) throws FilloException {
		
	if (!rs.getField("Cd_ClrSysId_value").isEmpty()) {
		if (!rs.getField("Cd_ClrSysId_value").isEmpty() && !rs.getField("Cd_ClrSysId_value").equalsIgnoreCase("M") && !rs.getField("Cd_ClrSysId_value").equalsIgnoreCase("I") && !rs.getField("Cd_ClrSysId_value").contains("#I") && !rs.getField("Cd_ClrSysId_value").contains("#S")) {
			ClrSysId.setCd(rs.getField("Cd_ClrSysId_value"));
		}else if(rs.getField("Cd_ClrSysId_value").equalsIgnoreCase("M")) {
			ClrSysId.setCd("");
		}else if(rs.getField("Cd_ClrSysId_value").isEmpty()) {
			
		}else if(rs.getField("Cd_ClrSysId_value").contains("#I")){
			ClrSysId.setCdX(rs.getField("Cd_ClrSysId_value").substring(2,rs.getField("Cd_ClrSysId_value").length()));
		}else if(rs.getField("Cd_ClrSysId_value").contains("#S")){
			ClrSysId.setCdX(rs.getField("Cd_ClrSysId_value").substring(2,rs.getField("Cd_ClrSysId_value").length()));
			ClrSysId.setCd(rs.getField("Cd_ClrSysId_value").substring(2,rs.getField("Cd_ClrSysId_value").length()));
		}
	 }
	
	if (!rs.getField("Prtry_ClrSysId_value").isEmpty()) {
		if (!rs.getField("Prtry_ClrSysId_value").isEmpty() && !rs.getField("Prtry_ClrSysId_value").equalsIgnoreCase("M") && !rs.getField("Prtry_ClrSysId_value").equalsIgnoreCase("I") && !rs.getField("Prtry_ClrSysId_value").contains("#I") && !rs.getField("Prtry_ClrSysId_value").contains("#S")) {
			ClrSysId.setPrtry(rs.getField("Prtry_ClrSysId_value"));
		}else if(rs.getField("Prtry_ClrSysId_value").equalsIgnoreCase("M")) {
			ClrSysId.setPrtry("");
		}else if(rs.getField("Prtry_ClrSysId_value").isEmpty()) {
			
		}else if(rs.getField("Prtry_ClrSysId_value").contains("#I")){
			ClrSysId.setPrtryX(rs.getField("Prtry_ClrSysId_value").substring(2,rs.getField("Prtry_ClrSysId_value").length()));
		}else if(rs.getField("Prtry_ClrSysId_value").contains("#S")){
			ClrSysId.setPrtryX(rs.getField("Prtry_ClrSysId_value").substring(2,rs.getField("Prtry_ClrSysId_value").length()));
			ClrSysId.setPrtry(rs.getField("Prtry_ClrSysId_value").substring(2,rs.getField("Prtry_ClrSysId_value").length()));
		}
	 }
	}	
}
