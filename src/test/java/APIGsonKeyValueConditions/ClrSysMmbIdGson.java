/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ClrSysMmbIdGson */
/* */
/* File Name : ClrSysMmbIdGson.java */
/* */
/* Description : set value for ClrSysMmbIdGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import CdtrAgt.ClrSysMmbId;

public class ClrSysMmbIdGson {
	public static void getClrSysMmbId(Recordset rs, ClrSysMmbId ClrSysMmbId) throws FilloException {
		if (!rs.getField("MmbId_value").isEmpty() && !rs.getField("MmbId_value").equalsIgnoreCase("M") && !rs.getField("MmbId_value").equalsIgnoreCase("I") && !rs.getField("MmbId_value").contains("#I") && !rs.getField("MmbId_value").contains("#S")) {
			ClrSysMmbId.setMmbId(rs.getField("MmbId_value"));
		}else if(rs.getField("MmbId_value").equalsIgnoreCase("M")) {
			ClrSysMmbId.setMmbId("");
		}else if(rs.getField("MmbId_value").isEmpty()) {
			
		}else if(rs.getField("MmbId_value").contains("#I")){
			ClrSysMmbId.setMmbIdX(rs.getField("MmbId_value").substring(2,rs.getField("MmbId_value").length()));
		}else if(rs.getField("MmbId_value").contains("#S")){
			ClrSysMmbId.setMmbIdX(rs.getField("MmbId_value").substring(2,rs.getField("MmbId_value").length()));
			ClrSysMmbId.setMmbId(rs.getField("MmbId_value").substring(2,rs.getField("MmbId_value").length()));
		}
		
	}
}
