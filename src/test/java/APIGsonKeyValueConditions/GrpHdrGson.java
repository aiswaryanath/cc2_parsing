/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : GrpHdrGson */
/* */
/* File Name : GrpHdrGson.java */
/* */
/* Description : set value for GrpHdrGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import JSONStructure_PAIN001.GrpHdr;

public class GrpHdrGson {
	public static void GetgrpHdrsetNbOfTxs(Recordset rs, GrpHdr GrpHdr) throws FilloException {
		if (!rs.getField("NbOfTxs_value").isEmpty() && !rs.getField("NbOfTxs_value").equalsIgnoreCase("M") && !rs.getField("NbOfTxs_value").equalsIgnoreCase("I") && !rs.getField("NbOfTxs_value").contains("#I") && !rs.getField("NbOfTxs_value").contains("#S")) {
			GrpHdr.setNbOfTxs(rs.getField("NbOfTxs_value"));
		}else if(rs.getField("NbOfTxs_value").equalsIgnoreCase("M")) {
			GrpHdr.setNbOfTxs("");
		}else if(rs.getField("NbOfTxs_value").isEmpty()) {
			
		}else if(rs.getField("NbOfTxs_value").contains("#I")){
			GrpHdr.setNbOfTxsX(rs.getField("NbOfTxs_value").substring(2,rs.getField("NbOfTxs_value").length()));
		}else if(rs.getField("NbOfTxs_value").contains("#S")){
			GrpHdr.setNbOfTxsX(rs.getField("NbOfTxs_value").substring(2,rs.getField("NbOfTxs_value").length()));
			GrpHdr.setNbOfTxs(rs.getField("NbOfTxs_value").substring(2,rs.getField("NbOfTxs_value").length()));
		}
	}

	public static void GetsetCtrlSum(Recordset rs, GrpHdr GrpHdr) throws FilloException {
		if (!rs.getField("CtrlSum_value").isEmpty() && !rs.getField("CtrlSum_value").equalsIgnoreCase("M") && !rs.getField("CtrlSum_value").equalsIgnoreCase("I") && !rs.getField("CtrlSum_value").contains("#I") && !rs.getField("CtrlSum_value").contains("#S")) {
			GrpHdr.setCtrlSum(rs.getField("CtrlSum_value"));
		}else if(rs.getField("CtrlSum_value").equalsIgnoreCase("M")) {
			GrpHdr.setCtrlSum("");
		}else if(rs.getField("CtrlSum_value").isEmpty()) {
			
		}else if(rs.getField("CtrlSum_value").contains("#I")){
			GrpHdr.setCtrlSumX(rs.getField("CtrlSum_value").substring(2,rs.getField("CtrlSum_value").length()));
		}else if(rs.getField("CtrlSum_value").contains("#S")){
			GrpHdr.setCtrlSumX(rs.getField("CtrlSum_value").substring(2,rs.getField("CtrlSum_value").length()));
			GrpHdr.setCtrlSum(rs.getField("CtrlSum_value").substring(2,rs.getField("CtrlSum_value").length()));
		}
	}

	public static void getGrpHdrMsgId(Recordset rs, GrpHdr GrpHdr) throws FilloException {
		if (!rs.getField("MsgId_value").isEmpty() && !rs.getField("MsgId_value").equalsIgnoreCase("M") && !rs.getField("MsgId_value").equalsIgnoreCase("I") && !rs.getField("MsgId_value").contains("#I") && !rs.getField("MsgId_value").contains("#S")) {
			GrpHdr.setMsgId(rs.getField("MsgId_value"));
		}else if(rs.getField("MsgId_value").equalsIgnoreCase("M")) {
			GrpHdr.setMsgId("");
		}else if(rs.getField("MsgId_value").isEmpty()) {
			
		}else if(rs.getField("MsgId_value").contains("#I")){
			GrpHdr.setMsgIdX(rs.getField("MsgId_value").substring(2,rs.getField("MsgId_value").length()));
		}else if(rs.getField("MsgId_value").contains("#S")){
			GrpHdr.setMsgIdX(rs.getField("MsgId_value").substring(2,rs.getField("MsgId_value").length()));
			GrpHdr.setMsgId(rs.getField("MsgId_value").substring(2,rs.getField("MsgId_value").length()));
		}
		
		if (!rs.getField("CreDtTm_value").isEmpty() && !rs.getField("CreDtTm_value").equalsIgnoreCase("M") && !rs.getField("CreDtTm_value").equalsIgnoreCase("I") && !rs.getField("CreDtTm_value").contains("#I") && !rs.getField("CreDtTm_value").contains("#S")) {
			GrpHdr.setCreDtTm(rs.getField("CreDtTm_value"));
		}else if(rs.getField("CreDtTm_value").equalsIgnoreCase("M")) {
			GrpHdr.setCreDtTm("");
		}else if(rs.getField("CreDtTm_value").isEmpty()) {
			
		}else if(rs.getField("CreDtTm_value").contains("#I")){
			GrpHdr.setCreDtTmX(rs.getField("CreDtTm_value").substring(2,rs.getField("CreDtTm_value").length()));
		}else if(rs.getField("CreDtTm_value").contains("#S")){
			GrpHdr.setCreDtTmX(rs.getField("CreDtTm_value").substring(2,rs.getField("CreDtTm_value").length()));
			GrpHdr.setCreDtTm(rs.getField("CreDtTm_value").substring(2,rs.getField("CreDtTm_value").length()));
		}

		
	}
}
