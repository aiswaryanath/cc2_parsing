/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : XchgRateInfGson */
/* */
/* File Name : XchgRateInfGson.java */
/* */
/* Description : set value for XchgRateInfGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import XchgRateInf.XchgRateInf;

public class XchgRateInfGson {
	
	public static void getXchgRateInf(Recordset rs, XchgRateInf XchgRateInf) throws FilloException {
//		if (rs.getField("XchgRate_value").equals("M")) {
//			XchgRateInf.setXchgRate(rs.getField("XchgRate_value"));
//		}
		
		if (!rs.getField("XchgRate_value").isEmpty() && !rs.getField("XchgRate_value").equalsIgnoreCase("M") && !rs.getField("XchgRate_value").equalsIgnoreCase("I") && !rs.getField("XchgRate_value").contains("#I") && !rs.getField("XchgRate_value").contains("#S")) {
			XchgRateInf.setXchgRate(rs.getField("XchgRate_value"));
		}else if(rs.getField("XchgRate_value").equalsIgnoreCase("M")) {
			XchgRateInf.setXchgRate("");
		}else if(rs.getField("XchgRate_value").isEmpty()) {
			
		}else if(rs.getField("XchgRate_value").contains("#I")){
			XchgRateInf.setXchgRateX(rs.getField("XchgRate_value").substring(2,rs.getField("XchgRate_value").length()));
		}else if(rs.getField("XchgRate_value").contains("#S")){
			XchgRateInf.setXchgRateX(rs.getField("XchgRate_value").substring(2,rs.getField("XchgRate_value").length()));
			XchgRateInf.setXchgRate(rs.getField("XchgRate_value").substring(2,rs.getField("XchgRate_value").length()));
		}
		
		
		if (!rs.getField("RateTp_value").isEmpty() && !rs.getField("RateTp_value").equalsIgnoreCase("M") && !rs.getField("RateTp_value").equalsIgnoreCase("I") && !rs.getField("RateTp_value").contains("#I") && !rs.getField("RateTp_value").contains("#S")) {
			XchgRateInf.setRateTp(rs.getField("RateTp_value"));
		}else if(rs.getField("RateTp_value").equalsIgnoreCase("M")) {
			XchgRateInf.setRateTp("");
		}else if(rs.getField("RateTp_value").isEmpty()) {
			
		}else if(rs.getField("RateTp_value").contains("#I")){
			XchgRateInf.setRateTpX(rs.getField("RateTp_value").substring(2,rs.getField("RateTp_value").length()));
		}else if(rs.getField("RateTp_value").contains("#S")){
			XchgRateInf.setRateTpX(rs.getField("RateTp_value").substring(2,rs.getField("RateTp_value").length()));
			XchgRateInf.setRateTp(rs.getField("RateTp_value").substring(2,rs.getField("RateTp_value").length()));
		}
		
		if (!rs.getField("CtrctId_value").isEmpty() && !rs.getField("CtrctId_value").equalsIgnoreCase("M") && !rs.getField("CtrctId_value").equalsIgnoreCase("I") && !rs.getField("CtrctId_value").contains("#I") && !rs.getField("CtrctId_value").contains("#S")) {
			XchgRateInf.setCtrctId(rs.getField("CtrctId_value"));
		}else if(rs.getField("CtrctId_value").equalsIgnoreCase("M")) {
			XchgRateInf.setCtrctId("");
		}else if(rs.getField("CtrctId_value").isEmpty()) {
			
		}else if(rs.getField("CtrctId_value").contains("#I")){
			XchgRateInf.setCtrctIdX(rs.getField("CtrctId_value").substring(2,rs.getField("CtrctId_value").length()));
		}else if(rs.getField("CtrctId_value").contains("#S")){
			XchgRateInf.setCtrctIdX(rs.getField("CtrctId_value").substring(2,rs.getField("CtrctId_value").length()));
			XchgRateInf.setCtrctId(rs.getField("CtrctId_value").substring(2,rs.getField("CtrctId_value").length()));
		}
		
	}
}
