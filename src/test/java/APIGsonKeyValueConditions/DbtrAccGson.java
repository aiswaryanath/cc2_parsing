/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrAccGson */
/* */
/* File Name : DbtrAccGson.java */
/* */
/* Description : set value for DbtrAccGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import DbtrAcct.DbtrAcct;
import DbtrAcct.DbtrAcctOthr;

public class DbtrAccGson {
	
	public static void getDbtrAcct(Recordset rs, DbtrAcctOthr DbtrAcctOthr) throws FilloException {
		
		if (!rs.getField("Id_2value").isEmpty() && !rs.getField("Id_2value").equalsIgnoreCase("M") && !rs.getField("Id_2value").equalsIgnoreCase("I") && !rs.getField("Id_2value").contains("#I") && !rs.getField("Id_2value").contains("#S")) {
			DbtrAcctOthr.setId(rs.getField("Id_2value"));
		}else if(rs.getField("Id_2value").equalsIgnoreCase("M")) {
			DbtrAcctOthr.setId("");
		}else if(rs.getField("Id_2value").isEmpty()) {
			
		}else if(rs.getField("Id_2value").contains("#I")){
			DbtrAcctOthr.setIdX(rs.getField("Id_2value").substring(2,rs.getField("Id_2value").length()));
		}else if(rs.getField("Id_2value").contains("#S")){
			DbtrAcctOthr.setIdX(rs.getField("Id_2value").substring(2,rs.getField("Id_2value").length()));
			DbtrAcctOthr.setId(rs.getField("Id_2value").substring(2,rs.getField("Id_2value").length()));
		}
		
	}
	
	public static void getDbtrAcctNm(Recordset rs, DbtrAcct DbtrAcct) throws FilloException {
		
		if (!rs.getField("Dbtr_Name").isEmpty() && !rs.getField("Dbtr_Name").equalsIgnoreCase("M") && !rs.getField("Dbtr_Name").equalsIgnoreCase("I") && !rs.getField("Dbtr_Name").contains("#I") && !rs.getField("Dbtr_Name").contains("#S")) {
			DbtrAcct.setNm(rs.getField("Dbtr_Name"));
		}else if(rs.getField("Dbtr_Name").equalsIgnoreCase("M")) {
			DbtrAcct.setNm("");
		}else if(rs.getField("Dbtr_Name").isEmpty()) {
			
		}else if(rs.getField("Dbtr_Name").contains("#I")){
			DbtrAcct.setNmX(rs.getField("Dbtr_Name").substring(2,rs.getField("Dbtr_Name").length()));
		}else if(rs.getField("Dbtr_Name").contains("#S")){
			DbtrAcct.setNmX(rs.getField("Dbtr_Name").substring(2,rs.getField("Dbtr_Name").length()));
			DbtrAcct.setNm(rs.getField("Dbtr_Name").substring(2,rs.getField("Dbtr_Name").length()));
		}
		
		if (!rs.getField("Dbtr_Currency").isEmpty() && !rs.getField("Dbtr_Currency").equalsIgnoreCase("M") && !rs.getField("Dbtr_Currency").equalsIgnoreCase("I") && !rs.getField("Dbtr_Currency").contains("#I") && !rs.getField("Dbtr_Currency").contains("#S")) {
			DbtrAcct.setCcy(rs.getField("Dbtr_Currency"));
		}else if(rs.getField("Dbtr_Currency").equalsIgnoreCase("M")) {
			DbtrAcct.setCcy("");
		}else if(rs.getField("Dbtr_Currency").isEmpty()) {
			
		}else if(rs.getField("Dbtr_Currency").contains("#I")){
			DbtrAcct.setCcyX(rs.getField("Dbtr_Currency").substring(2,rs.getField("Dbtr_Currency").length()));
		}else if(rs.getField("Dbtr_Currency").contains("#S")){
			DbtrAcct.setCcyX(rs.getField("Dbtr_Currency").substring(2,rs.getField("Dbtr_Currency").length()));
			DbtrAcct.setCcy(rs.getField("Dbtr_Currency").substring(2,rs.getField("Dbtr_Currency").length()));
		}
		
	}

	
	

	
}
