/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : InitgPtyGson */
/* */
/* File Name : InitgPtyGson.java */
/* */
/* Description : set value for InitgPtyGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import InitgPty.InitgPty;
import InitgPty.Othr;
import JSONStructure_PAIN001.GrpHdr;

public class InitgPtyGson {

	public static void InitgPtyOthrGson(Recordset rs, Othr Othr) throws FilloException {
		if (!rs.getField("Id_value").isEmpty() && !rs.getField("Id_value").equalsIgnoreCase("M") && !rs.getField("Id_value").equalsIgnoreCase("I") && !rs.getField("Id_value").contains("#I") && !rs.getField("Id_value").contains("#S")) {
			Othr.setId(rs.getField("Id_value"));
		}else if(rs.getField("Id_value").equalsIgnoreCase("M")) {
			Othr.setId("");
		}else if(rs.getField("Id_value").isEmpty()) {
			
		}else if(rs.getField("Id_value").contains("#I")){
			Othr.setIdX(rs.getField("Id_value").substring(2,rs.getField("Id_value").length()));
		}else if(rs.getField("Id_value").contains("#S")){
			Othr.setIdX(rs.getField("Id_value").substring(2,rs.getField("Id_value").length()));
			Othr.setId(rs.getField("Id_value").substring(2,rs.getField("Id_value").length()));
		}
	}
	
	public static void InitgPtyIdGson(Recordset rs, InitgPty InitgPty) throws FilloException {
		if (!rs.getField("Nm_value").isEmpty() && !rs.getField("Nm_value").equalsIgnoreCase("M") && !rs.getField("Nm_value").equalsIgnoreCase("I") && !rs.getField("Nm_value").contains("#I") && !rs.getField("Nm_value").contains("#S")) {
			InitgPty.setNm(rs.getField("Nm_value"));
		}else if(rs.getField("Nm_value").equalsIgnoreCase("M")) {
			InitgPty.setNm("");
		}else if(rs.getField("Nm_value").isEmpty()) {
			
		}else if(rs.getField("Nm_value").contains("#I")){
			InitgPty.setNmX(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
		}else if(rs.getField("Nm_value").contains("#S")){
			InitgPty.setNmX(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
			InitgPty.setNm(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
		}
	}
	
	
}
