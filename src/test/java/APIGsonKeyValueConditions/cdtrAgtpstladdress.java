/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : cdtrAgtpstladdress */
/* */
/* File Name : cdtrAgtpstladdress.java */
/* */
/* Description : set value for cdtrAgtpstladdress */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import CdtrAgt.CdtrAgtPstlAdr;

public class cdtrAgtpstladdress {
	
	public static void getCdtrAgtPstlAdrAddress(Recordset rs5, ArrayList AddressCdtrAgt) throws FilloException {
		if (!rs5.getField("AdrLine_1").isEmpty()) {
			AddressCdtrAgt.add(rs5.getField("AdrLine_1"));
		}
		if (!rs5.getField("AdrLine_2").isEmpty()) {
			AddressCdtrAgt.add(rs5.getField("AdrLine_2"));
		}
		if (!rs5.getField("AdrLine_3").isEmpty()) {
			AddressCdtrAgt.add(rs5.getField("AdrLine_3"));
		}
		
	}
	
	public static void getCtrAgtPstAdr(Recordset rs, CdtrAgtPstlAdr CdtrAgtPstlAdr) throws FilloException {
		
		if (!rs.getField("Ctry_value").isEmpty() && !rs.getField("Ctry_value").equalsIgnoreCase("M") && !rs.getField("Ctry_value").equalsIgnoreCase("I") && !rs.getField("Ctry_value").contains("#I") && !rs.getField("Ctry_value").contains("#S")) {
			CdtrAgtPstlAdr.setCtry(rs.getField("Ctry_value"));
		}else if(rs.getField("Ctry_value").equalsIgnoreCase("M")) {
			CdtrAgtPstlAdr.setCtry("");
		}else if(rs.getField("Ctry_value").isEmpty()) {
			
		}else if(rs.getField("Ctry_value").contains("#I")){
			CdtrAgtPstlAdr.setCtryX(rs.getField("Ctry_value").substring(2,rs.getField("Ctry_value").length()));
		}else if(rs.getField("Ctry_value").contains("#S")){
			CdtrAgtPstlAdr.setCtryX(rs.getField("Ctry_value").substring(2,rs.getField("Ctry_value").length()));
			CdtrAgtPstlAdr.setCtry(rs.getField("Ctry_value").substring(2,rs.getField("Ctry_value").length()));
		}
		
		if (!rs.getField("StrtNm_value").isEmpty()) {
			CdtrAgtPstlAdr.setStrtNm(rs.getField("StrtNm_value"));
		}
		
		if (!rs.getField("BldgNb_value").isEmpty()) {
			CdtrAgtPstlAdr.setBldgNb(rs.getField("BldgNb_value"));
		}
		
		if (!rs.getField("PstCd_value").isEmpty()) {
			CdtrAgtPstlAdr.setPstCd(rs.getField("PstCd_value"));
		}
		
		if (!rs.getField("TwnNm_value").isEmpty()) {
			CdtrAgtPstlAdr.setTwnNm(rs.getField("TwnNm_value"));
		}
		
		if (!rs.getField("CtrySubDvsn_value").isEmpty()) {
			CdtrAgtPstlAdr.setCtrySubDvsn(rs.getField("CtrySubDvsn_value"));
		}
	}
}
