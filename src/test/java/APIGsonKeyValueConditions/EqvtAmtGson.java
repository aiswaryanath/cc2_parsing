/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : EqvtAmtGson */
/* */
/* File Name : EqvtAmtGson.java */
/* */
/* Description : set value for EqvtAmtGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Amt.EqvtAmt;

public class EqvtAmtGson {

	public static void getEqvtAmt(Recordset rs, EqvtAmt EqvtAmt) throws FilloException {
		
		if (!rs.getField("Eqt_Amt").isEmpty() && !rs.getField("Eqt_Amt").equalsIgnoreCase("M") && !rs.getField("Eqt_Amt").equalsIgnoreCase("I") && !rs.getField("Eqt_Amt").contains("#I") && !rs.getField("Eqt_Amt").contains("#S")) {
			EqvtAmt.setAmt(rs.getField("Eqt_Amt"));
		}else if(rs.getField("Eqt_Amt").equalsIgnoreCase("M")) {
			EqvtAmt.setAmt("");
		}else if(rs.getField("Eqt_Amt").isEmpty()) {
			
		}else if(rs.getField("Eqt_Amt").contains("#I")){
			EqvtAmt.setAmtX(rs.getField("Eqt_Amt").substring(2,rs.getField("Eqt_Amt").length()));
		}else if(rs.getField("Eqt_Amt").contains("#S")){
			EqvtAmt.setAmtX(rs.getField("Eqt_Amt").substring(2,rs.getField("Eqt_Amt").length()));
			EqvtAmt.setAmt(rs.getField("Eqt_Amt").substring(2,rs.getField("Eqt_Amt").length()));
		}
		
		
		if (!rs.getField("Eqt_AmtCcy").isEmpty() && !rs.getField("Eqt_AmtCcy").equalsIgnoreCase("M") && !rs.getField("Eqt_AmtCcy").equalsIgnoreCase("I") && !rs.getField("Eqt_AmtCcy").contains("#I") && !rs.getField("Eqt_AmtCcy").contains("#S")) {
			EqvtAmt.setCcy(rs.getField("Eqt_AmtCcy"));
		}else if(rs.getField("Eqt_AmtCcy").equalsIgnoreCase("M")) {
			EqvtAmt.setCcy("");
		}else if(rs.getField("Eqt_AmtCcy").isEmpty()) {
			
		}else if(rs.getField("Eqt_AmtCcy").contains("#I")){
			EqvtAmt.setCcyX(rs.getField("Eqt_AmtCcy").substring(2,rs.getField("Eqt_AmtCcy").length()));
		}else if(rs.getField("Eqt_AmtCcy").contains("#S")){
			EqvtAmt.setCcyX(rs.getField("Eqt_AmtCcy").substring(2,rs.getField("Eqt_AmtCcy").length()));
			EqvtAmt.setCcy(rs.getField("Eqt_AmtCcy").substring(2,rs.getField("Eqt_AmtCcy").length()));
		}
		
		
		
		if (!rs.getField("Eqt_AmtCcyTransfer").isEmpty() && !rs.getField("Eqt_AmtCcyTransfer").equalsIgnoreCase("M") && !rs.getField("Eqt_AmtCcyTransfer").equalsIgnoreCase("I") && !rs.getField("Eqt_AmtCcyTransfer").contains("#I") && !rs.getField("Eqt_AmtCcyTransfer").contains("#S")) {
			EqvtAmt.setCcyOfTrf(rs.getField("Eqt_AmtCcyTransfer"));
		}else if(rs.getField("Eqt_AmtCcyTransfer").equalsIgnoreCase("M")) {
			EqvtAmt.setCcyOfTrf("");
		}else if(rs.getField("Eqt_AmtCcyTransfer").isEmpty()) {
			
		}else if(rs.getField("Eqt_AmtCcyTransfer").contains("#I")){
			EqvtAmt.setCcyOfTrfX(rs.getField("Eqt_AmtCcyTransfer").substring(2,rs.getField("Eqt_AmtCcyTransfer").length()));
		}else if(rs.getField("Eqt_AmtCcyTransfer").contains("#S")){
			EqvtAmt.setCcyOfTrfX(rs.getField("Eqt_AmtCcyTransfer").substring(2,rs.getField("Eqt_AmtCcyTransfer").length()));
			EqvtAmt.setCcyOfTrf(rs.getField("Eqt_AmtCcyTransfer").substring(2,rs.getField("Eqt_AmtCcyTransfer").length()));
		}
	}
}
