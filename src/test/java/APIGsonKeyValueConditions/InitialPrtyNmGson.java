/***************************************************************************************************/
/* Copyright © 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : InitialPrtyNmGson */
/* */
/* File Name : InitialPrtyNmGson.java */
/* */
/* Description : set value for InitialPrtyNmGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Dbtr.Dbtr;

public class InitialPrtyNmGson {
	public static void getInitgPtyNameGson(Recordset rs, Dbtr Dbtr) throws FilloException {
//		if (rs3.getField("Nm_value").equals("R")) {
//			Dbtr.setNm(rs3.getField("Nm_value"));
//		}
		if (!rs.getField("Nm_value").isEmpty() && !rs.getField("Nm_value").equalsIgnoreCase("M") && !rs.getField("Nm_value").equalsIgnoreCase("I") && !rs.getField("Nm_value").contains("#I") && !rs.getField("Nm_value").contains("#S")) {
			Dbtr.setNm(rs.getField("Nm_value"));
		}else if(rs.getField("Nm_value").equalsIgnoreCase("M")) {
			Dbtr.setNm("");
		}else if(rs.getField("Nm_value").isEmpty()) {
			
		}else if(rs.getField("Nm_value").contains("#I")){
			Dbtr.setNmX(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
		}else if(rs.getField("Nm_value").contains("#S")){
			Dbtr.setNmX(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
			Dbtr.setNm(rs.getField("Nm_value").substring(2,rs.getField("Nm_value").length()));
		}

	}

}
