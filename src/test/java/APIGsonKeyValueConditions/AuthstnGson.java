/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : AuthstnGson */
/* */
/* File Name : AuthstnGson.java */
/* */
/* Description : set value for AuthstnGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import JSONStructure_PAIN001.Authstn;
import JSONStructure_PAIN001.GrpHdr;

public class AuthstnGson {
	
	public static void GetAuthstn(Recordset rs, Authstn Authstn) throws FilloException {
		if (!rs.getField("Prtry_value").isEmpty() && !rs.getField("Prtry_value").equalsIgnoreCase("M") && !rs.getField("Prtry_value").equalsIgnoreCase("I") && !rs.getField("Prtry_value").contains("#I") && !rs.getField("Prtry_value").contains("#S")) {
			Authstn.setPrtry(rs.getField("Prtry_value"));
		}else if(rs.getField("Prtry_value").equalsIgnoreCase("M")) {
			Authstn.setPrtry("");
		}else if(rs.getField("Prtry_value").isEmpty()) {
			
		}else if(rs.getField("Prtry_value").contains("#I")){
			Authstn.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
		}else if(rs.getField("Prtry_value").contains("#S")){
			Authstn.setPrtryX(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
			Authstn.setPrtry(rs.getField("Prtry_value").substring(2,rs.getField("Prtry_value").length()));
		}
	}

	
}
