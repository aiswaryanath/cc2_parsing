/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : InstdAmtGson */
/* */
/* File Name : InstdAmtGson.java */
/* */
/* Description : set value for InstdAmtGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Amt.InstdAmt;

public class InstdAmtGson {
	
	public static void getInstdAmt(Recordset rs, InstdAmt InstdAmt) throws FilloException {
		
		if (!rs.getField("Amt_value").isEmpty() && !rs.getField("Amt_value").equalsIgnoreCase("M") && !rs.getField("Amt_value").equalsIgnoreCase("I") && !rs.getField("Amt_value").contains("#I") && !rs.getField("Amt_value").contains("#S")) {
			InstdAmt.setAmt(rs.getField("Amt_value"));
		}else if(rs.getField("Amt_value").equalsIgnoreCase("M")) {
			InstdAmt.setAmt("");
		}else if(rs.getField("Amt_value").isEmpty()) {
			
		}else if(rs.getField("Amt_value").contains("#I")){
			InstdAmt.setAmtX(rs.getField("Amt_value").substring(2,rs.getField("Amt_value").length()));
		}else if(rs.getField("Amt_value").contains("#S")){
			InstdAmt.setAmtX(rs.getField("Amt_value").substring(2,rs.getField("Amt_value").length()));
			InstdAmt.setAmt(rs.getField("Amt_value").substring(2,rs.getField("Amt_value").length()));
		}
		
		if (!rs.getField("Ccy_value").isEmpty() && !rs.getField("Ccy_value").equalsIgnoreCase("M") && !rs.getField("Ccy_value").equalsIgnoreCase("I") && !rs.getField("Ccy_value").contains("#I") && !rs.getField("Ccy_value").contains("#S")) {
			InstdAmt.setCcy(rs.getField("Ccy_value"));
		}else if(rs.getField("Ccy_value").equalsIgnoreCase("M")) {
			InstdAmt.setCcy("");
		}else if(rs.getField("Ccy_value").isEmpty()) {
			
		}else if(rs.getField("Ccy_value").contains("#I")){
			InstdAmt.setCcyX(rs.getField("Ccy_value").substring(2,rs.getField("Ccy_value").length()));
		}else if(rs.getField("Ccy_value").contains("#S")){
			InstdAmt.setCcyX(rs.getField("Ccy_value").substring(2,rs.getField("Ccy_value").length()));
			InstdAmt.setCcy(rs.getField("Ccy_value").substring(2,rs.getField("Ccy_value").length()));
		}

		
		
	}
}


