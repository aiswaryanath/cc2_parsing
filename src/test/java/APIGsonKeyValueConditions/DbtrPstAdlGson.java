/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrPstAdlGson */
/* */
/* File Name : DbtrPstAdlGson.java */
/* */
/* Description : set value for DbtrPstAdlGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Dbtr.PstlAdr;

public class DbtrPstAdlGson {
	public static void getDbtrPstlAdr(Recordset rs, PstlAdr PstlAdr) throws FilloException {
		if (!rs.getField("StrtNm_value").isEmpty()) {
			PstlAdr.setStrtNm(rs.getField("StrtNm_value"));
		}
		
		if (!rs.getField("BldgNb_value").isEmpty()) {
			PstlAdr.setBldgNb(rs.getField("BldgNb_value"));
		}
		
		if (!rs.getField("PstCd_value").isEmpty()) {
			PstlAdr.setPstCd(rs.getField("PstCd_value"));
		}
		
		if (!rs.getField("TwnNm_value").isEmpty()) {
			PstlAdr.setTwnNm(rs.getField("TwnNm_value"));
		}
		
		if (!rs.getField("CtrySubDvsn_value").isEmpty()) {
			PstlAdr.setCtrySubDvsn(rs.getField("CtrySubDvsn_value"));
		}
		
//		if (!rs.getField("Ctry_value").isEmpty()) {
//			PstlAdr.setCtry(rs.getField("Ctry_value"));
//		}
		
		if (!rs.getField("Ctry_value").isEmpty() && !rs.getField("Ctry_value").equalsIgnoreCase("M") && !rs.getField("Ctry_value").equalsIgnoreCase("I") && !rs.getField("Ctry_value").contains("#I") && !rs.getField("Ctry_value").contains("#S")) {
			PstlAdr.setCtry(rs.getField("Ctry_value"));
		}else if(rs.getField("Ctry_value").equalsIgnoreCase("M")) {
			PstlAdr.setCtry("");
		}else if(rs.getField("Ctry_value").isEmpty()) {
			
		}else if(rs.getField("Ctry_value").contains("#I")){
			PstlAdr.setCtryX(rs.getField("Ctry_value").substring(2,rs.getField("Ctry_value").length()));
		}else if(rs.getField("Ctry_value").contains("#S")){
			PstlAdr.setCtryX(rs.getField("Ctry_value").substring(2,rs.getField("Ctry_value").length()));
			PstlAdr.setCtry(rs.getField("Ctry_value").substring(2,rs.getField("Ctry_value").length()));
		}

		
	}
	
}
