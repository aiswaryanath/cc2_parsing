/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : CdtrGson */
/* */
/* File Name : CdtrGson.java */
/* */
/* Description : set value for CdtrGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/



package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import Cdtr.Cdtr;
import Cdtr.CdtrPstlAdr;

public class CdtrGson {
	public static void getCdtrPstlAdr(Recordset rs, CdtrPstlAdr CdtrPstlAdr) throws FilloException {
		if (!rs.getField("Ctry_value1").isEmpty() && !rs.getField("Ctry_value1").equalsIgnoreCase("M") && !rs.getField("Ctry_value1").equalsIgnoreCase("I") && !rs.getField("Ctry_value1").contains("#I") && !rs.getField("Ctry_value1").contains("#S")) {
			CdtrPstlAdr.setCtry(rs.getField("Ctry_value1"));
		}else if(rs.getField("Ctry_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setCtry("");
		}else if(rs.getField("Ctry_value1").isEmpty()) {
			
		}else if(rs.getField("Ctry_value1").contains("#I")){
			CdtrPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
		}else if(rs.getField("Ctry_value1").contains("#S")){
			CdtrPstlAdr.setCtryX(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
			CdtrPstlAdr.setCtry(rs.getField("Ctry_value1").substring(2,rs.getField("Ctry_value1").length()));
		}
		
		
		if (!rs.getField("StrtNm_value1").isEmpty() && !rs.getField("StrtNm_value1").equalsIgnoreCase("M") && !rs.getField("StrtNm_value1").equalsIgnoreCase("I") && !rs.getField("StrtNm_value1").contains("#I") && !rs.getField("StrtNm_value1").contains("#S")) {
			CdtrPstlAdr.setStrtNm(rs.getField("StrtNm_value1"));
		}else if(rs.getField("StrtNm_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setStrtNm("");
		}else if(rs.getField("StrtNm_value1").isEmpty()) {
			
		}else if(rs.getField("StrtNm_value1").contains("#I")){
			CdtrPstlAdr.setStrtNmX(rs.getField("StrtNm_value1").substring(2,rs.getField("StrtNm_value1").length()));
		}else if(rs.getField("StrtNm_value1").contains("#S")){
			CdtrPstlAdr.setStrtNmX(rs.getField("StrtNm_value1").substring(2,rs.getField("StrtNm_value1").length()));
			CdtrPstlAdr.setStrtNm(rs.getField("StrtNm_value1").substring(2,rs.getField("StrtNm_value1").length()));
		}
		
		
		if (!rs.getField("BldgNb_value1").isEmpty() && !rs.getField("BldgNb_value1").equalsIgnoreCase("M") && !rs.getField("BldgNb_value1").equalsIgnoreCase("I") && !rs.getField("BldgNb_value1").contains("#I") && !rs.getField("BldgNb_value1").contains("#S")) {
			CdtrPstlAdr.setBldgNb(rs.getField("BldgNb_value1"));
		}else if(rs.getField("BldgNb_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setBldgNb("");
		}else if(rs.getField("BldgNb_value1").isEmpty()) {
			
		}else if(rs.getField("BldgNb_value1").contains("#I")){
			CdtrPstlAdr.setBldgNb(rs.getField("BldgNb_value1").substring(2,rs.getField("BldgNb_value1").length()));
		}else if(rs.getField("BldgNb_value1").contains("#S")){
			CdtrPstlAdr.setBldgNb(rs.getField("BldgNb_value1").substring(2,rs.getField("BldgNb_value1").length()));
			CdtrPstlAdr.setBldgNb(rs.getField("BldgNb_value1").substring(2,rs.getField("BldgNb_value1").length()));
		}
		
		if (!rs.getField("PstCd_value1").isEmpty() && !rs.getField("PstCd_value1").equalsIgnoreCase("M") && !rs.getField("PstCd_value1").equalsIgnoreCase("I") && !rs.getField("PstCd_value1").contains("#I") && !rs.getField("PstCd_value1").contains("#S")) {
			CdtrPstlAdr.setPstCd(rs.getField("PstCd_value1"));
		}else if(rs.getField("PstCd_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setPstCd("");
		}else if(rs.getField("PstCd_value1").isEmpty()) {
			
		}else if(rs.getField("PstCd_value1").contains("#I")){
			CdtrPstlAdr.setPstCd(rs.getField("PstCd_value1").substring(2,rs.getField("PstCd_value1").length()));
		}else if(rs.getField("PstCd_value1").contains("#S")){
			CdtrPstlAdr.setPstCd(rs.getField("PstCd_value1").substring(2,rs.getField("PstCd_value1").length()));
			CdtrPstlAdr.setPstCd(rs.getField("PstCd_value1").substring(2,rs.getField("PstCd_value1").length()));
		}
		
		if (!rs.getField("TwnNm_value1").isEmpty() && !rs.getField("TwnNm_value1").equalsIgnoreCase("M") && !rs.getField("TwnNm_value1").equalsIgnoreCase("I") && !rs.getField("TwnNm_value1").contains("#I") && !rs.getField("TwnNm_value1").contains("#S")) {
			CdtrPstlAdr.setTwnNm(rs.getField("TwnNm_value1"));
		}else if(rs.getField("TwnNm_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setTwnNm("");
		}else if(rs.getField("TwnNm_value1").isEmpty()) {
			
		}else if(rs.getField("TwnNm_value1").contains("#I")){
			CdtrPstlAdr.setTwnNm(rs.getField("TwnNm_value1").substring(2,rs.getField("TwnNm_value1").length()));
		}else if(rs.getField("TwnNm_value1").contains("#S")){
			CdtrPstlAdr.setTwnNm(rs.getField("TwnNm_value1").substring(2,rs.getField("TwnNm_value1").length()));
			CdtrPstlAdr.setTwnNm(rs.getField("TwnNm_value1").substring(2,rs.getField("TwnNm_value1").length()));
		}
		if (!rs.getField("CtrySubDvsn_value1").isEmpty() && !rs.getField("CtrySubDvsn_value1").equalsIgnoreCase("M") && !rs.getField("CtrySubDvsn_value1").equalsIgnoreCase("I") && !rs.getField("CtrySubDvsn_value1").contains("#I") && !rs.getField("CtrySubDvsn_value1").contains("#S")) {
			CdtrPstlAdr.setCtrySubDvsn(rs.getField("CtrySubDvsn_value1"));
		}else if(rs.getField("CtrySubDvsn_value1").equalsIgnoreCase("M")) {
			CdtrPstlAdr.setCtrySubDvsn("");
		}else if(rs.getField("CtrySubDvsn_value1").isEmpty()) {
			
		}else if(rs.getField("CtrySubDvsn_value1").contains("#I")){
			CdtrPstlAdr.setCtrySubDvsn(rs.getField("CtrySubDvsn_value1").substring(2,rs.getField("CtrySubDvsn_value1").length()));
		}else if(rs.getField("CtrySubDvsn_value1").contains("#S")){
			CdtrPstlAdr.setCtrySubDvsn(rs.getField("CtrySubDvsn_value1").substring(2,rs.getField("CtrySubDvsn_value1").length()));
			CdtrPstlAdr.setCtrySubDvsn(rs.getField("CtrySubDvsn_value1").substring(2,rs.getField("CtrySubDvsn_value1").length()));
		}
		
	}
	
	
	public static void getCdtrNm(Recordset rs, Cdtr Cdtr) throws FilloException {
		
		if (!rs.getField("Nm_value1").isEmpty() && !rs.getField("Nm_value1").equalsIgnoreCase("M") && !rs.getField("Nm_value1").equalsIgnoreCase("I") && !rs.getField("Nm_value1").contains("#I") && !rs.getField("Nm_value1").contains("#S")) {
			Cdtr.setNm(rs.getField("Nm_value1"));
		}else if(rs.getField("Nm_value1").equalsIgnoreCase("M")) {
			Cdtr.setNm("");
		}else if(rs.getField("Nm_value1").isEmpty()) {
			
		}else if(rs.getField("Nm_value1").contains("#I")){
			Cdtr.setNmX(rs.getField("Nm_value1").substring(2,rs.getField("Nm_value1").length()));
		}else if(rs.getField("Nm_value1").contains("#S")){
			Cdtr.setNmX(rs.getField("Nm_value1").substring(2,rs.getField("Nm_value1").length()));
			Cdtr.setNm(rs.getField("Nm_value1").substring(2,rs.getField("Nm_value1").length()));
		}
		
	}
	
	
		
}
