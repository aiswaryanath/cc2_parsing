/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : RmtInfGson */
/* */
/* File Name : RmtInfGson.java */
/* */
/* Description : set value for RmtInfGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import RmtInf.RmtInf;

public class RmtInfGson {
	public static void getRmtInf(Recordset rs, RmtInf RmtInf) throws FilloException {
		if (!rs.getField("RmtInf_value").isEmpty() && !rs.getField("RmtInf_value").equalsIgnoreCase("M") && !rs.getField("RmtInf_value").equalsIgnoreCase("I") && !rs.getField("RmtInf_value").contains("#I") && !rs.getField("RmtInf_value").contains("#S")) {
			RmtInf.setUstrd(rs.getField("RmtInf_value"));
		}else if(rs.getField("RmtInf_value").equalsIgnoreCase("M")) {
			RmtInf.setUstrd("");
		}else if(rs.getField("RmtInf_value").isEmpty()) {
			
		}else if(rs.getField("RmtInf_value").contains("#I")){
			RmtInf.setUstrdX(rs.getField("RmtInf_value").substring(2,rs.getField("RmtInf_value").length()));
		}else if(rs.getField("RmtInf_value").contains("#S")){
			RmtInf.setUstrd(rs.getField("RmtInf_value").substring(2,rs.getField("RmtInf_value").length()));
			RmtInf.setUstrdX(rs.getField("RmtInf_value").substring(2,rs.getField("RmtInf_value").length()));
		}
	}
}
