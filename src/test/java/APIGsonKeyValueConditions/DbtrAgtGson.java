/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrAgtGson */
/* */
/* File Name : DbtrAgtGson.java */
/* */
/* Description : set value for DbtrAgtGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import DbtrAgt.DbtrAgtFinInstnId;

public class DbtrAgtGson {
	public static void getDbtrAgt(Recordset rs, DbtrAgtFinInstnId DbtrAgtFinInstnId) throws FilloException {
		if (!rs.getField("BICFI_value").isEmpty() && !rs.getField("BICFI_value").equalsIgnoreCase("M") && !rs.getField("BICFI_value").equalsIgnoreCase("I") && !rs.getField("BICFI_value").contains("#I") && !rs.getField("BICFI_value").contains("#S")) {
			DbtrAgtFinInstnId.setBICFI(rs.getField("BICFI_value"));
		}else if(rs.getField("BICFI_value").equalsIgnoreCase("M")) {
			DbtrAgtFinInstnId.setBICFI("");
		}else if(rs.getField("BICFI_value").isEmpty()) {
			
		}else if(rs.getField("BICFI_value").contains("#I")){
			DbtrAgtFinInstnId.setBICFIX(rs.getField("BICFI_value").substring(2,rs.getField("BICFI_value").length()));
		}else if(rs.getField("BICFI_value").contains("#S")){
			DbtrAgtFinInstnId.setBICFIX(rs.getField("BICFI_value").substring(2,rs.getField("BICFI_value").length()));
			DbtrAgtFinInstnId.setBICFI(rs.getField("BICFI_value").substring(2,rs.getField("BICFI_value").length()));
		}
		
		if (!rs.getField("Nm_3value").isEmpty() && !rs.getField("Nm_3value").equalsIgnoreCase("M") && !rs.getField("Nm_3value").equalsIgnoreCase("I") && !rs.getField("Nm_3value").contains("#I") && !rs.getField("Nm_3value").contains("#S")) {
			DbtrAgtFinInstnId.setNm(rs.getField("Nm_3value"));
		}else if(rs.getField("Nm_3value").equalsIgnoreCase("M")) {
			DbtrAgtFinInstnId.setNm("");
		}else if(rs.getField("Nm_3value").isEmpty()) {
			
		}else if(rs.getField("Nm_3value").contains("#I")){
			DbtrAgtFinInstnId.setNmX(rs.getField("Nm_3value").substring(2,rs.getField("Nm_3value").length()));
		}else if(rs.getField("Nm_3value").contains("#S")){
			DbtrAgtFinInstnId.setNmX(rs.getField("Nm_3value").substring(2,rs.getField("Nm_3value").length()));
			DbtrAgtFinInstnId.setNm(rs.getField("Nm_3value").substring(2,rs.getField("Nm_3value").length()));
		}
		
	}
}
