/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : PmTpInfSvcLvlGson */
/* */
/* File Name : PmTpInfSvcLvlGson.java */
/* */
/* Description : set value for PmTpInfSvcLvlGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import JSONStructure_PAIN001.PmtInf;
import PmtTpInf.LclInstrm;
import PmtTpInf.SvcLvl;

public class PmTpInfSvcLvlGson {
	
	public static void getSvcLvl(Recordset rs, SvcLvl SvcLvl) throws FilloException {
		
//		ReqdExctnDt_value
		if (!rs.getField("PmtTpInf_SvcLvl").isEmpty() && !rs.getField("PmtTpInf_SvcLvl").equalsIgnoreCase("M") && !rs.getField("PmtTpInf_SvcLvl").equalsIgnoreCase("I") && !rs.getField("PmtTpInf_SvcLvl").contains("#I") && !rs.getField("PmtTpInf_SvcLvl").contains("#S")) {
			SvcLvl.setCd(rs.getField("PmtTpInf_SvcLvl"));
		}else if(rs.getField("PmtTpInf_SvcLvl").equalsIgnoreCase("M")) {
			SvcLvl.setCd("");
		}else if(rs.getField("PmtTpInf_SvcLvl").isEmpty()) {
			
		}else if(rs.getField("PmtTpInf_SvcLvl").contains("#I")){
			SvcLvl.setCdX(rs.getField("PmtTpInf_SvcLvl").substring(2,rs.getField("PmtTpInf_SvcLvl").length()));
		}else if(rs.getField("PmtTpInf_SvcLvl").contains("#S")){
			SvcLvl.setCdX(rs.getField("PmtTpInf_SvcLvl").substring(2,rs.getField("PmtTpInf_SvcLvl").length()));
			SvcLvl.setCd(rs.getField("PmtTpInf_SvcLvl").substring(2,rs.getField("PmtTpInf_SvcLvl").length()));
		}
		
	}
	
//	PmtTpInf_LclInstrm	
	public static void getLclInstrm(Recordset rs, LclInstrm LclInstrm) throws FilloException {
		if (!rs.getField("PmtTpInf_LclInstrm").isEmpty() && !rs.getField("PmtTpInf_LclInstrm").equalsIgnoreCase("M") && !rs.getField("PmtTpInf_LclInstrm").equalsIgnoreCase("I") && !rs.getField("PmtTpInf_LclInstrm").contains("#I") && !rs.getField("PmtTpInf_LclInstrm").contains("#S")) {
			LclInstrm.setCd(rs.getField("PmtTpInf_LclInstrm"));
		}else if(rs.getField("PmtTpInf_LclInstrm").equalsIgnoreCase("M")) {
			LclInstrm.setCd("");
		}else if(rs.getField("PmtTpInf_LclInstrm").isEmpty()) {
			
		}else if(rs.getField("PmtTpInf_LclInstrm").contains("#I")){
			LclInstrm.setCdX(rs.getField("PmtTpInf_LclInstrm").substring(2,rs.getField("PmtTpInf_LclInstrm").length()));
		}else if(rs.getField("PmtTpInf_LclInstrm").contains("#S")){
			LclInstrm.setCdX(rs.getField("PmtTpInf_LclInstrm").substring(2,rs.getField("PmtTpInf_LclInstrm").length()));
			LclInstrm.setCd(rs.getField("PmtTpInf_LclInstrm").substring(2,rs.getField("PmtTpInf_LclInstrm").length()));
		}
	}
	
}
