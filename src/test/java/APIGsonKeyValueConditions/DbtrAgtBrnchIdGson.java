/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrAgtBrnchIdGson */
/* */
/* File Name : DbtrAgtBrnchIdGson.java */
/* */
/* Description : set value for DbtrAgtBrnchIdGson */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import DbtrAgt.DbtrAgtBrnchId;

public class DbtrAgtBrnchIdGson {
	public static void getDbtrAgtBrnchId(Recordset rs, DbtrAgtBrnchId BrnchId) throws FilloException {
		if (!rs.getField("Id_3valueF").isEmpty() && !rs.getField("Id_3valueF").equalsIgnoreCase("M") && !rs.getField("Id_3valueF").equalsIgnoreCase("I") && !rs.getField("Id_3valueF").contains("#I") && !rs.getField("Id_3valueF").contains("#S")) {
			BrnchId.setId(rs.getField("Id_3valueF"));
		}else if(rs.getField("Id_3valueF").equalsIgnoreCase("M")) {
			BrnchId.setId("");
		}else if(rs.getField("Id_3valueF").isEmpty()) {
			
		}else if(rs.getField("Id_3valueF").contains("#I")){
			BrnchId.setIdX(rs.getField("Id_3valueF").substring(2,rs.getField("Id_3valueF").length()));
		}else if(rs.getField("Id_3valueF").contains("#S")){
			BrnchId.setIdX(rs.getField("Id_3valueF").substring(2,rs.getField("Id_3valueF").length()));
			BrnchId.setId(rs.getField("Id_3valueF").substring(2,rs.getField("Id_3valueF").length()));
		}

	}

}
