/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : GetKeyChrBr */
/* */
/* File Name : GetKeyChrBr.java */
/* */
/* Description : set value for GetKeyChrBr */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import JSONStructure_PAIN001.CdtTrfTxInf;

public class GetKeyChrBr {
	public static void getChrBrGson(Recordset rs, CdtTrfTxInf CdtTrfTxInf) throws FilloException {
		if (!rs.getField("ChrgBr_value").isEmpty() && !rs.getField("ChrgBr_value").equalsIgnoreCase("M") && !rs.getField("ChrgBr_value").equalsIgnoreCase("I") && !rs.getField("ChrgBr_value").contains("#I") && !rs.getField("ChrgBr_value").contains("#S")) {
			CdtTrfTxInf.setChrgBr(rs.getField("ChrgBr_value"));
		}else if(rs.getField("ChrgBr_value").equalsIgnoreCase("M")) {
			CdtTrfTxInf.setChrgBr("");
		}else if(rs.getField("ChrgBr_value").isEmpty()) {
			
		}else if(rs.getField("ChrgBr_value").contains("#I")){
			CdtTrfTxInf.setChrgBrX(rs.getField("ChrgBr_value").substring(2,rs.getField("ChrgBr_value").length()));
		}else if(rs.getField("ChrgBr_value").contains("#S")){
			CdtTrfTxInf.setChrgBrX(rs.getField("ChrgBr_value").substring(2,rs.getField("ChrgBr_value").length()));
			CdtTrfTxInf.setChrgBr(rs.getField("ChrgBr_value").substring(2,rs.getField("ChrgBr_value").length()));
		}
	}
}
