/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : PmtInfGsonkeyValue */
/* */
/* File Name : PmtInfGsonkeyValue.java */
/* */
/* Description : set value for PmtInfGsonkeyValue */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package APIGsonKeyValueConditions;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import JSONStructure_PAIN001.PmtInf;

public class PmtInfGsonkeyValue {
	
	public static void PmtInfIdGson(Recordset rs, PmtInf PmtInf) throws FilloException {
//		System.out.println(rs.getField("PmtInfId_value"));
		if (!rs.getField("PmtInfId_value").isEmpty() && !rs.getField("PmtInfId_value").equalsIgnoreCase("M") && !rs.getField("PmtInfId_value").equalsIgnoreCase("I") && !rs.getField("PmtInfId_value").contains("#I") && !rs.getField("PmtInfId_value").contains("#S")) {
			PmtInf.setPmtInfId(rs.getField("PmtInfId_value"));
		}else if(rs.getField("PmtInfId_value").equalsIgnoreCase("M")) {
			PmtInf.setPmtInfId("");
		}else if(rs.getField("PmtInfId_value").isEmpty()) {
			
		}else if(rs.getField("PmtInfId_value").contains("#I")){
			PmtInf.setPmtInfIdX(rs.getField("PmtInfId_value").substring(2,rs.getField("PmtInfId_value").length()));
		}else if(rs.getField("PmtInfId_value").contains("#S")){
			PmtInf.setPmtInfIdX(rs.getField("PmtInfId_value").substring(2,rs.getField("PmtInfId_value").length()));
			PmtInf.setPmtInfId(rs.getField("PmtInfId_value").substring(2,rs.getField("PmtInfId_value").length()));
		}
		
		//PmtMtd
		if (!rs.getField("PmtMtd_value").isEmpty() && !rs.getField("PmtMtd_value").equalsIgnoreCase("M") && !rs.getField("PmtMtd_value").equalsIgnoreCase("I") && !rs.getField("PmtMtd_value").contains("#I") && !rs.getField("PmtMtd_value").contains("#S")) {
			PmtInf.setPmtMtd(rs.getField("PmtMtd_value"));
		}else if(rs.getField("PmtMtd_value").equalsIgnoreCase("M")) {
			PmtInf.setPmtMtd("");
		}else if(rs.getField("PmtMtd_value").isEmpty()) {
			
		}else if(rs.getField("PmtMtd_value").contains("#I")){
			PmtInf.setPmtMtdX(rs.getField("PmtMtd_value").substring(2,rs.getField("PmtMtd_value").length()));
		}else if(rs.getField("PmtMtd_value").contains("#S")){
			PmtInf.setPmtMtdX(rs.getField("PmtMtd_value").substring(2,rs.getField("PmtMtd_value").length()));
			PmtInf.setPmtMtd(rs.getField("PmtMtd_value").substring(2,rs.getField("PmtMtd_value").length()));
		}
		
		//NbOfTxs
		if (!rs.getField("NbOfTxs_value").isEmpty() && !rs.getField("NbOfTxs_value").equalsIgnoreCase("M") && !rs.getField("NbOfTxs_value").equalsIgnoreCase("I") && !rs.getField("NbOfTxs_value").contains("#I") && !rs.getField("NbOfTxs_value").contains("#S")) {
			PmtInf.setNbOfTxs(rs.getField("NbOfTxs_value"));
		}else if(rs.getField("NbOfTxs_value").equalsIgnoreCase("M")) {
			PmtInf.setNbOfTxs("");
		}else if(rs.getField("NbOfTxs_value").isEmpty()) {
			
		}else if(rs.getField("NbOfTxs_value").contains("#I")){
			PmtInf.setNbOfTxsX(rs.getField("NbOfTxs_value").substring(2,rs.getField("NbOfTxs_value").length()));
		}else if(rs.getField("NbOfTxs_value").contains("#S")){
			PmtInf.setNbOfTxsX(rs.getField("NbOfTxs_value").substring(2,rs.getField("NbOfTxs_value").length()));
			PmtInf.setNbOfTxs(rs.getField("NbOfTxs_value").substring(2,rs.getField("NbOfTxs_value").length()));
		}
		
//		CtrlSum_value
		if (!rs.getField("CtrlSum_value").isEmpty() && !rs.getField("CtrlSum_value").equalsIgnoreCase("M") && !rs.getField("CtrlSum_value").equalsIgnoreCase("I") && !rs.getField("CtrlSum_value").contains("#I") && !rs.getField("CtrlSum_value").contains("#S")) {
			PmtInf.setCtrlSum(rs.getField("CtrlSum_value"));
		}else if(rs.getField("CtrlSum_value").equalsIgnoreCase("M")) {
			PmtInf.setCtrlSum("");
		}else if(rs.getField("CtrlSum_value").isEmpty()) {
			
		}else if(rs.getField("CtrlSum_value").contains("#I")){
			PmtInf.setCtrlSumX(rs.getField("CtrlSum_value").substring(2,rs.getField("CtrlSum_value").length()));
		}else if(rs.getField("CtrlSum_value").contains("#S")){
			PmtInf.setCtrlSumX(rs.getField("CtrlSum_value").substring(2,rs.getField("CtrlSum_value").length()));
			PmtInf.setCtrlSum(rs.getField("CtrlSum_value").substring(2,rs.getField("CtrlSum_value").length()));
		}
		
//		ReqdExctnDt_value
		if (!rs.getField("ReqdExctnDt_value").isEmpty() && !rs.getField("ReqdExctnDt_value").equalsIgnoreCase("M") && !rs.getField("ReqdExctnDt_value").equalsIgnoreCase("I") && !rs.getField("ReqdExctnDt_value").contains("#I") && !rs.getField("ReqdExctnDt_value").contains("#S")) {
			PmtInf.setReqdExctnDt(rs.getField("ReqdExctnDt_value"));
		}else if(rs.getField("ReqdExctnDt_value").equalsIgnoreCase("M")) {
			PmtInf.setReqdExctnDt("");
		}else if(rs.getField("ReqdExctnDt_value").isEmpty()) {
			
		}else if(rs.getField("ReqdExctnDt_value").contains("#I")){
			PmtInf.setReqdExctnDtX(rs.getField("ReqdExctnDt_value").substring(2,rs.getField("ReqdExctnDt_value").length()));
		}else if(rs.getField("ReqdExctnDt_value").contains("#S")){
			PmtInf.setReqdExctnDtX(rs.getField("ReqdExctnDt_value").substring(2,rs.getField("ReqdExctnDt_value").length()));
			PmtInf.setReqdExctnDt(rs.getField("ReqdExctnDt_value").substring(2,rs.getField("ReqdExctnDt_value").length()));
		}
	}
	
//	public static void PmtMtdGson(Recordset rs, PmtInf PmtInf) throws FilloException {	
//	}
	
}
