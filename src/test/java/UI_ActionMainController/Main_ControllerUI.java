package UI_ActionMainController;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Utility.Data_Connection;

public class Main_ControllerUI {
	public static void main(String[] args) throws Exception{
		  Fillo fill=new Fillo();
		  Connection conn=fill.getConnection("D:/Core_ComponentTool/Test_Data/ManualActionTestData.xlsx");
		  Recordset rs = conn.executeQuery("Select * from MainController where Run_Status = 'Yes'");
			System.setProperty("webdriver.ie.driver", "D:/Core_ComponentTool/IE_Driver/IEDriverServer.exe");
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
			capability.setCapability("pageLoadStrategy", "eager");
			WebDriver driver = new InternetExplorerDriver(capability);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
			driver.get("http://10.10.8.62:10062/BPS/");
			driver.manage().window().maximize();

			while (rs.next()) {
				Login_module.Login_moduleExec(rs.getField("TestCase_ID"),driver);
	        }
	}
}
