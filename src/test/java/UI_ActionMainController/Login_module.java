package UI_ActionMainController;

import java.util.ArrayList;
import java.util.Set;
import java.util.StringTokenizer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import UIAction_Implimentations.UIOperation;

public class Login_module {
	

	public static void Login_moduleExec(String TestCaseID,WebDriver driver) throws Exception{
		  Fillo fill=new Fillo();
		  Connection conn=fill.getConnection("D:/Core_ComponentTool/Test_Data/ManualActionTestData.xlsx");
		  Recordset rs = conn.executeQuery("Select * from Login where TestCase_ID = '"+TestCaseID+"'");
			while (rs.next()) {
				UIOperation ob=new UIOperation();
				String User_Name = rs.getField("User_Name");
				String User_Name_data = rs.getField("User_Name_Value");
				String LOGIN_Btn = rs.getField("LOGIN_Btn");
				
//				ob.perform(Login_module.get_DataValue(User_Name).get(0),Login_module.get_DataValue(User_Name).get(1),Login_module.get_DataValue(User_Name).get(2),rs.getField("User_Name_data"), driver);
//				ob.perform(Login_module.get_DataValue(User_Name_data).get(0),Login_module.get_DataValue(User_Name_data).get(1),Login_module.get_DataValue(User_Name_data).get(2),rs.getField("User_Name_data"), driver);
//				ob.perform(Login_module.get_DataValue(LOGIN_Btn).get(0),Login_module.get_DataValue(LOGIN_Btn).get(1),Login_module.get_DataValue(LOGIN_Btn).get(2),"", driver);
				
				Thread.sleep(30000);
				Set<String> allHandles = driver.getWindowHandles();
				System.out.println("Count of windows:"+allHandles.size());  
				
				String currentWindowHandle = allHandles.iterator().next();
			    System.out.println("currentWindow Handle"+currentWindowHandle);
				
//			    allHandles.remove(allHandles.iterator().next());
			    
			  //get the last Window Handle
			    String lastHandle = allHandles.iterator().next();
			    System.out.println("last window handle"+lastHandle);
			    
			    driver.switchTo().window(lastHandle);
			    System.out.println(driver.getTitle());
			    
			    WebDriverWait wait = new WebDriverWait(driver, 10);
			    Thread.sleep(40000);
				WebElement link = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='CIBC Payments Hub']")));
				link.click();
			    
			    
			    driver.quit();
				
				
//		        Thread.sleep(30000);
//				WebDriverWait wait = new WebDriverWait(driver, 10);
//				
//				Set<String> Windows = driver.getWindowHandles();
//				for (String win : Windows) {
//					driver.switchTo().window(win);
//					if (!"Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN"
//							.equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle())) {
//						System.out.println(driver.getTitle());
//						break;
//					}
//				}
//
//				Thread.sleep(40000);
//				boolean buttonPresence = driver.findElement(By.xpath("//a[text()='CIBC Payments Hub']")).isDisplayed();
//				System.out.println("Link_displayed---->" + buttonPresence);
//				boolean buttonclickable = driver.findElement(By.xpath("//a[text()='CIBC Payments Hub']")).isEnabled();
//				System.out.println("Link_clickable---->" + buttonclickable);
//				
//				WebElement link = wait
//						.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='CIBC Payments Hub']")));
//				link.click();

				
		}
	}

	public static ArrayList<String> get_DataValue(String User_Name) {
		ArrayList<String> add_login= new ArrayList<String>();	
			StringTokenizer st = new StringTokenizer(User_Name);
			while(st.hasMoreTokens()){
				add_login.add(st.nextToken("#"));
			}
		return add_login;
	}
}
