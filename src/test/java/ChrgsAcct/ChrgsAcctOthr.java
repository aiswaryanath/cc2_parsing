/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ChrgsAcctOthr */
/* */
/* File Name : ChrgsAcctOthr.java */
/* */
/* Description : ChrgsAcctOthr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package ChrgsAcct;

public class ChrgsAcctOthr {
	private String Id;
	private String IdX;

	public String getIdX() {
		return IdX;
	}

	public void setIdX(String idX) {
		IdX = idX;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}
	
}
