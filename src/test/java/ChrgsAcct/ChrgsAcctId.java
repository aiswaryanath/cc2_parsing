/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ChrgsAcctId */
/* */
/* File Name : ChrgsAcctId.java */
/* */
/* Description : ChrgsAcctId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package ChrgsAcct;

public class ChrgsAcctId {
	ChrgsAcctOthr Othr;

	public ChrgsAcctOthr getOthr() {
		return Othr;
	}

	public void setOthr(ChrgsAcctOthr othr) {
		Othr = othr;
	}
	
}
