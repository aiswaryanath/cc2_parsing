/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : ChrgsAcct */
/* */
/* File Name : ChrgsAcct.java */
/* */
/* Description : ChrgsAcct POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package ChrgsAcct;
public class ChrgsAcct {
	
	private ChrgsAcctId Id;
	private String Ccy;
	private String CcyX;
	
	public String getCcyX() {
		return CcyX;
	}

	public void setCcyX(String ccyX) {
		CcyX = ccyX;
	}

	public String getCcy() {
		return Ccy;
	}

	public void setCcy(String ccy) {
		Ccy = ccy;
	}

	public ChrgsAcctId getId() {
		return Id;
	}

	public void setId(ChrgsAcctId id) {
		Id = id;
	}
	
}
