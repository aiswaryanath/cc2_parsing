package InitgPty;

public class InitgPty {
	private String Nm;
	private String NmX;
	
	private Id Id;
	private Id IdX; 

	public String getNmX() {
		return NmX;
	}

	public void setNmX(String nmX) {
		NmX = nmX;
	}

	public Id getIdX() {
		return IdX;
	}

	public void setIdX(Id idX) {
		IdX = idX;
	}

	public Id getId() {
		return Id;
	}

	public void setId(Id id) {
		Id = id;
	}

	public String getNm() {
		return Nm;
	}

	public void setNm(String nm) {
		Nm = nm;
	}
	
}
