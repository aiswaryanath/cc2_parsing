package UI_CC2_ManualActionInquiry;

import java.io.File;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Files;

import Utility.HandleWebElements;

public class CC2_Login {
	public static void Login_Execute(WebDriver driver, String credentials)throws Exception{
		driver.findElement(By.name("ArmorTicket")).clear();
		driver.findElement(By.name("ArmorTicket")).sendKeys(credentials);
		HandleWebElements.SuccessfullClickJS(driver, "//*[@src='images/indeximages/Login_button.gif']");
		
        Thread.sleep(10000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		Set<String> Windows = driver.getWindowHandles();
		for (String win : Windows) {
			driver.switchTo().window(win);
			if (!"Intellect Suite - Enterprise Platform for Boundaryless Banking from Intellect Design Arena-EN"
					.equalsIgnoreCase(driver.getTitle()) && !"BPS".equalsIgnoreCase(driver.getTitle())) {
				System.out.println("Automation Begins!!"+driver.getTitle());
				break;
			}
		}

		Thread.sleep(40000);
		HandleWebElements.SuccessfullClickFE(driver, "//a[text()='CIBC Payments Hub']");
		
//		 int count =0;
//		 while(true){
//		 WebElement ele1 = driver.findElement(By.xpath("//a[text()='CIBC Payments Hub']"));
//			 if (ele1.isDisplayed()) {
//				 System.out.println("Element found!!"+ele1.getAttribute("class"));
//				 ele1.click();
//				 break;
//			}else{
//				System.out.println(count);
//				count ++;
//			}
//		 } 
		
//		Thread.sleep(40000);
//		WebElement link = wait
//				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='CIBC Payments Hub']")));
//		link.click();
		 
		Thread.sleep(10000);
		// Switch To BPS Window
		Set<String> window = driver.getWindowHandles();
//		System.out.println("SIZE=" + window.size());
		Iterator<String> itr = window.iterator();
		while (itr.hasNext()) {
			Thread.sleep(30000);
			driver.switchTo().window(itr.next());
			if (!driver.getTitle().equals("Workspace Picklist")) {
				break;
			}
		}
		
//		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		Files.copy(src, new File("C:/Users/varun.paranganath/Desktop/TEST Data Bkp/error.png"));

	}
}
