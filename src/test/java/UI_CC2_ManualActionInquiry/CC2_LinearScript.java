package UI_CC2_ManualActionInquiry;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.codoid.products.exception.FilloException;

public class CC2_LinearScript {

	static String BPS_window;
	public static void Execute_FGT(WebDriver driver,String Credentials,String WorkitemID) throws FilloException, Exception {
		System.setProperty("webdriver.ie.driver", "D:/Core_ComponentTool/IE_Driver/IEDriverServer.exe");
		DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
		capability.setCapability("pageLoadStrategy", "eager");
		driver = new InternetExplorerDriver(capability);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		driver.get("http://10.10.8.62:10062/BPS/");
		driver.manage().window().maximize();

		CC2_Login.Login_Execute(driver, Credentials);
		if (Credentials.equalsIgnoreCase("BCCMKR1")) {
			System.out.println("Maker Script Begins");
			CC2_Inquiry.Inquiry(driver, WorkitemID);
		}
	}

	public static void main(String[] args) throws Exception {
		ArrayList<String> WorkitemID_list=new ArrayList<String>();
		WorkitemID_list.add("7883281");
//		WorkitemID_list.add("6927938");
//		WorkitemID_list.add("6927940");
		
		for (int i = 0; i < WorkitemID_list.size(); i++) {
			WebDriver driver = null;
		try{
			CC2_LinearScript.Execute_FGT(driver,"BCCMKR1",WorkitemID_list.get(i));
		}catch (Exception e) {
			e.printStackTrace();
		}

//		try{
//			CC2_LinearScript.Execute_FGT(driver,"BCCCKR1",WorkitemID_list.get(i));
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
	  }	
	}
}
