package JSONStructure_PAIN001;

import java.util.ArrayList;

import SplmtryData.SplmtryData;

public class CstmrCdtTrfInitn {
	GrpHdr GrpHdr;
//	private PmtInf[] PmtInf;
//	private SplmtryData[] SplmtryData;
	ArrayList<PmtInf> PmtInf;
	ArrayList <SplmtryData> SplmtryData;
	
	public ArrayList<PmtInf> getPmtInf1() {
		return PmtInf;
	}

	public void setPmtInf1(ArrayList<PmtInf> pmtInf1) {
		PmtInf = pmtInf1;
	}

	public ArrayList<SplmtryData> getSplmtryData() {
		return SplmtryData;
	}

	public void setSplmtryData(ArrayList<SplmtryData> splmtryData) {
		SplmtryData = splmtryData;
	}

//	public SplmtryData[] getSplmtryData() {
//		return SplmtryData;
//	}
//
//	public void setSplmtryData(SplmtryData[] splmtryData) {
//		SplmtryData = splmtryData;
//	}

//	public PmtInf[] getPmtInf() {
//		return PmtInf;
//	}
//	
//	public void setPmtInf(PmtInf[] pmtInf) {
//		PmtInf = pmtInf;
//	}

	public GrpHdr getGrpHdr() {
		return GrpHdr;
	}

	public void setGrpHdr(GrpHdr grpHdr) {
		GrpHdr = grpHdr;
	}
}
