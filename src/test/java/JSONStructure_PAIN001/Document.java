package JSONStructure_PAIN001;

public class Document {
	
	private CstmrCdtTrfInitn CstmrCdtTrfInitn;

	public CstmrCdtTrfInitn getCstmrCdtTrfInitn() {
		return CstmrCdtTrfInitn;
	}

	public void setCstmrCdtTrfInitn(CstmrCdtTrfInitn cstmrCdtTrfInitn) {
		CstmrCdtTrfInitn = cstmrCdtTrfInitn;
	}
}
