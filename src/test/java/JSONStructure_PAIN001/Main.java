package JSONStructure_PAIN001;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import Amt.Amt;
import Amt.EqvtAmt;
import Amt.InstdAmt;
import Cdtr.Cdtr;
import Cdtr.CdtrPstlAdr;
import CdtrAcct.CdtrAcct;
import CdtrAcct.CdtrAcctId;
import CdtrAcct.CdtrAcctOthr;
import CdtrAgt.CdtrAgt;
import CdtrAgt.CdtrAgtPstlAdr;
import CdtrAgt.ClrSysId;
import CdtrAgt.ClrSysMmbId;
import CdtrAgt.FinInstnId;
import ChrgsAcct.ChrgsAcctId;
import ChrgsAcct.ChrgsAcctOthr;
import Dbtr.Dbtr;
import Dbtr.DbtrId;
import Dbtr.DbtrOrgId;
import Dbtr.DbtrOthr;
import Dbtr.PstlAdr;
import DbtrAcct.DbtrAcct;
import DbtrAcct.DbtrAcctId;
import DbtrAcct.DbtrAcctOthr;
import DbtrAgt.DbtrAgt;
import DbtrAgt.DbtrAgtBrnchId;
import DbtrAgt.DbtrAgtFinInstnId;
import DbtrAgt.DbtrAgtOthr;
import DbtrAgt.DbtrAgtPstlAdr;
import InitgPty.Id;
import InitgPty.InitgPty;
import InitgPty.OrgId;
import InitgPty.Othr;
import IntrmyAgt1.IntrmyAgt1;
import IntrmyAgt1.IntrmyAgt1FinInstnId;
import PmtId.PmtId;
import PmtTpInf.LclInstrm;
import PmtTpInf.PmtTpInf;
import PmtTpInf.SvcLvl;
import Purp.Purp;
import RmtInf.RmtInf;
import SplmtryData.Envlp;
import SplmtryData.SplmtryData;
import XchgRateInf.XchgRateInf;

public class Main {
	static PrintStream json_console = null;
	public static void main(String[] args) throws FileNotFoundException, FilloException {
/*		
		GrpHdr GrpHdr =new GrpHdr();
		
		GrpHdr.setMsgId("ARVIND_000289");
		GrpHdr.setCreDtTm("2019-06-13T18:15:05");
		GrpHdr.setNbOfTxs("1");
		GrpHdr.setCtrlSum("100");
		
		Authstn Authstn=new Authstn();
		Authstn.setPrtry("AUTH DATA135");
		
		GrpHdr.setAuthstn(Authstn);
		
		//InitgPty
		Othr Othr=new Othr();
		Othr.setId("123324234");
		
		OrgId OrgId=new OrgId();
		OrgId.setOthr(Othr);
		
		Id Id=new Id();
		Id.setOrgId(OrgId);
		
		InitgPty InitgPty=new InitgPty();
		InitgPty.setNm("BILL PAY CEU Auto");
		
		InitgPty.setId(Id);
		
		GrpHdr.setInitgPty(InitgPty);
		
		CstmrCdtTrfInitn CstmrCdtTrfInitn=new CstmrCdtTrfInitn();
		CstmrCdtTrfInitn.setGrpHdr(GrpHdr);
		PmtInf PmtInf =new PmtInf();
		PmtInf.setPmtInfId("ACHRMTBATCH1");
		PmtInf.setPmtMtd("TRF");
		PmtInf.setNbOfTxs("1");
		PmtInf.setCtrlSum("100");
		PmtInf.setReqdExctnDt("2019-06-11");
		
		
//		PmtTpInf Starts here
		SvcLvl SvcLvl=new SvcLvl();
		SvcLvl.setCd("URGP");
		
		LclInstrm LclInstrm=new LclInstrm();
		LclInstrm.setCd("TRF");
		
		PmtTpInf PmtTpInf=new PmtTpInf();
		PmtTpInf.setLclInstrm(LclInstrm);
		PmtTpInf.setSvcLvl(SvcLvl);
		
		PmtInf.setPmtTpInf(PmtTpInf);
//		PmtTpInf Ends here		
		
		
//		ChrgsAcct Starts here
		ChrgsAcctOthr Othr1=new ChrgsAcctOthr();
		Othr1.setId("004120243310");
		
		ChrgsAcctId  Id1=new ChrgsAcctId();
		Id1.setOthr(Othr1);
		
		ChrgsAcct.ChrgsAcct ChrgsAcct = new ChrgsAcct.ChrgsAcct();
		ChrgsAcct.setId(Id1);		
		
		PmtInf.setChrgsAcct(ChrgsAcct);
//		ChrgsAcct Ends here
		
//		Dbtr Starts Here
		
		PstlAdr PstlAdr=new PstlAdr();
		PstlAdr.setCtry("CA");
		PstlAdr.setStrtNm("Lane No 2");
		
		DbtrOthr DbtrOthr=new DbtrOthr();
		DbtrOthr.setId("1480000002");
		
		DbtrOrgId DbtrOrgId=new DbtrOrgId();
		DbtrOrgId.setOthr(DbtrOthr);
		
		DbtrId  DbtrId=new DbtrId();
		DbtrId.setOrgId(DbtrOrgId);
		
		Dbtr Dbtr=new Dbtr();
		Dbtr.setId(DbtrId);
		Dbtr.setNm("BILL PAY CEU Auto");
		Dbtr.setPstlAdr(PstlAdr);
		
		PmtInf.setDbtr(Dbtr);
//		Dbtr Ends here
		
//		DbtrAcct Starts here
		
		DbtrAcctOthr DbtrAcctOthr=new DbtrAcctOthr();
		DbtrAcctOthr.setId("0243310");
		
		DbtrAcctId DbtrAcctId=new DbtrAcctId();
		DbtrAcctId.setOthr(DbtrAcctOthr);
		
		DbtrAcct DbtrAcct=new DbtrAcct();
		DbtrAcct.setId(DbtrAcctId);
		
		PmtInf.setDbtrAcct(DbtrAcct);
//		DbtrAcct Ends here
		
//		DbtrAgt Starts here
		DbtrAgtBrnchId BrnchId=new DbtrAgtBrnchId();
		BrnchId.setId("00412");
		
		DbtrAgtPstlAdr DbtrAgtPstlAdr=new DbtrAgtPstlAdr();
		DbtrAgtPstlAdr.setCtry("CA");
		
		DbtrAgtOthr DbtrAgtOthr=new DbtrAgtOthr();
		DbtrAgtOthr.setId("0010");
		
		DbtrAgtFinInstnId DbtrAgtFinInstnId=new DbtrAgtFinInstnId();
		DbtrAgtFinInstnId.setBICFI("CIBCCATAXXX");
		DbtrAgtFinInstnId.setNm("CIBC");
		DbtrAgtFinInstnId.setPstlAdr(DbtrAgtPstlAdr);
		DbtrAgtFinInstnId.setOthr(DbtrAgtOthr);
		
		DbtrAgt DbtrAgt=new DbtrAgt();
		DbtrAgt.setBrnchId(BrnchId);
		DbtrAgt.setFinInstnId(DbtrAgtFinInstnId);
		
		PmtInf.setDbtrAgt(DbtrAgt);
	
				
		System.setProperty("ROW", "2");
		Fillo fill=new Fillo();
		Connection conn=fill.getConnection("D:\\Core_ComponentTool\\Test_Data\\Test_dataPain001_1.xlsx");
		Recordset rs1=conn.executeQuery("Select * from SplmtryData where Splmtry_ID = 'BVTID13'");
		ArrayList<SplmtryData> al =  new ArrayList<SplmtryData>();
		while(rs1.next()){
			SplmtryData SplmtryData1 =null;
//			SplmtryData[] SplmtryData2 = new SplmtryData[2]; 
				Envlp Envlp1=new Envlp();
				Envlp1.setCnts(rs1.getField("Cnts_Value"));
				SplmtryData1=new SplmtryData();
				SplmtryData1.setEnvlp(Envlp1);
				al.add(SplmtryData1);
		PmtInf PmtInf1[]={PmtInf};
		CstmrCdtTrfInitn.setPmtInf(PmtInf1);
		//CstmrCdtTrfInitn.setSplmtryData(SplmtryData2);
		CstmrCdtTrfInitn.setSplmtryData(al);
		}

//Ends PmtInf here		
		
//		CdtTrfTxInf starts here
		CdtTrfTxInf CdtTrfTxInf=new CdtTrfTxInf();
		
		//* ChrgBr
		CdtTrfTxInf.setChrgBr("DEBT");
		
		//* InstrForDbtrAgt
		CdtTrfTxInf.setInstrForDbtrAgt("EN");
		
		//* PmtId
		PmtId PmtId=new PmtId();
		PmtId.setInstrId("ACHREMIT");
		PmtId.setEndToEndId("ACHREMIT");
		
		CdtTrfTxInf.setPmtId(PmtId);
		
		//Cdtr
		CdtrPstlAdr CdtrPstlAdr=new CdtrPstlAdr();
		CdtrPstlAdr.setCtry("CA");
		CdtrPstlAdr.setStrtNm("Lane No 2");
		
		Cdtr Cdtr=new Cdtr();
		Cdtr.setNm("SAM PVT LTD");
		Cdtr.setPstlAdr(CdtrPstlAdr);
		
		CdtTrfTxInf.setCdtr(Cdtr);
		
		//Amt
		EqvtAmt EqvtAmt=new EqvtAmt();
		EqvtAmt.setAmt("100");
		EqvtAmt.setCcyOfTrf("USD");
		
		InstdAmt InstdAmt=new InstdAmt();
		InstdAmt.setAmt("100");
		InstdAmt.setCcy("USD");
		
		Amt Amt = new Amt();
		Amt.setEqvtAmt(EqvtAmt);
		Amt.setInstdAmt(InstdAmt);
		
		CdtTrfTxInf.setAmt(Amt);
		
		//Amt ends here
		
//		XchgRateInf starts here
		
		XchgRateInf XchgRateInf=new XchgRateInf();
		XchgRateInf.setXchgRate("12931");
		XchgRateInf.setRateTp("AGRD");
		XchgRateInf.setCtrctId("213887431");
		
		CdtTrfTxInf.setXchgRateInf(XchgRateInf);
		
//		XchgRateInf Ends here		
		IntrmyAgt1FinInstnId IntrmyAgt1FinInstnId =new IntrmyAgt1FinInstnId();
		IntrmyAgt1FinInstnId.setBICFI("BUKBGB22");
		
		IntrmyAgt1 IntrmyAgt1=new IntrmyAgt1();
		IntrmyAgt1.setFinInstnId(IntrmyAgt1FinInstnId);
		
		CdtTrfTxInf.setIntrmyAgt1(IntrmyAgt1);
		
//		CdtrAgt Starts here
		
		ClrSysId ClrSysId=new ClrSysId();
		ClrSysId.setCd("USABA");
		
		ClrSysMmbId ClrSysMmbId=new ClrSysMmbId();
		ClrSysMmbId.setMmbId("011000015");
		ClrSysMmbId.setClrSysId(ClrSysId);
		
		FinInstnId FinInstnId=new FinInstnId();
		FinInstnId.setClrSysMmbId(ClrSysMmbId);
		FinInstnId.setNm("FEDERAL RESERVE BANK");
		
		CdtrAgtPstlAdr CdtrAgtPstlAdr=new CdtrAgtPstlAdr();
		CdtrAgtPstlAdr.setCtry("US");
		
		FinInstnId.setPstlAdr(CdtrAgtPstlAdr);
		
		CdtrAgt CdtrAgt=new CdtrAgt();
		CdtrAgt.setFinInstnId(FinInstnId);
		
		CdtTrfTxInf.setCdtrAgt(CdtrAgt);
		
		//CdtrAcct
		CdtrAcctOthr CdtrAcctOthr=new CdtrAcctOthr();
		CdtrAcctOthr.setId("123456789");
		CdtrAcctId  CdtrAcctId=new CdtrAcctId();
		CdtrAcctId.setOthr(CdtrAcctOthr);
		
		CdtrAcct CdtrAcct=new CdtrAcct();
		CdtrAcct.setId(CdtrAcctId);
		
		CdtTrfTxInf.setCdtrAcct(CdtrAcct);
		
//		CdtrAgt Ends here		
		
		
//     RmtInf Starts here
		RmtInf RmtInf=new RmtInf();
		RmtInf.setUstrd("Remitance Info Line01 DataXXXXXXX01Remitance Info Line02 DataXXXXXXX02Remitance Info Line03 DataXXXXXXX03Remitance Info Line04 DataXXXXXXX04");
		CdtTrfTxInf.setRmtInf(RmtInf);
//	     RmtInf Ends here

//		SplmtryData Starts here
		Envlp Envlp=new Envlp();
		Envlp.setCnts("StlmMth : A");
		
		SplmtryData SplmtryData=new SplmtryData();
		SplmtryData.setEnvlp(Envlp);
		
		CdtTrfTxInf.setSplmtryData(SplmtryData);
//		SplmtryData Ends here
		
//		Purp
		Purp Purp=new Purp();
		Purp.setPrtry("SAL");
		
		CdtTrfTxInf.setPurp(Purp);
		
		CdtTrfTxInf CdtTrfTxInf1[]={CdtTrfTxInf};
		PmtInf.setCdtTrfTxInf(CdtTrfTxInf1);

//CdtTrfTxInf Ends here here
		
		Document Document=new Document();
		Document.setCstmrCdtTrfInitn(CstmrCdtTrfInitn);
		
		RootClass RootClass=new RootClass();
		RootClass.setDocument(Document);
		
		Gson Gson =new GsonBuilder().setPrettyPrinting().create();
		JsonElement str=Gson.toJsonTree(RootClass);
//		String str2= Gson.toJson(str);
		String prettyJsonString = Gson.toJson(str);

		json_console = new PrintStream(new File("D:\\Core_ComponentTool\\_Output\\GSON_Json.json"));
		System.setOut(json_console);
		
		System.out.println(prettyJsonString);
*/	
	}
}
