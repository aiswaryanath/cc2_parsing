package JSONStructure_PAIN001;

import InitgPty.InitgPty;

public class GrpHdr {
	
	private String MsgId;
	private String MsgIdX;
	
	private String CreDtTm;
	private String CreDtTmX;
	
	public String getMsgIdX() {
		return MsgIdX;
	}
	public void setMsgIdX(String msgIdX) {
		MsgIdX = msgIdX;
	}
	public String getCreDtTmX() {
		return CreDtTmX;
	}
	public void setCreDtTmX(String creDtTmX) {
		CreDtTmX = creDtTmX;
	}
	private String NbOfTxs;
	private String NbOfTxsX;
	
	private String CtrlSum;
	private String CtrlSumX;
	
	public String getNbOfTxsX() {
		return NbOfTxsX;
	}
	public void setNbOfTxsX(String nbOfTxsX) {
		NbOfTxsX = nbOfTxsX;
	}
	public String getCtrlSumX() {
		return CtrlSumX;
	}
	public void setCtrlSumX(String ctrlSumX) {
		CtrlSumX = ctrlSumX;
	}
	private Authstn Authstn;
	
	private InitgPty InitgPty;
	
	public InitgPty getInitgPty() {
		return InitgPty;
	}
	public void setInitgPty(InitgPty initgPty) {
		InitgPty = initgPty;
	}
	public String getMsgId() {
		return MsgId;
	}
	public void setMsgId(String msgId) {
		MsgId = msgId;
	}
	public String getNbOfTxs() {
		return NbOfTxs;
	}
	public void setNbOfTxs(String nbOfTxs) {
		NbOfTxs = nbOfTxs;
	}
	
	public Authstn getAuthstn() {
		return Authstn;
	}
	public void setAuthstn(Authstn authstn) {
		Authstn = authstn;
	}
	public String getCreDtTm() {
		return CreDtTm;
	}
	public void setCreDtTm(String creDtTm) {
		CreDtTm = creDtTm;
	}
	public String getCtrlSum() {
		return CtrlSum;
	}
	public void setCtrlSum(String ctrlSum) {
		CtrlSum = ctrlSum;
	}
	
}
