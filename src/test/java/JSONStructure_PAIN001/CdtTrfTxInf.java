package JSONStructure_PAIN001;

import Amt.Amt;
import Cdtr.Cdtr;
import CdtrAcct.CdtrAcct;
import CdtrAgt.CdtrAgt;
import IntrmyAgt1.IntrmyAgt1;
import PmtId.PmtId;
import Purp.Purp;
import RmtInf.RmtInf;
import SplmtryData.SplmtryData;
import XchgRateInf.XchgRateInf;

public class CdtTrfTxInf {
	private PmtId PmtId;
	private Amt Amt;
	private XchgRateInf XchgRateInf;
	private String ChrgBr;
	private String ChrgBrX;
	private IntrmyAgt1 IntrmyAgt1;
	private CdtrAgt CdtrAgt;
	private Cdtr Cdtr;
	private CdtrAcct CdtrAcct;
	private RmtInf RmtInf;
	private SplmtryData SplmtryData;
	private Purp Purp;
	private String InstrForDbtrAgt;
	private String InstrForDbtrAgtX;
	
	
	
	public String getInstrForDbtrAgtX() {
		return InstrForDbtrAgtX;
	}
	public void setInstrForDbtrAgtX(String instrForDbtrAgtX) {
		InstrForDbtrAgtX = instrForDbtrAgtX;
	}
	public String getInstrForDbtrAgt() {
		return InstrForDbtrAgt;
	}
	public void setInstrForDbtrAgt(String instrForDbtrAgt) {
		InstrForDbtrAgt = instrForDbtrAgt;
	}
	public PmtId getPmtId() {
		return PmtId;
	}
	public void setPmtId(PmtId pmtId) {
		PmtId = pmtId;
	}
	public Amt getAmt() {
		return Amt;
	}
	public void setAmt(Amt amt) {
		Amt = amt;
	}
	public XchgRateInf getXchgRateInf() {
		return XchgRateInf;
	}
	public void setXchgRateInf(XchgRateInf xchgRateInf) {
		XchgRateInf = xchgRateInf;
	}
	public String getChrgBr() {
		return ChrgBr;
	}
	public void setChrgBr(String chrgBr) {
		ChrgBr = chrgBr;
	}
	public IntrmyAgt1 getIntrmyAgt1() {
		return IntrmyAgt1;
	}
	public void setIntrmyAgt1(IntrmyAgt1 intrmyAgt1) {
		IntrmyAgt1 = intrmyAgt1;
	}
	public CdtrAgt getCdtrAgt() {
		return CdtrAgt;
	}
	public void setCdtrAgt(CdtrAgt cdtrAgt) {
		CdtrAgt = cdtrAgt;
	}
	public Cdtr getCdtr() {
		return Cdtr;
	}
	public void setCdtr(Cdtr cdtr) {
		Cdtr = cdtr;
	}
	public CdtrAcct getCdtrAcct() {
		return CdtrAcct;
	}
	public void setCdtrAcct(CdtrAcct cdtrAcct) {
		CdtrAcct = cdtrAcct;
	}
	public RmtInf getRmtInf() {
		return RmtInf;
	}
	public void setRmtInf(RmtInf rmtInf) {
		RmtInf = rmtInf;
	}
	public SplmtryData getSplmtryData() {
		return SplmtryData;
	}
	public void setSplmtryData(SplmtryData splmtryData) {
		SplmtryData = splmtryData;
	}
	public Purp getPurp() {
		return Purp;
	}
	public void setPurp(Purp purp) {
		Purp = purp;
	}
	
	public String getChrgBrX() {
		return ChrgBrX;
	}
	public void setChrgBrX(String chrgBrX) {
		ChrgBrX = chrgBrX;
	}
}
