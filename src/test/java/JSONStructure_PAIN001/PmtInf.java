package JSONStructure_PAIN001;

import java.util.ArrayList;

import ChrgsAcct.ChrgsAcct;
import Dbtr.Dbtr;
import DbtrAcct.DbtrAcct;
import DbtrAgt.DbtrAgt;
import PmtTpInf.PmtTpInf;

public class PmtInf {
	private String PmtInfId;
	private String PmtInfIdX;
	
	private String PmtMtd;
	private String PmtMtdX;
	
	private String NbOfTxs;
	private String NbOfTxsX;
	
	private String CtrlSum;
	private String CtrlSumX;
	
	private PmtTpInf PmtTpInf;
	private String ReqdExctnDt;
	private String ReqdExctnDtX;
	
	private Dbtr Dbtr;
	private DbtrAcct DbtrAcct;
	private DbtrAgt DbtrAgt;
	
	public String getPmtInfIdX() {
		return PmtInfIdX;
	}
	public void setPmtInfIdX(String pmtInfIdX) {
		PmtInfIdX = pmtInfIdX;
	}
	public String getPmtMtdX() {
		return PmtMtdX;
	}
	public void setPmtMtdX(String pmtMtdX) {
		PmtMtdX = pmtMtdX;
	}
	public String getNbOfTxsX() {
		return NbOfTxsX;
	}
	public void setNbOfTxsX(String nbOfTxsX) {
		NbOfTxsX = nbOfTxsX;
	}
	public String getCtrlSumX() {
		return CtrlSumX;
	}
	public void setCtrlSumX(String ctrlSumX) {
		CtrlSumX = ctrlSumX;
	}
	public String getReqdExctnDtX() {
		return ReqdExctnDtX;
	}
	public void setReqdExctnDtX(String reqdExctnDtX) {
		ReqdExctnDtX = reqdExctnDtX;
	}
	private ChrgsAcct ChrgsAcct;
	
	
//	CdtTrfTxInf[] CdtTrfTxInf;
	ArrayList<CdtTrfTxInf> CdtTrfTxInf;
	
	public ArrayList<CdtTrfTxInf> getCdtTrfTxInf1() {
		return CdtTrfTxInf;
	}
	public void setCdtTrfTxInf1(ArrayList<CdtTrfTxInf> cdtTrfTxInf1) {
		CdtTrfTxInf = cdtTrfTxInf1;
	}
//	public CdtTrfTxInf[] getCdtTrfTxInf() {
//		return CdtTrfTxInf;
//	}
//	public void setCdtTrfTxInf(CdtTrfTxInf[] cdtTrfTxInf) {
//		CdtTrfTxInf = cdtTrfTxInf;
//	}
	public ChrgsAcct getChrgsAcct() {
		return ChrgsAcct;
	}
	public void setChrgsAcct(ChrgsAcct chrgsAcct) {
		ChrgsAcct = chrgsAcct;
	}
	public Dbtr getDbtr() {
		return Dbtr;
	}
	public void setDbtr(Dbtr dbtr) {
		Dbtr = dbtr;
	}
	public DbtrAcct getDbtrAcct() {
		return DbtrAcct;
	}
	public void setDbtrAcct(DbtrAcct dbtrAcct) {
		DbtrAcct = dbtrAcct;
	}
	public DbtrAgt getDbtrAgt() {
		return DbtrAgt;
	}
	public void setDbtrAgt(DbtrAgt dbtrAgt) {
		DbtrAgt = dbtrAgt;
	}
	public PmtTpInf getPmtTpInf() {
		return PmtTpInf;
	}
	public void setPmtTpInf(PmtTpInf pmtTpInf) {
		PmtTpInf = pmtTpInf;
	}
	public String getReqdExctnDt() {
		return ReqdExctnDt;
	}
	public void setReqdExctnDt(String reqdExctnDt) {
		ReqdExctnDt = reqdExctnDt;
	}
	
	public String getCtrlSum() {
		return CtrlSum;
	}
	public void setCtrlSum(String ctrlSum) {
		CtrlSum = ctrlSum;
	}
	public String getPmtInfId() {
		return PmtInfId;
	}
	public void setPmtInfId(String pmtInfId) {
		PmtInfId = pmtInfId;
	}
	public String getPmtMtd() {
		return PmtMtd;
	}
	public void setPmtMtd(String pmtMtd) {
		PmtMtd = pmtMtd;
	}
	public String getNbOfTxs() {
		return NbOfTxs;
	}
	public void setNbOfTxs(String nbOfTxs) {
		NbOfTxs = nbOfTxs;
	}
	
}
