/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : PstlAdr */
/* */
/* File Name : PstlAdr.java */
/* */
/* Description : PstlAdr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Dbtr;

import java.util.ArrayList;

import Address_Line.Addresses;

public class PstlAdr {
	private String StrtNm;
	private String BldgNb;
	private String PstCd;
	private String TwnNm;
	private String CtrySubDvsn;
	private String Ctry;
	private String CtryX;
	private ArrayList AdrLine;
	
	public String getCtryX() {
		return CtryX;
	}

	public void setCtryX(String ctryX) {
		CtryX = ctryX;
	}

	public ArrayList getAdrLine() {
		return AdrLine;
	}

	public void setAdrLine(ArrayList adrLine) {
		AdrLine = adrLine;
	}

	public String getBldgNb() {
		return BldgNb;
	}

	public void setBldgNb(String bldgNb) {
		BldgNb = bldgNb;
	}

	public String getPstCd() {
		return PstCd;
	}

	public void setPstCd(String pstCd) {
		PstCd = pstCd;
	}

	public String getTwnNm() {
		return TwnNm;
	}

	public void setTwnNm(String twnNm) {
		TwnNm = twnNm;
	}

	public String getCtrySubDvsn() {
		return CtrySubDvsn;
	}

	public void setCtrySubDvsn(String ctrySubDvsn) {
		CtrySubDvsn = ctrySubDvsn;
	}

	public String getStrtNm() {
		return StrtNm;
	}

	public void setStrtNm(String strtNm) {
		StrtNm = strtNm;
	}

	public String getCtry() {
		return Ctry;
	}

	public void setCtry(String ctry) {
		Ctry = ctry;
	}
	
}
