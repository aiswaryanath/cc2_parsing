/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrOrgId */
/* */
/* File Name : DbtrOrgId.java */
/* */
/* Description : DbtrOrgId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Dbtr;

public class DbtrOrgId {
	private String AnyBIC;
	private String AnyBICX;
	private DbtrOthr Othr;
	
	
	public String getAnyBICX() {
		return AnyBICX;
	}

	public void setAnyBICX(String anyBICX) {
		AnyBICX = anyBICX;
	}

	public String getAnyBIC() {
		return AnyBIC;
	}

	public void setAnyBIC(String anyBIC) {
		AnyBIC = anyBIC;
	}

	public DbtrOthr getOthr() {
		return Othr;
	}

	public void setOthr(DbtrOthr othr) {
		Othr = othr;
	}
	
}
