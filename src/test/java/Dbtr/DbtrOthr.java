/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrOthr */
/* */
/* File Name : DbtrOthr.java */
/* */
/* Description : DbtrOthr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Dbtr;

public class DbtrOthr {
	private String Id;
	private String IdX;

	public String getIdX() {
		return IdX;
	}

	public void setIdX(String idX) {
		IdX = idX;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}
	
}
