/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrId */
/* */
/* File Name : DbtrId.java */
/* */
/* Description : DbtrId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Dbtr;

public class DbtrId {
	
	private DbtrOrgId OrgId;

	public DbtrOrgId getOrgId() {
		return OrgId;
	}

	public void setOrgId(DbtrOrgId orgId) {
		OrgId = orgId;
	}
}
