/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : Dbtr */
/* */
/* File Name : Dbtr.java */
/* */
/* Description : Dbtr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Dbtr;

public class Dbtr {
	private String Nm;
	private String NmX;
	private PstlAdr PstlAdr;
	private DbtrId Id;
	
	public String getNmX() {
		return NmX;
	}

	public void setNmX(String nmX) {
		NmX = nmX;
	}

	
	public DbtrId getId() {
		return Id;
	}

	public void setId(DbtrId id) {
		Id = id;
	}

	public PstlAdr getPstlAdr() {
		return PstlAdr;
	}

	public void setPstlAdr(PstlAdr pstlAdr) {
		PstlAdr = pstlAdr;
	}

	public String getNm() {
		return Nm;
	}

	public void setNm(String nm) {
		Nm = nm;
	}
	
	
	
}
