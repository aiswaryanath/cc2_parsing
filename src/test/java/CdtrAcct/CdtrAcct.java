/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : CdtrAcct */
/* */
/* File Name : CdtrAcct.java */
/* */
/* Description : CdtrAcct POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package CdtrAcct;

public class CdtrAcct {
	private CdtrAcctId Id;
	 
	
	public CdtrAcctId getId() {
		return Id;
	}

	public void setId(CdtrAcctId id) {
		Id = id;
	}
}
