package PmtId;
public class PmtId {
	private String InstrId;
	private String InstrIdX;
	
	private String EndToEndId;
	private String EndToEndIdX;
	
	public String getInstrIdX() {
		return InstrIdX;
	}
	public void setInstrIdX(String instrIdX) {
		InstrIdX = instrIdX;
	}
	public String getEndToEndIdX() {
		return EndToEndIdX;
	}
	public void setEndToEndIdX(String endToEndIdX) {
		EndToEndIdX = endToEndIdX;
	}
	public String getInstrId() {
		return InstrId;
	}
	public void setInstrId(String instrId) {
		InstrId = instrId;
	}
	public String getEndToEndId() {
		return EndToEndId;
	}
	public void setEndToEndId(String endToEndId) {
		EndToEndId = endToEndId;
	}
	
}
