/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : Amt */
/* */
/* File Name : Amt.java */
/* */
/* Description :POJO for Amt */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Amt;

public class Amt {
	private InstdAmt InstdAmt;
	private EqvtAmt EqvtAmt;
	
	public EqvtAmt getEqvtAmt() {
		return EqvtAmt;
	}
	public void setEqvtAmt(EqvtAmt eqvtAmt) {
		EqvtAmt = eqvtAmt;
	}
	public InstdAmt getInstdAmt() {
		return InstdAmt;
	}
	public void setInstdAmt(InstdAmt instdAmt) {
		InstdAmt = instdAmt;
	}
	
}
