/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : EqvtAmt */
/* */
/* File Name : EqvtAmt.java */
/* */
/* Description :POJO for EqvtAmt */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Amt;

public class EqvtAmt {
	private String Amt;
	private String AmtX;
	private String Ccy;
	private String CcyX;
	private String CcyOfTrf;
	private String CcyOfTrfX;
	
	
	public String getCcy() {
		return Ccy;
	}
	public void setCcy(String ccy) {
		Ccy = ccy;
	}
	public String getCcyX() {
		return CcyX;
	}
	public void setCcyX(String ccyX) {
		CcyX = ccyX;
	}
	public String getCcyOfTrfX() {
		return CcyOfTrfX;
	}
	public void setCcyOfTrfX(String ccyOfTrfX) {
		CcyOfTrfX = ccyOfTrfX;
	}
	public String getAmtX() {
		return AmtX;
	}
	public void setAmtX(String amtX) {
		AmtX = amtX;
	}
	public String getAmt() {
		return Amt;
	}
	public void setAmt(String amt) {
		Amt = amt;
	}
	public String getCcyOfTrf() {
		return CcyOfTrf;
	}
	public void setCcyOfTrf(String ccyOfTrf) {
		CcyOfTrf = ccyOfTrf;
	}
}
