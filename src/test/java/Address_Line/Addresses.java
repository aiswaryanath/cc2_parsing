/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : Addresses */
/* */
/* File Name : Addresses.java */
/* */
/* Description :POJO for Address line*/
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Address_Line;

public class Addresses {
	private String AdrLine;

	public String getAddress() {
		return AdrLine;
	}

	public void setAddress(String address) {
		AdrLine = address;
	}
	
}
