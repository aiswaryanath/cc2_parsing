package MT103UI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.codoid.products.exception.FilloException;

import Busness_Validation.ExecuteQuery;
import Busness_Validation.GtbExcepGridDetailvalidation;
import Busness_Validation.psh_response_messagesValidation;
import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;
import Utility.SaltString;

public class GetValueMT103 {
	
	public static String getdata_AcctRefNo(String FIle_WORKITEMID) throws Exception{
		String SRC_REF_NUM=null;
		try{
			Class.forName(ReadDB.getENV1_OracleDriver());
			Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
			ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection,"select WORKITEMID,FILE_WORKITEM_ID,SRC_REF_NUM FROM TXN_PROC_OPN WHERE FILE_WORKITEM_ID   = '"+FIle_WORKITEMID+"'");
		  try {
	    	  while(LHS_ExceptionResult.next()) {
	    		  SRC_REF_NUM = LHS_ExceptionResult.getString("SRC_REF_NUM");
			  }
			}catch (Exception e) {
					e.printStackTrace();
			}finally {
				connection.close();
				LHS_ExceptionResult.close();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return SRC_REF_NUM;
}	

	public static String getdata_TXN_STATUS(String FIle_WORKITEMID) throws Exception{
		String TXN_STATUS=null;
		try{
			Class.forName(ReadDB.getENV1_OracleDriver());
			Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
			ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection,"select WORKITEMID,FILE_WORKITEM_ID,SRC_REF_NUM,TXN_STATUS FROM TXN_PROC_OPN WHERE FILE_WORKITEM_ID   = '"+FIle_WORKITEMID+"'");
		  try {
	    	  while(LHS_ExceptionResult.next()) {
	    		  TXN_STATUS = LHS_ExceptionResult.getString("TXN_STATUS");
			  }
			}catch (Exception e) {
					e.printStackTrace();
			}finally {
				connection.close();
				LHS_ExceptionResult.close();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return TXN_STATUS;
}	

	
	
	public static String getdata_WIRE_EXT_REF_NUM(String FIle_WORKITEMID) throws Exception{
		String WIRE_EXT_REF_NUM=null;
		try{
			Class.forName(ReadDB.getENV1_OracleDriver());
			Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
			ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection,"select WORKITEMID,FILE_WORKITEM_ID,SRC_REF_NUM,WIRE_EXT_REF_NUM FROM TXN_PROC_OPN WHERE FILE_WORKITEM_ID   = '"+FIle_WORKITEMID+"'");
		  try {
	    	  while(LHS_ExceptionResult.next()) {
	    		  WIRE_EXT_REF_NUM = LHS_ExceptionResult.getString("WIRE_EXT_REF_NUM");
			  }
			}catch (Exception e) {
					e.printStackTrace();
			}finally {
				connection.close();
				LHS_ExceptionResult.close();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return WIRE_EXT_REF_NUM;
}	
	
	
	public static String GetValueMT103data(String FILE_WORKITEM_ID) throws Exception {
		String str = "PSH4OTO18081410000"+SaltString.getRandNo(3)+"1I000"+GetValueMT103.getdata_AcctRefNo(FILE_WORKITEM_ID)+"       "+new SimpleDateFormat("yyMMdd").format(new Date()).toString()+"090814";
		return str;
	}
	
	public static void main(String[] args) throws Exception{
		System.out.println(getdata_TXN_STATUS("7885735"));
//		String date = new SimpleDateFormat("yyMMdd").format(new Date());
//		System.out.println(SaltString.getRandNo(3));
//		System.out.println(GetValueMT103.getdata_AcctRefNo("7494923"));
//		System.out.println(GetValueMT103.getdata_AcctRefNo("7883377"));
	}
}
