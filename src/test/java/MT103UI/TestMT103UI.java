package MT103UI;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.codoid.products.exception.FilloException;

import MT199.MT199;
import MT199UI.TestMT199UI;
import MT900UI.MT900;
import MT900UI.TestMT900UI;
import ReadDBConfiguration.ReadDB;
import Utility.TakeScreenShot_evidence;

public class TestMT103UI {
	
	public static void PUSHMT103(String FileWorkitemID) throws Exception{
		System.setProperty("webdriver.chrome.driver", "D:/Core_ComponentTool/IE_Driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
//		driver.get("http://10.10.8.62:10062/BPS/MT103StubCaller.jsp");
		driver.get(ReadDB.getMT103_Stub());
		driver.manage().window().maximize();
		
		driver.findElement(By.name("txtName")).sendKeys(GetValueMT103.GetValueMT103data(FileWorkitemID));
		
		TakeScreenShot_evidence.Evidences(driver, "PUSHMT103");
		driver.findElement(By.xpath("//input[@value='Submit Form']")).click();
		Thread.sleep(1000);
		TakeScreenShot_evidence.Evidences(driver, "PUSHMT103_SubmitLOG");
		driver.quit();
	}
	
	  public static void main(String[] args) throws Exception {
		  
		  PUSHMT103("7891324");
		  Thread.sleep(2000);
			MT199.CreateFile_MT199("7891324");
			TestMT199UI.PUSHMT199();
		Thread.sleep(2000);	
		MT900.CreateFile_MT900("7891324");
			TestMT900UI.PUSHMT900();
		
			
		  
		  
/*		System.setProperty("webdriver.ie.driver", "D:/Core_ComponentTool/IE_Driver/IEDriverServer.exe");
		DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
		capability.setCapability("pageLoadStrategy", "eager");
		WebDriver driver = new InternetExplorerDriver(capability);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		driver.get("http://10.10.8.62:10062/BPS/testMT900.jsp");
		driver.manage().window().maximize();
		
		driver.findElement(By.name("txtName")).sendKeys(GetValueMT103.GetValueMT103data("7883377"));
		
		driver.findElement(By.xpath("//input[@value='Reset Form']")).click();
		driver.quit();
*/	
		  
	  }

}
