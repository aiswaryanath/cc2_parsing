package DbtrAgt;

public class DbtrAgtPstlAdr {
	private String Ctry ;
	private String CtryX ;
	
	public String getCtryX() {
		return CtryX;
	}

	public void setCtryX(String ctryX) {
		CtryX = ctryX;
	}

	public String getCtry() {
		return Ctry;
	}

	public void setCtry(String ctry) {
		Ctry = ctry;
	}
	
}
