package ParseRecordExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Constants_pack.Constants;

public class Update_ExcelResult {
	
	public static void Update_DataReportAck(String TESTCASEID,String Actual_Result,String column_name) throws Exception{
		try{
			  //Timestamp timestamp1 = new Timestamp(System.currentTimeMillis());
			   // System.out.println("start time of"+timestamp1);
			
		System.setProperty("ROW", "2");
		Fillo fill=new Fillo();
		Connection conn =fill.getConnection(Constants._ExcelPath);
	
		conn.executeUpdate("update PAIN001 SET "+column_name+" = '"+Actual_Result+"' where Test_ID  = '"+TESTCASEID+"'");
		//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    //System.out.println("end time of"+timestamp);
	
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public static void Update_DataReportAckAsync(String TESTCASEID,String Actual_Result,String column_name,String column_name2,String Actual_Result2) throws Exception{
//		try{
//		System.setProperty("ROW", "2");
//		Fillo fill=new Fillo();
//		Connection conn =fill.getConnection(Constants._ExcelPath);
//		conn.executeUpdate("update PAIN001 SET "+column_name+" = '"+Actual_Result+"' , "+column_name2+" = '"+Actual_Result2+"'  where Test_ID  = '"+TESTCASEID+"'");
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	
	public static void clearCellValues(String column_name ,String TESTCASEID) throws Exception{
		try{
		System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			conn.executeUpdate("update PAIN001 SET "+column_name+" = ''  where Test_ID  = '"+TESTCASEID+"'");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void clearCellValues(String TESTCASEID) throws Exception{
		try{
		System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			conn.executeUpdate("update PAIN001 SET Actual_Error_Desc = '', Actual_Error_Code = '', Actual_FileStatus = '', Actual_GrpSts = '', Actual_Acknowledge = '', Server_ResponseCode = ''  where Test_ID  = '"+TESTCASEID+"'");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getCountRow() throws Exception{
		try{
			FileInputStream Fip = new FileInputStream(new File(Constants_pack.Constants._ExcelPath));
			HSSFWorkbook workbook = new HSSFWorkbook(Fip);
			HSSFSheet sheet =workbook.getSheet("PAIN001");
//			HSSFRow row=sheet.createRow(0);
//			Cell cell = row.createCell(0);
			int noOfColumns = sheet.getLastRowNum();
		
			workbook.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void clearActualData() throws Exception{
		try{
			System.setProperty("ROW", "2");
			Fillo fill=new Fillo(); 
			Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
			Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE  = 'Y'");
			int count=0;
			while (rs.next()) {
				clearCellValues(rs.getField("Test_ID"));
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}	
	 }
	
	public static void main(String[] args) throws Exception {
	//	Update_DataReportAck("JSON_Pain001v05_CMO_POSITIVE","Hello","Actual_GrpSts");
		updatepoi();
	  }
	
	public static void updatepoi()
	{
		try {
			  Timestamp timestamp1 = new Timestamp(System.currentTimeMillis());
			    System.out.println("start time of"+timestamp1);
		    FileInputStream file = new FileInputStream(new File("D:\\Core_ComponentTool\\AutomationTest_dataPain001.xlsx"));

		    XSSFWorkbook workbook = new XSSFWorkbook(file);
		    XSSFSheet sheet = workbook.getSheetAt(2);
		    Cell cell = null;

		    //Update the value of cell
		    cell = sheet.getRow(8).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(9).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(10).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(11).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(12).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(13).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(14).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(15).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(16).getCell(2);
		    cell.setCellValue("Aishwarya");
		    cell = sheet.getRow(17).getCell(2);
		    cell.setCellValue("Aishwarya");
		    /*
		    cell = sheet.getRow(2).getCell(2);
		    cell.setCellValue(cell.getNumericCellValue() * 2);
		    cell = sheet.getRow(3).getCell(2);
		    cell.setCellValue(cell.getNumericCellValue() * 2);
*/
		    file.close();

		    FileOutputStream outFile = new FileOutputStream(new File("D:\\Core_ComponentTool\\AutomationTest_dataPain001.xlsx"));
		    workbook.write(outFile);
		    outFile.close();
			  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			    System.out.println("END time of"+timestamp);

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
	}
