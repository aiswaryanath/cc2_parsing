package SplmtryData;

public class Cnts {
	private String StlmMth;
	private String PymtAmt;
	private String Ccy;
	private String PANID;
	private String FXId;
	private String DbAmt;
	
	
	private String StlmMthX;
	private String PymtAmtX;
	private String CcyX;
	private String PANIDX;
	private String FXIdX;
	private String DbAmtX;

	
	public String getDbAmt() {
		return DbAmt;
	}
	public void setDbAmt(String dbAmt) {
		DbAmt = dbAmt;
	}
	public String getDbAmtX() {
		return DbAmtX;
	}
	public void setDbAmtX(String dbAmtX) {
		DbAmtX = dbAmtX;
	}
	public String getStlmMthX() {
		return StlmMthX;
	}
	public void setStlmMthX(String stlmMthX) {
		StlmMthX = stlmMthX;
	}
	public String getPymtAmtX() {
		return PymtAmtX;
	}
	public void setPymtAmtX(String pymtAmtX) {
		PymtAmtX = pymtAmtX;
	}
	public String getCcyX() {
		return CcyX;
	}
	public void setCcyX(String ccyX) {
		CcyX = ccyX;
	}
	public String getPANIDX() {
		return PANIDX;
	}
	public void setPANIDX(String pANIDX) {
		PANIDX = pANIDX;
	}
	public String getFXIdX() {
		return FXIdX;
	}
	public void setFXIdX(String fXIdX) {
		FXIdX = fXIdX;
	}
	public String getStlmMth() {
		return StlmMth;
	}
	public void setStlmMth(String stlmMth) {
		StlmMth = stlmMth;
	}
	public String getPymtAmt() {
		return PymtAmt;
	}
	public void setPymtAmt(String pymtAmt) {
		PymtAmt = pymtAmt;
	}
	public String getCcy() {
		return Ccy;
	}
	public void setCcy(String ccy) {
		Ccy = ccy;
	}
	public String getPANID() {
		return PANID;
	}
	public void setPANID(String pANID) {
		PANID = pANID;
	}
	public String getFXId() {
		return FXId;
	}
	public void setFXId(String fXId) {
		FXId = fXId;
	}
	
	
}
