package SplmtryData;

import java.util.ArrayList;

public class SplmtryData {
	private Envlp Envlp; 
	private ArrayList Envlp1;
	private ArrayList SplmtryData;
	
	public ArrayList getSplmtryData() {
		return SplmtryData;
	}

	public Envlp getEnvlp() {
		return Envlp;
	}

	public void setEnvlp(Envlp envlp) {
		Envlp = envlp;
	}

	public ArrayList getEnvlp1() {
		return Envlp1;
	}

	public void setEnvlp1(ArrayList envlp1) {
		Envlp1 = envlp1;
	}

	public void setSplmtryData(ArrayList splmtryData) {
		SplmtryData = splmtryData;
	}


	
	
}
