/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : Cdtr */
/* */
/* File Name : Cdtr.java */
/* */
/* Description : Cdtr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Cdtr;
public class Cdtr {
	
	private String Nm;
	private String NmX;
	private CdtrPstlAdr PstlAdr;
	
	public String getNmX() {
		return NmX;
	}
	public void setNmX(String nmX) {
		NmX = nmX;
	}
	public String getNm() {
		return Nm;
	}
	public void setNm(String nm) {
		Nm = nm;
	}
	public CdtrPstlAdr getPstlAdr() {
		return PstlAdr;
	}
	public void setPstlAdr(CdtrPstlAdr pstlAdr) {
		PstlAdr = pstlAdr;
	}
	
}
