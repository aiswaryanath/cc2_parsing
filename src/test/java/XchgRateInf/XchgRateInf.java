/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : XchgRateInf */
/* */
/* File Name : XchgRateInf.java */
/* */
/* Description :XchgRateInf POJO*/
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package XchgRateInf;

public class XchgRateInf {
	private String XchgRate;
	private String XchgRateX;
	
	private String RateTp;
	private String RateTpX;
	
	private String CtrctId;
	private String CtrctIdX;
	
	public String getXchgRateX() {
		return XchgRateX;
	}
	public void setXchgRateX(String xchgRateX) {
		XchgRateX = xchgRateX;
	}
	public String getRateTpX() {
		return RateTpX;
	}
	public void setRateTpX(String rateTpX) {
		RateTpX = rateTpX;
	}
	public String getCtrctIdX() {
		return CtrctIdX;
	}
	public void setCtrctIdX(String ctrctIdX) {
		CtrctIdX = ctrctIdX;
	}
	public String getXchgRate() {
		return XchgRate;
	}
	public void setXchgRate(String xchgRate) {
		XchgRate = xchgRate;
	}
	public String getRateTp() {
		return RateTp;
	}
	public void setRateTp(String rateTp) {
		RateTp = rateTp;
	}
	public String getCtrctId() {
		return CtrctId;
	}
	public void setCtrctId(String ctrctId) {
		CtrctId = ctrctId;
	}
	
}
