/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : Batch_ProcessValidation */
/* */
/* File Name : Batch_ProcessValidation.java */
/* */
/* Description : Batch_ProcessValidation is used to get data from DB to validate the Batch failure in Pain001 JSON */
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Busness_Validation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;

public class Batch_ProcessValidation {
		
	public static void getdata_BatchProcessValidation(String TESTCASEID,String WORKITEMID,String STATUS) throws Exception{
		try{
			Class.forName(ReadDB.getENV1_OracleDriver());
			Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16864","CIBC_PAYMENTS_WEB12C4_VIEW","CIBC_PAYMENTS_WEB12C4_VIEW");
			ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection,"SELECT WORKITEMID, FILE_ID, BATCH_STS_CD FROM BATCH_PROCESS WHERE FILE_ID ='"+WORKITEMID+"'");
		  try {
	    	  while(LHS_ExceptionResult.next()) {
	    		  String BATCH_STS_CD=LHS_ExceptionResult.getString("BATCH_STS_CD");
	    		  String _WORKITEMID=LHS_ExceptionResult.getString("WORKITEMID");
	    		  Update_ExcelResult.Update_DataReportAck(TESTCASEID, BATCH_STS_CD, "Batch_Status_Actual");
	  			   //Async Resp
	  			 //  psh_response_messagesValidation.Getpsh_response_messages(TESTCASEID, WORKITEMID,STATUS);
	    		  if (BATCH_STS_CD.equalsIgnoreCase("BTREJE")) {
	    			  GtbExcepGridDetailvalidation.GtbExcepGridDetail(TESTCASEID, _WORKITEMID);
	    		  }
			  }
			}catch (Exception e) {
					e.printStackTrace();
			}finally {
				connection.close();
				LHS_ExceptionResult.close();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
}	
	
//	public static void main(String[] args) throws Exception{
//		Batch_ProcessValidation.getdata_BatchProcessValidation("Magic1","264137");
//	}

}
