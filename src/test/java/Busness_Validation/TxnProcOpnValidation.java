/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : FileProcOpnValidation */
/* */
/* File Name : FileProcOpnValidation.java */
/* */
/* Description : FileProcOpnValidation is used to validate and retrieve the data from FILE_PROC_OPN , 
 * BATCH_PROCESS, psh_response_messages , GTB_EXCEP_GRID_DETAIL  ,TXN_PROC_OPN and update in excel Simultaneously.
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/
package Busness_Validation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;

public class TxnProcOpnValidation {

	public static void getdata_TxnProcOpn(String TestCaseID,String WorkItemId,String Status) throws Exception{
		try{
				Class.forName(ReadDB.getENV1_OracleDriver());
				Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
//				Class.forName("oracle.jdbc.driver.OracleDriver");
//				Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16864","CIBC_PAYMENTS_WEB12C4_VIEW","CIBC_PAYMENTS_WEB12C4_VIEW");
				ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection, "Select WORKITEMID,FILE_WORKITEM_ID,cust_id,txns_oti09,TXN_STATUS,WIRE_EXT_REF_NUM,SRC_REF_NUM,TRN_STATUS_CODE FROM TXN_PROC_OPN WHERE FILE_WORKITEM_ID IN ("+WorkItemId+")");
		   try {
		    	  while(LHS_ExceptionResult.next()) {
	    			String TXN_Status = LHS_ExceptionResult.getString("TXN_STATUS");
	    			String WORKITEMID = LHS_ExceptionResult.getString("WORKITEMID");
//	    			String FILE_WORKITEM_ID = LHS_ExceptionResult.getString("FILE_WORKITEM_ID");

    				//Async Resp
    				//psh_response_messagesValidation.Getpsh_response_messages(TestCaseID, WORKITEMID,"TRXN");
    				
    				Update_ExcelResult.Update_DataReportAck(TestCaseID, TXN_Status, "TXN_StatusActual");
    				
//    				TXN_StatusActual
//  					Update WORKITEM ID From TXN_PROC_OPN 
//		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, WORKITEMID, "WorkItem_ID");
		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, WORKITEMID, "PaymentWorkItem_ID");

		    			if (TXN_Status.equalsIgnoreCase("Payment Rejected")) {
		    				GtbExcepGridDetailvalidation.GtbExcepGridDetail(TestCaseID, WORKITEMID);
						}
		    			if (TXN_Status.equalsIgnoreCase("Payment Suspended")) {
		    				GtbExcepGridDetailvalidation.GtbExcepGridDetail(TestCaseID, WORKITEMID);
		    		}
				  }
				}catch (Exception e) {
						e.printStackTrace();
				}finally {
					connection.close();
					LHS_ExceptionResult.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
	}
}