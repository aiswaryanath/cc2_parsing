/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : FileProcOpnValidation */
/* */
/* File Name : FileProcOpnValidation.java */
/* */
/* Description : ParseAsynchMessage is used to validate the Sync response JSON Structure. 
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package Busness_Validation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import ParseRecordExcel.Update_ExcelResult;

public class ParseAsynchMessage {

	public static String readFile(String filename) {
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}

	/*public static String _AsynchJSONStructure_Validation(String TestCaseID) throws Exception{
	    try{		
			StringBuilder errorLogs = new StringBuilder();
			String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_Pain001_ASYC.json");
			//GrpHdr
			JSONObject json_objectGrpHdr=new JSONObject(toString_RespJSON_Structureval).getJSONObject("Document").getJSONObject("CstmrPmtStsRpt").getJSONObject("GrpHdr");
			json_objectGrpHdr.getString("CreDtTm");
			json_objectGrpHdr.getString("MsgId");
			json_objectGrpHdr.getJSONObject("InitgPty").get("Nm");
			System.out.println(json_objectGrpHdr.getJSONObject("InitgPty").getJSONObject("Id").getJSONObject("OrgId").getString("AnyBIC"));
			
			//OrgnlGrpInfAndSts
			JSONObject json_objectOrgnlGrpInfAndSts=new JSONObject(toString_RespJSON_Structureval).getJSONObject("Document").getJSONObject("CstmrPmtStsRpt").getJSONObject("OrgnlGrpInfAndSts");
			json_objectOrgnlGrpInfAndSts.get("OrgnlMsgId");
			json_objectOrgnlGrpInfAndSts.get("OrgnlMsgNmId");
			
			Update_ExcelResult.Update_DataReportAck(TestCaseID, json_objectOrgnlGrpInfAndSts.get("GrpSts").toString(), "Resp_Status");
			
	    }catch (Exception e) {
			Update_ExcelResult.Update_DataReportAck(TestCaseID, e.getMessage(), "Resp_Status");
		}
		return TestCaseID;	
	}
*/
	public static String _AsynchJSONStructure_ValidationWIP(String TestCaseID) throws Exception{
	    try{		
			StringBuilder errorLogs = new StringBuilder();
			String toString_RespJSON_Structureval = readFile("D:/Core_ComponentTool/_Output/"+TestCaseID+"_Pain001_ASYC.json");
			//GrpHdr
			JSONObject json_objectGrpHdr=new JSONObject(toString_RespJSON_Structureval).getJSONObject("Document").getJSONObject("CstmrPmtStsRpt").getJSONObject("GrpHdr");
			json_objectGrpHdr.getString("CreDtTm");
			json_objectGrpHdr.getString("MsgId");
			json_objectGrpHdr.getJSONObject("InitgPty").get("Nm");
			json_objectGrpHdr.getJSONObject("InitgPty").getJSONObject("Id").getJSONObject("OrgId").getString("AnyBIC");
			
			//OrgnlGrpInfAndSts
			JSONObject json_objectOrgnlGrpInfAndSts=new JSONObject(toString_RespJSON_Structureval).getJSONObject("Document").getJSONObject("CstmrPmtStsRpt").getJSONObject("OrgnlGrpInfAndSts");
			json_objectOrgnlGrpInfAndSts.get("OrgnlMsgId");
			json_objectOrgnlGrpInfAndSts.get("OrgnlMsgNmId");
//			System.out.println("OrgnlGrpInfAndSts:"+json_objectOrgnlGrpInfAndSts.get("GrpSts").toString());
			
			//Update Resp_Status
			Update_ExcelResult.Update_DataReportAck(TestCaseID, json_objectOrgnlGrpInfAndSts.get("GrpSts").toString(), "Async_GrpSts_Actual");
			
			String Checkpoint = json_objectOrgnlGrpInfAndSts.get("GrpSts").toString();
			
			System.out.println("Checkpoint:"+Checkpoint);
			
			if (Checkpoint.equalsIgnoreCase("RJCT")) {
				
				//OrgnlPmtInfAndSts
				JSONObject json_objectOrgnlPmtInfAndSts=new JSONObject(toString_RespJSON_Structureval).getJSONObject("Document").getJSONObject("CstmrPmtStsRpt").getJSONObject("OrgnlPmtInfAndSts");
				
				if (json_objectOrgnlPmtInfAndSts.has("PmtInfSts")) {
					System.out.println("PmtInfSts::"+json_objectOrgnlPmtInfAndSts.get("PmtInfSts"));
					Update_ExcelResult.Update_DataReportAck(TestCaseID, json_objectOrgnlPmtInfAndSts.get("PmtInfSts").toString(), "Async_PmtInfSts_Actual");
				}
				
				if (json_objectOrgnlPmtInfAndSts.has("TxInfAndSts")) {
				
				if (json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").has("TxSts")) {
//					System.out.println(json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").get("OrgnlInstrId"));
					System.out.println("OrgnlPmtInfAndSts:"+json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").get("TxSts"));
					Update_ExcelResult.Update_DataReportAck(TestCaseID, json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").get("TxSts").toString(), "Async_TxInfAndSts_Actual");
				}	
				
				//Error Code
//				System.out.println("Rsn Cd:"+json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").getJSONArray("StsRsnInf").getJSONObject(0).getJSONObject("Rsn").get("Cd"));
				JSONArray StsRsnInf_arr = json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").getJSONArray("StsRsnInf");
				System.out.println(StsRsnInf_arr.length());
				ArrayList<String> StsRsnInf_res=new ArrayList<String>();
				StsRsnInf_res.add(StsRsnInf_arr.getJSONObject(0).get("AddtlInf").toString());
				System.out.println(StsRsnInf_res.get(0));
				
				for (int i = 0; i < StsRsnInf_arr.length(); i++) {
					System.out.println("1Cd:"+StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString());
					System.out.println("1AddtlInf:"+StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString());
					
					Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Async_ErrorCode_Actual");
					Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString(), "Async_Errordesc_Actual");
				}
			  }else{
				  JSONArray StsRsnInf_arr = json_objectOrgnlPmtInfAndSts.getJSONArray("StsRsnInf");
				  System.out.println(StsRsnInf_arr.length());
					ArrayList<String> StsRsnInf_res=new ArrayList<String>();
					StsRsnInf_res.add(StsRsnInf_arr.getJSONObject(0).get("AddtlInf").toString());
					System.out.println(StsRsnInf_res.get(0));
//					Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_res.get(0), "Actual_FileStatus");
					for (int i = 0; i < StsRsnInf_arr.length(); i++) {
						System.out.println("2Cd:"+StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString());
						System.out.println("2AddtlInf:"+StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString());
						
						Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Cd").toString(), "Async_ErrorCode_Actual");
						Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString(), "Async_Errordesc_Actual");
					}
			  }
			}
			
			if (Checkpoint.equalsIgnoreCase("ACSP")) {
				JSONObject json_objectOrgnlPmtInfAndSts=new JSONObject(toString_RespJSON_Structureval).getJSONObject("Document").getJSONObject("CstmrPmtStsRpt").getJSONObject("OrgnlPmtInfAndSts");
				
				if (json_objectOrgnlPmtInfAndSts.has("PmtInfSts")) {
					System.out.println("PmtInfSts::"+json_objectOrgnlPmtInfAndSts.get("PmtInfSts"));
					Update_ExcelResult.Update_DataReportAck(TestCaseID, json_objectOrgnlPmtInfAndSts.get("PmtInfSts").toString(), "Async_PmtInfSts_Actual");
				}
				
				if (json_objectOrgnlPmtInfAndSts.has("TxInfAndSts")) {
				
				if (json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").has("TxSts")) {
					System.out.println("OrgnlPmtInfAndSts:"+json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").get("TxSts"));
					Update_ExcelResult.Update_DataReportAck(TestCaseID, json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").get("TxSts").toString(), "Async_TxInfAndSts_Actual");
				}	
				
				JSONArray StsRsnInf_arr = json_objectOrgnlPmtInfAndSts.getJSONObject("TxInfAndSts").getJSONArray("StsRsnInf");
				System.out.println(StsRsnInf_arr.length());
				ArrayList<String> StsRsnInf_res=new ArrayList<String>();
				StsRsnInf_res.add(StsRsnInf_arr.getJSONObject(0).get("AddtlInf").toString());
				System.out.println(StsRsnInf_res.get(0));
				
				for (int i = 0; i < StsRsnInf_arr.length(); i++) {
					System.out.println("3Prtry:"+StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Prtry").toString());
					System.out.println("3AddtlInf:"+StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString());
					
					Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).getJSONObject("Rsn").get("Prtry").toString(), "StsRsnInfRsn_Actual");
					Update_ExcelResult.Update_DataReportAck(TestCaseID, StsRsnInf_arr.getJSONObject(i).get("AddtlInf").toString(), "StsRsnInf_AddtlInfActual");
				}

				
				
			}	
				
			}
	    }catch (Exception e) {
//			Update_ExcelResult.Update_DataReportAck(TestCaseID, e.getMessage(), "Resp_Status");
	    	e.printStackTrace();
		}
		return TestCaseID;	
	}

	public static void main(String[] args) throws Exception{
		System.setProperty("ROW", "2");
		Fillo fill=new Fillo();
		Connection conn=fill.getConnection(Constants_pack.Constants._ExcelPath);
		Recordset rs=conn.executeQuery("Select * from PAIN001 where RUN_CASE = 'Y'");
		 while (rs.next()) {
			 System.out.println(rs.getField("Test_ID"));
			 _AsynchJSONStructure_ValidationWIP(rs.getField("Test_ID"));
		 }	 
	}
}
