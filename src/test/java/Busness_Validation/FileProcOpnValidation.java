/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : FileProcOpnValidation */
/* */
/* File Name : FileProcOpnValidation.java */
/* */
/* Description : FileProcOpnValidation is used to validate and retrieve the data from FILE_PROC_OPN , 
 * BATCH_PROCESS, psh_response_messages , GTB_EXCEP_GRID_DETAIL  ,TXN_PROC_OPN and update in excel Simultaneously.
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Busness_Validation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;


public class FileProcOpnValidation {
	
public static void getdata_FileProcOpn(String TestCaseID,String MsgID,String _Acknowledge) throws Exception{
	try{
			Class.forName(ReadDB.getENV1_OracleDriver());
			Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16864","CIBC_PAYMENTS_WEB12C4_VIEW","CIBC_PAYMENTS_WEB12C4_VIEW");
			ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection, "SELECT WORKITEMID, MESSAGE_ID, FILE_STATUS, FILE_NAME FROM FILE_PROC_OPN WHERE MESSAGE_ID = '"+MsgID+"'");
	   try {
	    	  while(LHS_ExceptionResult.next()) {
    			String FILE_STATUS = LHS_ExceptionResult.getString("FILE_STATUS");
    			String WORKITEMID = LHS_ExceptionResult.getString("WORKITEMID");
    			String FILE_NAME = LHS_ExceptionResult.getString("FILE_NAME");
    			//System.out.println(WORKITEMID+"\t"+FILE_STATUS);
	    			Update_ExcelResult.Update_DataReportAck(TestCaseID, WORKITEMID, "File_WorkitemID");
	    			Update_ExcelResult.Update_DataReportAck(TestCaseID, FILE_STATUS, "File_Status_Actual");
	    			Update_ExcelResult.Update_DataReportAck(TestCaseID, FILE_NAME, "FILE_NAME");
    				//Async Resp
    			//	psh_response_messagesValidation.Getpsh_response_messages(TestCaseID, WORKITEMID,"FILE");

	    			if (_Acknowledge.equalsIgnoreCase("RJCT")) {
	    				GtbExcepGridDetailvalidation.GtbExcepGridDetail(TestCaseID, WORKITEMID);
					}else if(_Acknowledge.equalsIgnoreCase("ACCP")){
						TxnProcOpnValidation.getdata_TxnProcOpn(TestCaseID,WORKITEMID,"TRXN");  
						Batch_ProcessValidation.getdata_BatchProcessValidation(TestCaseID, WORKITEMID,"BATCH");
						GtbExcepGridDetailvalidation.GtbExcepGridDetail(TestCaseID, WORKITEMID);
					}
	    	  }
			}catch (Exception e) {
					e.printStackTrace();
			}finally {
				connection.close();
				LHS_ExceptionResult.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
}

//	public static void main(String[] args) throws Exception{
//		System.out.println(ReadDB.getENV1_OracleDriver());
//		System.out.println(ReadDB.getENV1_ConnName());
//		System.out.println(ReadDB.getENV1_Uname());
//		System.out.println(ReadDB.getENV1_Pass());
//	}
}
