/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : FileProcOpnValidation */
/* */
/* File Name : FileProcOpnValidation.java */
/* */
/* Description : FileProcOpnValidation is used to validate and retrieve the data from FILE_PROC_OPN , 
 * BATCH_PROCESS, psh_response_messages , GTB_EXCEP_GRID_DETAIL  ,TXN_PROC_OPN and update in excel Simultaneously.
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Busness_Validation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;

public class GtbExcepGridDetailvalidation {
	
public static void GtbExcepGridDetail(String TestCaseID,String WorkItemID) throws Exception{
		try{
				Class.forName(ReadDB.getENV1_OracleDriver());
				Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
//				Class.forName("oracle.jdbc.driver.OracleDriver");
//				Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@10.10.9.55:1521:SIR16864","CIBC_PAYMENTS_WEB12C4_VIEW","CIBC_PAYMENTS_WEB12C4_VIEW");
				ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection, "SELECT EXCEPTION_ID,EXCEPTION_DESC, EXTERNAL_EXCEPTION_ID, EXTERNAL_EXCEPTION_DESC, TAG_NAME FROM GTB_EXCEP_GRID_DETAIL WHERE WORKITEMID = '"+WorkItemID+"'");
		   try {
		    	  while(LHS_ExceptionResult.next()) {
	    			String EXCEPTION_ID = LHS_ExceptionResult.getString("EXCEPTION_ID");
	    			String EXCEPTION_DESC = LHS_ExceptionResult.getString("EXCEPTION_DESC");
	    			
	    			String EXTERNAL_EXCEPTION_ID = LHS_ExceptionResult.getString("EXTERNAL_EXCEPTION_ID");
	    			String EXTERNAL_EXCEPTION_DESC = LHS_ExceptionResult.getString("EXTERNAL_EXCEPTION_DESC");

		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, EXCEPTION_ID, "Internal_Exception_IDActual");
		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, EXCEPTION_DESC.replaceAll("'", ""), "InternalExceptionDescriptionActual");
		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, EXTERNAL_EXCEPTION_ID, "External_Exception_IDActual");
		    			
		    			//fix below error description
		    			Update_ExcelResult.Update_DataReportAck(TestCaseID, EXTERNAL_EXCEPTION_DESC.replaceAll("'", ""), "ExternalExceptionDescriptionActual");
	                }
				}catch (Exception e) {
						e.printStackTrace();
				}finally {
					connection.close();
					LHS_ExceptionResult.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
	}
}
