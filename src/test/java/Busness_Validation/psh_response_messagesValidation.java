/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : FileProcOpnValidation */
/* */
/* File Name : FileProcOpnValidation.java */
/* */
/* Description : FileProcOpnValidation is used to validate and retrieve the data from FILE_PROC_OPN , 
 * BATCH_PROCESS, psh_response_messages , GTB_EXCEP_GRID_DETAIL  ,TXN_PROC_OPN and update in excel Simultaneously.
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package Busness_Validation;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import ParseRecordExcel.Update_ExcelResult;
import ReadDBConfiguration.ReadDB;

public class psh_response_messagesValidation {
	public static void Getpsh_response_messages(String TestCaseID,String WorkItemID,String STATUS) throws Exception{
		try{
				Class.forName(ReadDB.getENV1_OracleDriver());
				Connection connection=DriverManager.getConnection(ReadDB.getENV1_ConnName(),ReadDB.getENV1_Uname(),ReadDB.getENV1_Pass());
				ResultSet LHS_ExceptionResult=ExecuteQuery.getResultSet(connection, "select WORKITEMID,RESP_TYPE,RESP_FORMAT,CHANNEL_SOURCE,RESP_FILE_NAME,RESP_STATUS_DESC,RESP_MESSAGE from psh_response_messages where workitemid= '"+WorkItemID+"'");
				//Update WorkItemID
				Update_ExcelResult.Update_DataReportAck(TestCaseID, WorkItemID, "WorkItemId");
		   try {
		    	  while(LHS_ExceptionResult.next()) {
	    			    String RESP_MESSAGE = LHS_ExceptionResult.getString("RESP_MESSAGE");
	    			    FileWriter fw=new FileWriter("D:\\Core_ComponentTool\\_Output\\"+TestCaseID+"_Pain001_ASYC.json");    
	    		           fw.write(RESP_MESSAGE);    
	    		         fw.close();    

	    		         ParseAsynchMessage._AsynchJSONStructure_ValidationWIP(TestCaseID);
	                }
				}catch (Exception e) {
						e.printStackTrace();
				}finally {
					connection.close();
					LHS_ExceptionResult.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public static void main(String[] args) throws Exception{
		psh_response_messagesValidation.Getpsh_response_messages("JSON_Pain001v05_CMO_BV-MCM_TC454", "324234","");
	}
	
}
