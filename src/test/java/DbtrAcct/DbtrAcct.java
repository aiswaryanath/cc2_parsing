/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrAcct */
/* */
/* File Name : DbtrAcct.java */
/* */
/* Description : DbtrAcct POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package DbtrAcct;

public class DbtrAcct {
	private DbtrAcctId Id;
	private String Ccy;
	private String CcyX;
	private String Nm;
	private String NmX;
	
	public String getCcyX() {
		return CcyX;
	}

	public void setCcyX(String ccyX) {
		CcyX = ccyX;
	}

	public String getNmX() {
		return NmX;
	}

	public void setNmX(String nmX) {
		NmX = nmX;
	}

	public String getCcy() {
		return Ccy;
	}

	public void setCcy(String ccy) {
		Ccy = ccy;
	}

	public String getNm() {
		return Nm;
	}

	public void setNm(String nm) {
		Nm = nm;
	}

	public DbtrAcctId getId() {
		return Id;
	}

	public void setId(DbtrAcctId id) {
		Id = id;
	}
}
