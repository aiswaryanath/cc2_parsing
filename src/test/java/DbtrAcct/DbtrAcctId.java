/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrAcctId */
/* */
/* File Name : DbtrAcctId.java */
/* */
/* Description : DbtrAcctId POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/

package DbtrAcct;

public class DbtrAcctId {
	private DbtrAcctOthr Othr;

	public DbtrAcctOthr getOthr() {
		return Othr;
	}

	public void setOthr(DbtrAcctOthr othr) {
		Othr = othr;
	}
	
}
