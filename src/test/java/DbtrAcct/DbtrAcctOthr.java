/***************************************************************************************************/
/* Copyright � 2019 Intellect Design Arena a Ltd. All rights reserved */
/* */
/************************************************************************/
/* Application : CORE COMPONENT AUTOMATION TOOL	*/
/* Module Name : DbtrAcctOthr */
/* */
/* File Name : DbtrAcctOthr.java */
/* */
/* Description : DbtrAcctOthr POJO
/* */
/* Author : Varun Paranganath 
 * */
/****************************************************************************************************/


package DbtrAcct;

public class DbtrAcctOthr {
	private String Id;
	private String IdX;
	
	public String getIdX() {
		return IdX;
	}

	public void setIdX(String idX) {
		IdX = idX;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}
	
}
